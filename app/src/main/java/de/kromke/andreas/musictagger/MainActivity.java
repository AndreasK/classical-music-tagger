/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.musictagger;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.UriPermission;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.DocumentsContract;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
//import java.io.FileDescriptor;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import android.os.ParcelFileDescriptor;
import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.nio.charset.Charset;
//import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import de.kromke.andreas.utilities.AsyncTaskExecutor;
import de.kromke.andreas.utilities.DirectoryTree;
import de.kromke.andreas.utilities.DirectoryTree.DirectoryEntry;
import de.kromke.andreas.utilities.DirectoryTreeFile;
import de.kromke.andreas.utilities.DirectoryTreeSaf;
import de.kromke.andreas.utilities.DirectoryTreeVirtualRoot;
import de.kromke.andreas.utilities.MyMediaDataSource;
import de.kromke.andreas.utilities.SafUtilities;
import de.kromke.andreas.utilities.UriToPath;

import static android.content.UriPermission.INVALID_TIME;
import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;

/** @noinspection JavadocBlankLines, RedundantSuppression , JavadocLinkAsPlainText , CommentedOutCode */ // handles the file or directory selector
@SuppressWarnings("Convert2Lambda")
public class MainActivity extends BasicActivity
        implements
            AdapterView.OnItemClickListener,
            AdapterView.OnItemLongClickListener,
            ServiceConnection,
            MediaPlayer.OnCompletionListener
{
    private static final String LOG_TAG = "CMT : MainActivity";
    private static final String stateSelected = "selected";
    private static final String stateCurrDir = "currDir";
    private static final int maxNumOfPersistedPermissions = 7;

    private final static int MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE = 11;
    private final static int MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES = 12;
    boolean mFileReadPermissionGranted = false;
    boolean mFileWritePermissionGranted = false;
    protected boolean mbScannerServiceConnected = false;
    TextView mFooterView;
    View mDividerView;
    ListView mPathListView;
    FloatingActionButton mFloatingButton1;
    ProgressBar mProgressBar;
    boolean mbAsyncTaskBusy;
    ArrayAdapter<String> mPathListAdapter;
    ArrayList<String> mAudioPathArrayList = null;   // strings to be shown in the list
    int mNumOfAudioFiles = 0;
    boolean[] mSelectedList = null;             // either null or one "boolean" for each <mAudioPathArrayList> entry
    int mNumOfSelectedFiles = 0;
    boolean[] mEditableList = null;             // either null or one "boolean" for each <mAudioPathArrayList> entry
    int mNumOfEditableFiles = 0;

    boolean mbSuccessfullyConvertedToSaf;
    boolean mbNotConvertedToSaf;
    boolean mbNoSafPermission;

    DirectoryTree mDir = null;
    List<DirectoryEntry> mFileList = null;      // used for both directory and file mode
    String mSavedDir = null;
    private boolean mBackKeyAlreadyPressed;     // for folder view to go back to album view
    Context mThePackageContext;

    // these static variable should survive a device rotation. TODO: test it
    static private MediaPlayer mMediaPlayer = null;
    static private int mPlayingIndex = -1;

    // strange objects needed to start other Activities
    ActivityResultLauncher<Intent> mTaggerActivityLauncher;
    // this does not make sense because we cannot pass an Intent with necessary flags
    //ActivityResultLauncher<Uri> mRequestOpenDocumentTreeActivityLauncher;
    ActivityResultLauncher<Intent> mOpenDocumentTreeActivityLauncher;
    ActivityResultLauncher<Intent> mOpenDocumentActivityLauncher;
    ActivityResultLauncher<Intent> mStorageAccessPermissionActivityLauncher;

    private static class UriOrPath
    {
        Uri uri = null;
        String path = null;
        UriOrPath(Uri theUri) {uri = theUri;}
        UriOrPath(String thePath) {path = thePath;}
    }


    /************************************************************************************
     *
     * Activity method: create the activity, holding all tabs
     *
     *  Note that this is also called in case the device had been turned, or
     *  in case the device had been shut off for a while. In this case we only have
     *  to re-initialise the GUI, but not the tables, database etc.
     *
     *  Note that the parameters are passed in an "intent" which we can get with
     *  getIntent(). The intent remains valid also after a device turn, i.e. the
     *  Activity will be recreated with the original parameters plus the saved
     *  InstanceState.
     *
     ***********************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mPathListView = findViewById(R.id.file_list);
        mFooterView = findViewById(R.id.footerText);
        mDividerView = findViewById(R.id.list_divider_line);
        mBackKeyAlreadyPressed = false;

        mbMenuItemSafSelectEnabled = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);

        mFloatingButton1 = findViewById(R.id.fab);
        mProgressBar = findViewById(R.id.progressBar);
        if (!mbAsyncTaskBusy)
        {
            mProgressBar.setVisibility(View.GONE);
        }

        registerRequestTaggerCallback();
        registerOpenDocumentTreeCallback();
        registerOpenDocumentCallback();
        registerStorageAccessPermissionCallback();

        mThePackageContext = this;
        mFloatingButton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (mNumOfSelectedFiles == 0)
                {
                    Snackbar.make(view, R.string.str_noFileSelected, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                /* Maybe File access was not granted, but we are in SAF mode.
                if (!mReadWritePermissionGranted)
                {
                    Snackbar.make(view, R.string.str_noAccessGranted, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                else
                */
                {
                    //startEditActivity();
                    // run async task to get <editable> flags, then start activity
                    mbAsyncTaskBusy = true;
                    mProgressBar.setVisibility(View.VISIBLE);
                    new FileAccessTask().execute(2);
                }
            }
        });

        // set localised strings
        DirectoryTreeFile.strWriteProtected = getString(R.string.str_write_protected);
        DirectoryTreeFile.strNoAccess = getString(R.string.str_no_access);
        DirectoryTreeSaf.strWriteProtected = getString(R.string.str_write_protected);
        DirectoryTreeVirtualRoot.strStorageSelection = getString(R.string.str_storage_selection);
        DirectoryTreeVirtualRoot.strSafOnly = getString(R.string.str_saf_only);

        if (savedInstanceState != null)
        {
            Log.d(LOG_TAG, "onCreate() : read selection array from saved state");
            mSelectedList = savedInstanceState.getBooleanArray(stateSelected);
            mSavedDir = savedInstanceState.getString(stateCurrDir);
            Log.d(LOG_TAG, "onCreate() : saved directory is " + mSavedDir);
            initListSelection();
        }

        // We need the old (pre-Android-11) file read permissions at once, otherwise
        // we cannot run on older Android versions. Later we may ask for full
        // file access on Android 11 and above.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            if (Environment.isExternalStorageManager())
            {
                Log.d(LOG_TAG, "onCreate(): permission already granted");
                mFileReadPermissionGranted = true;
                mFileWritePermissionGranted = true;
            }
            else
            {
                if (UserSettings.getBool(UserSettings.PREF_DBG_REQUEST_MEDIA_FILE_ACCESS, false))
                {
                    // In fact we can ask for READ permission to some (?) files, including audio, but
                    // we will never get WRITE permission.
                    requestForMediaReadPermission30();
                }
            }
        }
        else
        {
            requestForPermissionOld();
        }

        onFileAccessModesChanged();
        updateActionBar();
    }


    /**************************************************************************
     *
     * Activity method
     *
     *************************************************************************/
    @Override
    protected void onDestroy()
    {
        Log.d(LOG_TAG, "onDestroy()");

        if (isFinishing())
        {
            Log.d(LOG_TAG, "onDestroy() : is finishing");
            if ((mMediaPlayer != null) && (mPlayingIndex >= 0))
            {
                mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
        }

        if (mbScannerServiceConnected)
        {
            try
            {
                unbindService(this);        // there once was an exception here...
            } catch (Exception e)
            {
                Log.w(LOG_TAG, "Error on unbindService()", e);
            }
            // TODO: also stop service?!?
            mbScannerServiceConnected = false;
        }

        super.onDestroy();
    }


    /************************************************************************************
     *
     * Activity method: save state
     *
     ***********************************************************************************/
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)
    {
        Log.d(LOG_TAG, "onSaveInstanceState()");
        super.onSaveInstanceState(outState);
        outState.putBooleanArray(stateSelected, mSelectedList);

        // save current directory as string (path or URI)
        if (mDir != null)
        {
            DirectoryEntry de = mDir.getCurrent();
            if (de != null)
            {
                String path = de.toString();
                if (!path.isEmpty())
                {
                    outState.putString(stateCurrDir, path);
                }
            }
        }
    }


    /************************************************************************************
     *
     * Activity method: got new intent
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent intent)
    {
        Log.d(LOG_TAG, "onNewIntent(): intent action is " + intent.getAction());
        // reset state of activity
        mSelectedList = null;
        setIntent(intent);
        setupPathList();
        updateActionBar();
        super.onNewIntent(intent);
    }


    /**************************************************************************
     *
     * ignore keys during file operations
     *
     *************************************************************************/
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        Log.d(LOG_TAG, "dispatchKeyEvent()");
        if (mbAsyncTaskBusy)
        {
            Log.d(LOG_TAG, "dispatchKeyEvent(): ignore, because reading is busy");
            return true;        // eat the key
        }
        else
            return super.dispatchKeyEvent(event);
    }


    /************************************************************************************
     *
     * set selection colour etc.
     *
     ***********************************************************************************/
    private void initListSelection()
    {
        mNumOfSelectedFiles = 0;
        if (mSelectedList != null)
        {
            for (boolean aMSelectedList : mSelectedList)
            {
                if (aMSelectedList)
                {
                    mNumOfSelectedFiles++;
                }
            }
        }
    }


    /************************************************************************************
     *
     * decide if file name belongs to an audio file
     *
     ***********************************************************************************/
    private boolean isAudioFile(final String name)
    {
        int pos = name.lastIndexOf(".");
        if (pos >= 0)
        {
            String extension = name.substring(pos);
            //noinspection RedundantIfStatement
            if (extension.equals(".mp3") ||
                extension.equals(".mp4") ||
                extension.equals(".m4a") ||
                extension.equals(".flac") ||
                extension.equals(".ogg") ||
                extension.equals(".opus") ||
                extension.equals(".MP3") ||
                extension.equals(".MP4") ||
                extension.equals(".M4A") ||
                extension.equals(".FLAC") ||
                extension.equals(".OGG") /*||
                extension.equals(".OPUS")*/
            )
            {
                return true;
            }
        }
        return false;
    }


    /************************************************************************************
     *
     * decide if file is an audio file
     *
     ***********************************************************************************/
    private boolean isAudioFile(DirectoryEntry de)
    {
        return !de.isDirectory() && isAudioFile(de.getName());
    }


    /************************************************************************************
     *
     * get file names
     *
     * https://stackoverflow.com/questions/41096332/issues-traversing-through-directory-hierarchy-with-android-storage-access-framew
     * https://stackoverflow.com/questions/27759915/bug-when-listing-files-with-android-storage-access-framework-on-lollipop
     *
     ***********************************************************************************/
    private void populateFiles()
    {
        mProgressBar.setVisibility(View.VISIBLE);

        // run async task to read directory
        mbAsyncTaskBusy = true;
        new FileAccessTask().execute(1);
    }


    /************************************************************************************
     *
     * called when AsyncTask has run: mFileList = mDir.getChildren()
     *
     ***********************************************************************************/
    private void populateFiles2(@SuppressWarnings({"unused", "RedundantSuppression"}) int result)
    {
        if (mDir == null)
        {
            Log.e(LOG_TAG, "populateFiles2(): malfunction, no directory");
            return;
        }
        Log.d(LOG_TAG, "populateFiles2(): found " + mFileList.size() + " directory entries");

        mAudioPathArrayList = new ArrayList<>();
        String info = mDir.getInfoString();
        mFooterView.setText(info);
        mNumOfAudioFiles = 0;

        for (DirectoryEntry de : mFileList)
        {
            String s = de.getName();
            if (de.isDirectory())
            {
//                    Log.d(LOG_TAG, "populateFiles2(): found directory " +f.getName());
                s = s + "/";
            }
            else
            {
//                    Log.d(LOG_TAG, "populateFiles2(): found file " + name);
                if (isAudioFile(s))
                {
                    mNumOfAudioFiles++;
                }
            }
            mAudioPathArrayList.add(s);
        }

        setupPathList2();

        mbAsyncTaskBusy = false;
        mProgressBar.setVisibility(View.GONE);
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private int getIndexOfOldestPersistedUriPermission(List<UriPermission> thePermissionList)
    {
        long otime = INVALID_TIME;
        int oindex = -1;

        int i = 0;
        for (UriPermission perm : thePermissionList)
        {
            long ptime = perm.getPersistedTime();
            if (ptime == INVALID_TIME)
            {
                Log.w(LOG_TAG, "getIndexOfOldestPersistedUriPermission(): invalid time for Uri " + perm.getUri());
            }
            else
            if (oindex == -1)
            {
                oindex = i;
                otime = ptime;
            }
            else
            if (ptime < otime)
            {
                otime = ptime;
                oindex = i;
            }
            i++;
        }

        return oindex;
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private boolean gotoVirtualRoot()
    {
        Log.d(LOG_TAG, "gotoVirtualRoot()");
        List<UriPermission> thePermissionList = getContentResolver().getPersistedUriPermissions();

        // remove old entries
        while (thePermissionList.size() > maxNumOfPersistedPermissions)
        {
            int index = getIndexOfOldestPersistedUriPermission(thePermissionList);
            if (index < 0)
            {
                break;
            }
            else
            {
                Uri uri = thePermissionList.get(index).getUri();
                Log.d(LOG_TAG, "gotoVirtualRoot(): remove oldest persisted permission: " + uri);
                getContentResolver().releasePersistableUriPermission(uri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                thePermissionList.remove(index);
            }
        }

        String mediaPaths = getMusicBasePath();
        // Classic File paths, access must be permitted as "full file access" with Android 11 and above,
        //  but will never be with with Play Store version.
        String[] rootPaths = mediaPaths.split("\n");
        // Add number of stored SAF paths. For Android 11 and above these are the only valid ones in Play Store version.
        int n = rootPaths.length + thePermissionList.size();
        Log.d(LOG_TAG, "gotoVirtualRoot(): n = " + n);

        if (n > 1)
        {
            DirectoryTreeVirtualRoot theDir = new DirectoryTreeVirtualRoot(rootPaths);
            theDir.setUriPermissions(this, thePermissionList);
            mDir = theDir;
            return true;
        }

        return false;
    }


    /* ***********************************************************************************
     *
     * Debug helper: Analyse the DocumentFile for readability, writeability etc.
     *
     ***********************************************************************************/
    /*
    private void analyseDocumentFile(@NonNull DocumentFile df)
    {
        Uri uriFromDf = df.getUri();
        Log.d(LOG_TAG, "addToFileList() : Uri = " + uriFromDf);
        Log.d(LOG_TAG, "-- isDocumentUri: " + DocumentFile.isDocumentUri(this, df.getUri()));
        Log.d(LOG_TAG, "--------- isFile: " + df.isFile());
        Log.d(LOG_TAG, "------ isVirtual: " + df.isVirtual());
        Log.d(LOG_TAG, "----------- name: " + df.getName());
        Log.d(LOG_TAG, "----------- type: " + df.getType());
        Log.d(LOG_TAG, "--------- parent: " + df.getParentFile());
        Log.d(LOG_TAG, "--------- length: " + df.length());
        Log.d(LOG_TAG, "--------- exists: " + df.exists());
        Log.d(LOG_TAG, "-------- canRead: " + df.canRead());
        Log.d(LOG_TAG, "------- canWrite: " + df.canWrite());

        InputStream is = null;
        try
        {
            is = getContentResolver().openInputStream(uriFromDf);
            if (is == null)
            {
                Log.e(LOG_TAG, "-- cannot open InputStream");
            }
            else
            {
                int flen = (int) df.length();
                byte[] data = new byte[flen];
                int res = is.read(data);
                is.close();
                Log.d(LOG_TAG, "-- data read:" + res);
            }
        } catch (FileNotFoundException e)
        {
            e.printStackTrace();
            Log.e(LOG_TAG, "-- file not found exception:" + e);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "-- I/O exception:" + e);
        }
        Log.d(LOG_TAG, "------- input stream: " + is);

        try
        {
            ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(uriFromDf, "r");
            if (pfd == null)
            {
                Log.e(LOG_TAG, "-- cannot open ParcelFileDescriptor with mode \"r\"");
            }
            else
            {
                Log.d(LOG_TAG, "-- pfd with mode \"r\": " + pfd);
                pfd.close();
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "-- file not found exception:" + e);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "-- I/O exception:" + e);
        }

        try
        {
            ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(uriFromDf, "rw");
            if (pfd == null)
            {
                Log.e(LOG_TAG, "-- cannot open ParcelFileDescriptor with mode \"rw\"");
            }
            else
            {
                Log.d(LOG_TAG, "-- pfd with mode \"rw\": " + pfd);
                FileDescriptor fd = pfd.getFileDescriptor();
                FileOutputStream fos = new FileOutputStream(fd);
                String demo = "written using FileOutputStream in mode rw";
//                fos.write(demo.getBytes(StandardCharsets.UTF_8));
                fos.write(demo.getBytes(StandardCharsets.UTF_8), 0, 0);
                pfd.close();
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "-- file not found exception:" + e);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "-- I/O exception:" + e);
        }

        if (false)
        OutputStream os = null;
        try
        {
            os = getContentResolver().openOutputStream(uriFromDf);
            if (os == null)
            {
                Log.e(LOG_TAG, "-- cannot open OutputStream");
            }
            else
            {
                String demo = "Hallo erstmal";
                os.write(demo.getBytes(StandardCharsets.UTF_8));
                os.close();
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "-- file not found exception:" + e);
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "-- I/O exception:" + e);
        }
        Log.d(LOG_TAG, "------- output stream: " + os);
        }
    }
    */


    /************************************************************************************
     *
     * Add either path (preferred) or Uri to the file array, at index i
     *
     ***********************************************************************************/
    private void addObject(String path, Uri uri)
    {
        DocumentFile df = null;
        if (path == null)
        {
            df = DocumentFile.fromSingleUri(this, uri);
            if (df == null)
            {
                path = uri.getPath();
            }
        }
        else
        {
            int vindex = getVolumeIndex(path);

            if (vindex < 0)
            {
                Log.w(LOG_TAG, "addObject() : file not found in volume list: " + path);
            } else if (vindex > 0)
            {
                if ((Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) && mFileWritePermissionGranted)
                {
                    // full file access for Android 11 and newer: SD card is writable via File access
                    Log.d(LOG_TAG, "addObject() : file not on internal memory, but writable due to full file access: " + path);
                }
                else
                {
                    Log.d(LOG_TAG, "addObject() : file not on internal memory and will not be writable: " + path);
                    df = SafUtilities.getUriFromPath(path, this);
                    if (df == null)
                    {
                        mbNotConvertedToSaf = true;
                    } else
                    {
                        Log.d(LOG_TAG, "addObject() : successfully converted to SAF: " + path);
                        mbSuccessfullyConvertedToSaf = true;
                    }
                }
            }
        }

        if (df != null)
        {
            //analyseDocumentFile(df);
            Log.d(LOG_TAG, "addObject() : DocumentFile name is: " + df.getName());
            DirectoryTreeSaf.DirectoryEntrySaf de = new DirectoryTreeSaf.DirectoryEntrySaf(df, this);
            if (!de.hasParent())
            {
                mbNoSafPermission = true;
            }
            mFileList.add(de);
            path = de.getPath();
            String decodedPath = Uri.decode(path);

            // handle bug in Google's SMB client
            if (!path.equals(decodedPath))
            {
                Log.e(LOG_TAG, "addObject() : path is not decoded: " + path);
                Log.w(LOG_TAG, "addObject() : decoding to: " + decodedPath);
                path = decodedPath;
            }

            String name = df.getName();
            if ((path.startsWith("/document/")) && ((name == null) || (!path.endsWith(name))))
            {
                path = Uri.decode(de.toString()) + " (" + name + ")";
            }
            /*
            if (path.startsWith("/document/msf:"))
            {
                //special handling for Download folder
                path = "/document/msf:" + df.getName();
            }
            else
            if (path.startsWith("/document/acc="))
            {
                //special handling for Google Drive
                path = "/document/gd:" + df.getName();
            }
            else
            if (path.startsWith("/document/file,"))
            {
                //special handling for Box
                path = "/document/box:" + df.getName();
            }
            else
            if (path.startsWith("/document/content://com.microsoft.skydrive.content.metadata/"))
            {
                //special handling for Microsoft OneDrive
                path = "/document/onedrive:" + df.getName();
            }
            else
            if (path.startsWith("/document/smb:"))
            {
                //special handling for Google SMB client: path should be decoded, but in fact is not (bug in Document Provider)
                path = Uri.decode(path);
            }
            */
            else
            {
                path = DirectoryTreeSaf.getPseudoPath(path);
            }

        }
        else
        {
            // internal memory, or SAF failure
            mFileList.add(new DirectoryTreeFile.DirectoryEntryFile(path));
        }

        mAudioPathArrayList.add(path);
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private void enterFileBrowserMode()
    {
        Log.d(LOG_TAG, "enterFileBrowserMode(): no path table");

        String mediaPaths = getMusicBasePath();
        String[] rootPaths = mediaPaths.split("\n");

        if (!gotoVirtualRoot())
        {
            mDir = new DirectoryTreeFile(rootPaths[0]);
        }

        if (mSavedDir != null)
        {
            DirectoryTree d = new DirectoryTreeSaf(this, mSavedDir);
            if (d.isValid())
            {
                mDir = d;       // SAF mode
            }
            else
            {
                mDir.setCurrent(this, mSavedDir);       // File mode
            }
        }
        populateFiles();
        // show path elements
        mDividerView.setVisibility(View.VISIBLE);
        mFooterView.setVisibility(View.VISIBLE);
    }


    /************************************************************************************
     *
     * helper
     *
     ***********************************************************************************/
    private void enterFileListMode(ArrayList<UriOrPath> newObjectList)
    {
        if (mDir != null)
        {
            mDir = null;    // leave directory mode
            mFileList = null;
            mAudioPathArrayList = null;
        }

        if (mAudioPathArrayList == null)
        {
            mAudioPathArrayList = new ArrayList<>();
        }

        if (mFileList == null)
        {
            mFileList = new ArrayList<>();
        }

        mbSuccessfullyConvertedToSaf = false;
        mbNotConvertedToSaf = false;
        mbNoSafPermission = false;

        for (UriOrPath theObject : newObjectList)
        {
            if ((theObject.path == null) && (theObject.uri != null))
            {
                //
                // try to derive path from Uri (heuristic methods)
                //

                Log.d(LOG_TAG, "enterFileListMode(): intent data Uri path is " + theObject.uri.getPath());
                Log.d(LOG_TAG, "enterFileListMode():  Uri authority = " + theObject.uri.getAuthority());
                Log.d(LOG_TAG, "enterFileListMode():  Uri scheme = " + theObject.uri.getScheme());
                Log.d(LOG_TAG, "enterFileListMode():  Uri host = " + theObject.uri.getHost());

                String thePath = UriToPath.getPathFromIntentUri(this, theObject.uri);
                Log.d(LOG_TAG, "enterFileListMode(): derived path is " + thePath);

                if (thePath != null)
                {
                    //
                    // replace Uri by path, if path is valid
                    //

                    File f = new File(thePath);
                    Log.d(LOG_TAG, "enterFileListMode(): f.exists() = " + f.exists());
                    Log.d(LOG_TAG, "enterFileListMode(): f.isFile() = " + f.isFile());
                    Log.d(LOG_TAG, "enterFileListMode(): f.canRead() = " + f.canRead());
                    Log.d(LOG_TAG, "enterFileListMode(): f.canWrite() = " + f.canWrite() + " (if false: note that Android lies!)");
                    if (f.exists() && f.isFile() && f.canRead())
                    {
                        Log.d(LOG_TAG, "enterFileListMode(): path seems to be valid");
                    } else
                    {
                        Log.w(LOG_TAG, "enterFileListMode(): path seems to be invalid");
                        thePath = null;
                    }

                    theObject.path = thePath;
                }
            }

            //
            // store Uri or path
            //

            addObject(theObject.path, theObject.uri);
        }

        if (mbNoSafPermission)
        {
            hintMissingSafPermission();
        }
        if (mbNotConvertedToSaf)
        {
            Toast.makeText(getApplicationContext(), R.string.str_path_not_converted_to_saf, Toast.LENGTH_LONG).show();
        }
        else
        if (mbSuccessfullyConvertedToSaf)
        {
            Toast.makeText(getApplicationContext(), R.string.str_path_converted_to_saf, Toast.LENGTH_LONG).show();
        }

        // each file in list is an audio file
        mNumOfAudioFiles = mAudioPathArrayList.size();

        // hide path elements
        mDividerView.setVisibility(View.GONE);
        mFooterView.setVisibility(View.GONE);
        setupPathList2();
    }


    /************************************************************************************
     *
     * init function, called after read access has been granted
     *
     * application started:
     *  intent action is android.intent.action.MAIN
     *  intent category is android.intent.category.LAUNCHER
     *
     * "Open with":
     *
     *  (Uri scheme is always "content")
     *  (Uri host and authority are "com.*.*...." or "media"
     *
     * ASUS file manager:
     * ==================
     *  intent action is "android.intent.action.VIEW"
     *  mData is "content://com.asus.filemanager.OpenFileProvider/file/storage/9C33-6BBD/Music/Bla%20Bla.mp3"
     *  mType is "audio/mpeg"
     * or
     *  mData is "content://com.asus.filemanager.OpenFileProvider/file/sdcard/Music/Bla%20Bla.mp3"
     *  DocumentFile:
     *      isDocumentUri       false
     *      isFile              false
     *      type                null
     *      parent              null
     *      exists              true
     *      canRead             false
     *      canWrite            false
     *      really readable     yes
     *      really writeable    no
     *
     * LG file manager:
     * ================
     *  mType is "audio/*"
     *
     *  mData is "content://media/external/audio/media/4711"
     *  DocumentFile:
     *      isDocumentUri       false
     *      isFile              true
     *      type                audio/mpeg
     *      parent              null
     *      exists              true
     *      canRead             true
     *      canWrite            false
     *      really readable     yes
     *      really writeable    yes
     *
     * Total Commander:
     * =================
     *  mType is "audio/mp4a-latm"
     *
     *  mData is "content://com.ghisler.files/storage/emulated/0/Music/Hymnen/Bla%20Bla.m4a"
     *  DocumentFile:
     *      isDocumentUri       false
     *      isFile              true
     *      type                audio/mpeg
     *      parent              null
     *      exists              true
     *      canRead             true
     *      canWrite            true
     *      really readable     yes
     *      really writeable    yes
     *
     * MiX file manager:
     * =================
     *  mType is "audio/mp4a-latm"
     *
     *  mData is "content://com.mixplorer.fileProvider/CEWvRkIDpqtERwAod-sFJApuj8q5oXgrol6Pkjo-r1aI5i9HR_rtF_Rwf___P___zGa1My_30S9HwJpOjw==/Bla%20Bla.m4a"
     *  DocumentFile:
     *      isDocumentUri       false
     *      isFile              true
     *      type                audio/mp4a-latm
     *      parent              null
     *      exists              true
     *      canRead             true
     *      canWrite            false
     *      really readable     yes
     *      really writeable    yes
     *
     * "Files" from System Settings: primary storage
     * ============================
     *  mData is "content://com.android.externalstorage.documents/document/primary%3AMusic%2FHymnen%2FBla%20bla.m4a
     *  DocumentFile:
     *      isDocumentUri       true
     *      isFile              true
     *      type                audio/mpeg
     *      parent              null
     *      exists              true
     *      canRead             true
     *      canWrite            true
     *      really readable     yes
     *      really writeable    yes
     *
     * "Files" from System Settings: Downloads
     * ============================
     *  mData is "content://com.android.providers.downloads.documents/document/msf%3A11627"
     *  -> authority = com.android.providers.downloads.documents
     *  -> path is "/document/msf:11627"
     *
     * "Files" from System Settings: Google Drive
     * ============================
     *  mData is "content://com.google.android.apps.docs.storage/document/acc%3D1%3Bdoc%3Dencoded%3De9z...FdUHS9Kj"
     *  -> authority = com.google.android.apps.docs.storage
     *  -> path is "/document/acc=1;doc=encoded=e9z...FdUHS9Kj"
     *
     * "Files" from System Settings: Box
     * ============================
     *  mData is "content://com.box.android.documents/document/file%2C741...48769"
     *  -> authority = com.box.android.documents
     *  -> path is "/document/file,741...8769"
     *
     * "Files" from System Settings: OneDrive
     * ============================
     *  mData is "content://com.microsoft.skydrive.content.StorageAccessProvider/document/content%3A%2F%2Fcom.microsoft.skydrive.content.metadata%2FDrive%2FID%2F1%2FItem%2FID%2F9%2FProperty%2F%3FRefreshOption%3DAutoRefresh%26RefreshTimeOut%3D15000%26CostAttributionKey%3D11-21-1"
     *  -> authority = com.microsoft.skydrive.content.StorageAccessProvider
     *  -> path is "/document/content://com.microsoft.skydrive.content.metadata/Drive/ID/1/Item/ID/9/Property/?RefreshOption=AutoRefresh&RefreshTimeOut=15000&CostAttributionKey=11-21-1"
     *
     * "Share":
     *
     * mData is null
     * mClipData is not null and holds lots of information
     * mExtras is a Bundle
     * For ACTION_SEND, look in the EXTRA_STREAM extra for a Uri or in the EXTRA_TEXT extra for the actual text
     *
     ***********************************************************************************/
    private void setupPathList()
    {
        Intent theIntent = getIntent();
        String action = theIntent.getAction();
        ArrayList<UriOrPath> newObjectList = new ArrayList<>();

        Log.d(LOG_TAG, "setupPathList(): intent action is " + theIntent.getAction());
        Log.d(LOG_TAG, "setupPathList(): intent type is " + theIntent.getType());
        Log.d(LOG_TAG, "setupPathList(): intent data is " + theIntent.getData());

        //
        // official messages like SHARE and OPEN_WITH
        //

        if ((Intent.ACTION_VIEW.equals(action)) ||
            (Intent.ACTION_EDIT.equals(action)))
        {
            Uri theUri = theIntent.getData();
            if (theUri != null)
            {
                UriOrPath newObject = new UriOrPath(theUri);
                newObjectList.add(newObject);
            }
        }
        else
        if ((Intent.ACTION_SEND.equals(action)))
        {
            Uri theUri = theIntent.getParcelableExtra(Intent.EXTRA_STREAM);
            if (theUri != null)
            {
                UriOrPath newObject = new UriOrPath(theUri);
                newObjectList.add(newObject);
            }
        }
        else
        if ((Intent.ACTION_SEND_MULTIPLE.equals(action)))
        {
            ArrayList<Uri> uriList = theIntent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
            if (uriList != null)
            {
                for (Uri theUri : uriList)
                {
                    UriOrPath newObject = new UriOrPath(theUri);
                    newObjectList.add(newObject);
                }
            }
        }

        //
        // Unofficial message from music player applications
        //

        ArrayList<String> pathList = theIntent.getStringArrayListExtra("pathTable");
        if (pathList != null)
        {
            for (String thePath : pathList)
            {
                // this could be a path or URI
                Uri uri = Uri.parse(thePath);
                UriOrPath newObject;
                if ((uri.getScheme() != null) && (uri.getAuthority() != null))
                {
                    newObject = new UriOrPath(uri);     // valid Uri
                }
                else
                {
                    newObject = new UriOrPath(thePath);     // no Uri
                }

                newObjectList.add(newObject);
            }
        }

        //
        // Enter file browser mode or file list mode
        //

        if ((mAudioPathArrayList == null) && (newObjectList.isEmpty()))
        {
            enterFileBrowserMode();
        }
        else
        if (!newObjectList.isEmpty())
        {
            enterFileListMode(newObjectList);
        }
    }


    /************************************************************************************
     *
     * second part of setupPathList(), called after AsyncTask has finished or from enterFileListMode()
     *
     ***********************************************************************************/
    private void setupPathList2()
    {
        if ((mSelectedList != null) && (mSelectedList.length != mAudioPathArrayList.size()))
        {
            Log.w(LOG_TAG, "setupPathList2(): recreate selection array due to size mismatch");
            mSelectedList = null;   // force recreation
        }

        if (mSelectedList == null)
        {
            Log.d(LOG_TAG, "setupPathList2(): create selection array");
            mSelectedList = new boolean[mAudioPathArrayList.size()];
            mNumOfSelectedFiles = 0;
        }

        // Assign adapter to ListView
        setPathListAdapter();
        mPathListView.setAdapter(mPathListAdapter);
        mPathListView.setOnItemClickListener(this);
        mPathListView.setOnItemLongClickListener(this);
        mBackKeyAlreadyPressed = false;
        updateActionBar();
    }


    /**************************************************************************
     *
     * update action bar
     *
     *************************************************************************/
    private void updateActionBar()
    {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
        {
            actionBar.setSubtitle(mNumOfSelectedFiles + "/" + getStrForFilesN(mNumOfAudioFiles));
        }

        int id = (mNumOfSelectedFiles > 0) ? R.color.action_button_info_colour : R.color.action_button_info_disabled_colour;
        ColorStateList tint = AppCompatResources.getColorStateList(this, id);
        mFloatingButton1.setBackgroundTintList(tint);

        mbMenuItemRevertEnabled = mbMenuItemRemoveEnabled = (mNumOfSelectedFiles > 0);
        mbMenuItemRenameEnabled = (mNumOfSelectedFiles == 1);
        updatePlayState();
    }


    /**************************************************************************
     *
     * update GUI accordingly
     *
     *************************************************************************/
    @Override
    protected void onBasePathChanged()
    {
        if (mDir != null)
        {
            // only redraw in directory mode
            mAudioPathArrayList = null; // force recreation
            setupPathList();
        }
    }


    /**************************************************************************
     *
     * update GUI accordingly
     *
     *************************************************************************/
    private void onFileAccessModesChanged()
    {
        DirectoryTreeVirtualRoot.mFileReadWriteAccess = mFileReadPermissionGranted;
        DirectoryTreeFile.mFileReadAccess = mFileReadPermissionGranted;
        DirectoryTreeFile.mFileWriteAccess = mFileWritePermissionGranted;
        mAudioPathArrayList = null; // force recreation
        setupPathList();
    }


    /**************************************************************************
     *
     * called when access request has been denied or granted
     * (old method for Android 4.4. .. 10 and for media files in Android 11 and newer)
     *
     *************************************************************************/
    @Override
    public void onRequestPermissionsResult
    (
        int requestCode,
        @NonNull String[] permissions,
        @NonNull int[] grantResults
    )
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(LOG_TAG, "onRequestPermissionsResult()");

        //noinspection SwitchStatementWithTooFewBranches
        switch (requestCode)
        {
            // Android 4.4 .. 10
            case MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission granted");
                    mFileReadPermissionGranted = true;
                    mFileWritePermissionGranted = true;
                    onFileAccessModesChanged();
                } else
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): permission denied");
                    Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                }
            }

            // Android 11 and newer
            case MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES:
            {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): media read permission granted");
                    mFileReadPermissionGranted = true;
                    //mFileWritePermissionGranted = false;
                    onFileAccessModesChanged();
                } else
                {
                    Log.d(LOG_TAG, "onRequestPermissionsResult(): media read permission denied");
                }
            }
        }
    }


    /**************************************************************************
     *
     * variant for Android 4.4 .. 10
     *
     *************************************************************************/
    private void requestForPermissionOld()
    {
        String[] requests = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

        int permissionCheck1 = ContextCompat.checkSelfPermission(this, requests[0]);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this, requests[1]);
        if ((permissionCheck1 == PackageManager.PERMISSION_GRANTED) && (permissionCheck2 == PackageManager.PERMISSION_GRANTED))
        {
            Log.d(LOG_TAG, "file r/w permissions immediately granted");
            setupPathList();
            mFileReadPermissionGranted = true;
            mFileWritePermissionGranted = true;
        } else
        {
            Log.d(LOG_TAG, "request permission");
            ActivityCompat.requestPermissions(this, requests,
                    MY_PERMISSIONS_REQUEST_READ_WRITE_EXTERNAL_STORAGE);
        }
    }


    /**************************************************************************
     *
     * media read access for Android 11 and newer
     *
     * Note that this makes not much sense as we only can read audio files,
     * but not change them. Note that we also see images, but not if they
     * are detected as album art.
     *
     * Also note that Google's official Android documentation does not tell the
     * truth. Instead, in Android 11 WRITE_EXTERNAL_STORAGE is ignored,
     * and READ_EXTERNAL_STORAGE grants read access to media files.
     *
     *************************************************************************/
    private void requestForMediaReadPermission30()
    {
        //noinspection ExtractMethodRecommender
        String[] requests;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
        {
            // Android 13 needs different requests than 4..12
            requests = new String[]{
                    Manifest.permission.READ_MEDIA_AUDIO,
                    Manifest.permission.READ_MEDIA_IMAGES
            };
        }
        else
        {
            // this one is invalid in Android 13
            requests = new String[]{
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE  // we might omit that
            };
        }

        int permissionCheck1 = ContextCompat.checkSelfPermission(this, requests[0]);
        int permissionCheck2 = ContextCompat.checkSelfPermission(this, requests[1]);
        if ((permissionCheck1 == PackageManager.PERMISSION_GRANTED) && (permissionCheck2 == PackageManager.PERMISSION_GRANTED))
        {
            Log.d(LOG_TAG, "media read permissions immediately granted");
            mFileReadPermissionGranted = true;
            //mFileWritePermissionGranted = false;
            setupPathList();
        } else
        {
            Log.d(LOG_TAG, "request media read permission");
            ActivityCompat.requestPermissions(this, requests,
                    MY_PERMISSIONS_REQUEST_READ_MEDIA_FILES);
        }
    }


    /**************************************************************************
     *
     * Request for full file access (Android 11 (API 30) and newer)
     *
     * Callback is located in
     * registerStorageAccessPermissionCallback()/onActivityResult()
     *
     *************************************************************************/
    @RequiresApi(api = Build.VERSION_CODES.R)
    protected void requestForPermission30()
    {
        Intent intent = new Intent(ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        mStorageAccessPermissionActivityLauncher.launch(intent);
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void setSelectionMode(View view, int position)
    {
        int id;
        boolean isEven = (position % 2 == 0);

        if ((mSelectedList != null) && (mSelectedList[position]))
        {
            id = (isEven) ? R.color.colourLineSelectedEven : R.color.colourLineSelectedOdd;
        }
        else
        {
            id = (isEven) ? R.color.colourLineNormalEven : R.color.colourLineNormalOdd;
        }

        if (mDir != null)
        {
            if ((mFileList == null) || (position >= mFileList.size()))
            {
                Log.e(LOG_TAG, "setSelectionMode(): position " + position + " is outside of mFileList");
            }
            else
            {
                // disable non-audio files
                DirectoryEntry de = mFileList.get(position);
                //noinspection RedundantIfStatement
                if (de.isDirectory() || isAudioFile(de))
                {
                    view.setEnabled(true);
                } else
                {
                    view.setEnabled(false);
                }
            }
        }

        view.setBackgroundColor(getResources().getColor(id));
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void setPathListAdapter()
    {
        mPathListAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mAudioPathArrayList)
        {
            @Override
            public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent)
            {
                //Log.d(LOG_TAG, "getView(position = " + position + ")");
                View theView = super.getView(position, convertView, parent);
                setSelectionMode(theView, position);
                return theView;
            }

        };
    }


    /**************************************************************************
     *
     * load current directory and create new selection table with
     * all items unselected
     *
     *************************************************************************/
    private void chDir(DirectoryEntry de)
    {
        if (de == null)
        {
            Log.d(LOG_TAG, "chDir() : virtual root");
        }
        else
        {
            Log.d(LOG_TAG, "chDir(\"" + de.getPath() + "\")");
        }
        populateFiles();        // this runs asynchronously
    }


    /**************************************************************************
     *
     * called when list item is touched
     *
     *************************************************************************/
    public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id)
    {
        Log.d(LOG_TAG, "onItemClick(pos = " + position + ")");
        if (mbAsyncTaskBusy)
        {
            Log.d(LOG_TAG, "onItemClick(): ignore, because reading is busy");
            return;
        }

        // detect clicks to directory
        if (mDir != null)
        {
            DirectoryEntry de = mFileList.get(position);
            if (de.isDirectory())
            {
                Log.d(LOG_TAG, "onItemClick() : set directory " + de.getName());
                DirectoryTree newDir = mDir.setCurrent(this, de);
                if (newDir != null)
                {
                    mDir = newDir;
                    chDir(de);
                }
                else
                {
                    Log.d(LOG_TAG, "onItemClick() : cannot set directory " + de.getName());
                }
                return;
            }
            else
            if (!isAudioFile(de))
            {
                // ignore other files
                return;
            }
        }

        mSelectedList[position] = !mSelectedList[position];
        setSelectionMode(view, position);
        if (mSelectedList[position])
        {
            mNumOfSelectedFiles++;
        }
        else
        {
            mNumOfSelectedFiles--;
        }
        updateActionBar();
    }


    /**************************************************************************
     *
     * called when list item is long touched
     *
     *************************************************************************/
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
    {
        if (mbAsyncTaskBusy)
        {
            Log.d(LOG_TAG, "onItemLongClick(): ignore, because reading is busy");
            return true;
        }

        // select all
        boolean newValue;
        // check if all are disabled
        if (mNumOfSelectedFiles == mNumOfAudioFiles)
        {
            newValue = false;
            mNumOfSelectedFiles = 0;
        }
        else
        {
            newValue = true;
            mNumOfSelectedFiles = mNumOfAudioFiles;
        }
        for (int i = 0; i < mSelectedList.length; i++)
        {
            if (mDir != null)
            {
                // skip directories and non-audio files
                DirectoryEntry de = mFileList.get(i);
                if (de.isDirectory() || !isAudioFile(de))
                {
                    continue;
                }
            }
            mSelectedList[i] = newValue;
        }
        // force redraw
        //mPathListView.setAdapter(mPathListAdapter);   // too brute
        mPathListAdapter.notifyDataSetChanged();

        updateActionBar();
        // click consumed
        return true;
    }


    /**************************************************************************
     *
     * find file by name
     *
     *************************************************************************/
    private DirectoryEntry findFile(final String name)
    {
        for (DirectoryEntry de : mFileList)
        {
            if (de.getName().equals(name))
            {
                return de;
            }
        }

        return null;
    }


    /**************************************************************************
     *
     * helper for rename the selected file
     *
     *************************************************************************/
    private int getSelectedIndex()
    {
        for (int i = 0; i < mFileList.size(); i++)
        {
            if (mSelectedList[i])
            {
                return i;
            }
        }

        return -1;
    }


    /**************************************************************************
     *
     * helper to force reload of directory
     *
     *************************************************************************/
    private void forceReload()
    {
        if (mDir != null)
        {
            DirectoryEntry de = mDir.getCurrent();
            if (de != null)
            {
                chDir(de);
            }
        }
        else
        {
            mPathListAdapter.notifyDataSetChanged();
        }
    }


    /**************************************************************************
     *
     * selected files are reverted or permanently removed
     *
     *************************************************************************/
    private void doRemoveOrRevert(boolean bIsRemove)
    {
        int nSuccess = 0;
        ArrayList<String> audioPathList = new ArrayList<>();    // list of paths to pass to Classic Music Scanner

        // revert or remove any selected file
        for (int i = 0; i < mFileList.size(); i++)
        {
            if (mSelectedList[i])
            {
                DirectoryEntry de = mFileList.get(i);

                if ((mDir == null) || isAudioFile(de))
                {
                    String path = de.getFilePath();     // convert SAF paths to standard paths, if possible
                    boolean bSuccess = false;

                    if (bIsRemove)
                    {
                        bSuccess = de.delete();
                    }
                    else
                    {
                        String name = de.getName();
                        DirectoryEntry bde = findFile(name + ".backup");
                        if (bde != null)
                        {
                            // This file has a corresponding ".backup" file.
                            // 1. : remove audio file
                            bSuccess = de.delete();     // will destroy de.name
                            if (bSuccess)
                            {
                                // 2.: rename backup file to audio file
                                bSuccess = bde.renameToEx(name, true);
                            }
                        }
                    }

                    if (bSuccess)
                    {
                        audioPathList.add(path);
                        nSuccess++;
                    }
                }
            }
        }

        // in case of success force reload of directory and inform media scanners
        if ((nSuccess > 0) && (mDir != null))
        {
            forceReload();
            triggerScanners(audioPathList);
        }
    }


    /**************************************************************************
     *
     * handle REVERT menu entry
     *
     * The respective files are replaced by their backups, if exist
     *
     *************************************************************************/
    protected void actionRevert()
    {
        doRemoveOrRevert(false);
    }


    /**************************************************************************
     *
     * handle REMOVE menu entry
     *
     * First opens a dialogue for confirmation
     *
     *************************************************************************/
    protected void actionRemove()
    {
        dialogRemoveFiles();
    }


    /**************************************************************************
     *
     * handle RENAME menu entry
     *
     * First opens a dialogue for new file name
     *
     *************************************************************************/
    protected void actionRename()
    {
        dialogRenameFile();
    }


    /**************************************************************************
     *
     * play a track
     *
     *************************************************************************/
    private void playNext()
    {
        int i = Math.max(mPlayingIndex, 0);
        for (; i < mSelectedList.length; i++)
        {
            if (!mSelectedList[i])
            {
                continue;   // only play selected files
            }

            DirectoryEntry de = mFileList.get(i);

            if ((mDir == null) || isAudioFile(de))
            {
                boolean bSuccess = prepareMediaPlayer(de);

                String text;
                if (bSuccess)
                {
                    text = "playing: ";
                    mMediaPlayer.start();
                    mPlayingIndex = i;
                }
                else
                {
                    text = "playing denied by Android: ";
                    mPlayingIndex = -1;
                }

                text += de.getName();
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();

                break;
            }
        }

        if (i >= mSelectedList.length)
        {
            mPlayingIndex = -1;     // no more tracks found
        }
    }


    /**************************************************************************
     *
     * set data source and prepare media player
     *
     * try Uri, path and MediaDataSource variants
     *
     *************************************************************************/
    boolean prepareMediaPlayer(DirectoryEntry de)
    {
        Uri uri = de.getUri();
        try
        {
            if (uri != null)
            {
                //Log.d(LOG_TAG, "playNext() : setDataSource(\"" + uri.toString() + "\")");
                mMediaPlayer.setDataSource(this, uri);
            } else
            {
                //Log.d(LOG_TAG, "playNext() : setDataSource(\"" + de.getPath() + "\")");
                mMediaPlayer.setDataSource(de.getPath());
            }
        } catch (IOException e)
        {
            if (uri == null)
            {
                Log.e(LOG_TAG, "Error on setDataSource(path)", e);
                return false;
            }

            if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.M)
            {
                Log.e(LOG_TAG, "Error on setDataSource(Uri)", e);
                return false;
            }

            // With Uri and Android 6, we retry with byte buffer
            Log.e(LOG_TAG, "Error on setDataSource(Uri), reason: \"" + e.getMessage() + "\", retrying with byte buffer");
            try
            {
                MyMediaDataSource dataSource = new MyMediaDataSource(getContentResolver(), uri);
                mMediaPlayer.setDataSource(dataSource);
            } catch (IOException e2)
            {
                Log.e(LOG_TAG, "Error on setDataSource(byteBuffer)", e2);
                return false;
            }
        } catch (Exception e)
        {
            // handle other exceptions
            Log.e(LOG_TAG, "Error on setDataSource(path)", e);
            return false;
        }

        try
        {
            mMediaPlayer.prepare();
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "Error on prepare()", e);
            return false;
        }

        return true;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private void updatePlayState()
    {
        if (mPlayingIndex >= 0)
        {
            mbMenuItemPlayEnabled = true;   // enable menu item ...
            mbMenuItemStopEnabled = true;   // ... and show STOP, not PLAY
        } else
        {
            mbMenuItemPlayEnabled = (mNumOfSelectedFiles > 0);
            mbMenuItemStopEnabled = false;   // if enabled, show PLAY
        }
    }


    /**************************************************************************
     *
     * handle PLAY menu entry
     *
     * Tries to play the first selected file.
     * If playing, stop it.
     *
     *************************************************************************/
    protected void actionPlay()
    {
        if ((mMediaPlayer != null) && (mPlayingIndex >= 0))
        {
            // STOP function
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mPlayingIndex = -1;
        }
        else
        {
            // PLAY function
            if (mMediaPlayer == null)
            {
                mMediaPlayer = new MediaPlayer();       // if there is already one, just discard it
                mMediaPlayer.setOnCompletionListener(this);
            }

            mPlayingIndex = 0;
            playNext();
        }

        updatePlayState();
    }


    /**************************************************************************
     *
     * handle BACK key for directory navigation
     *
     *************************************************************************/
    @Override
    protected boolean handleBackKey()
    {
        //noinspection StatementWithEmptyBody
        if (mDir != null)
        {
            if (mDir.isRoot())
            {
                if (!(mDir instanceof DirectoryTreeVirtualRoot))
                {
                    if (gotoVirtualRoot())
                    {
                        // go back to virtual root
                        chDir(null);
                        return true;
                    }
                }

                if (!mBackKeyAlreadyPressed)
                {
                    Toast.makeText(getApplicationContext(), R.string.str_press_back_key_twice, Toast.LENGTH_SHORT).show();
                    mBackKeyAlreadyPressed = true;
                    return true;
                }
            }
            else
            {
                DirectoryTree.DirectoryEntry de = mDir.gotoParent();
                if (de != null)
                {
                    chDir(de);
                }
                return true;    // event was consumed
            }
        }
        else
        {
            // in file list view just leave
        }
        return false;
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    private boolean isEditable(DirectoryEntry de)
    {
        return de.isSeekable(getContentResolver()) != DirectoryEntry.opResult.ERROR;
    }


    /**************************************************************************
     *
     * start edit activity on green floating button or via next/previous file
     *
     ************************************************************************
     * @noinspection CommentedOutCode*/
    private void startEditActivity()
    {
        Log.d(LOG_TAG, "startEditActivity()");

        mbAsyncTaskBusy = false;
        mProgressBar.setVisibility(View.GONE);

        ArrayList<String> audioEditPathList = new ArrayList<>();
        ArrayList<String> audioEditUriList = new ArrayList<>();
        int indexOfAudioFiles = -1;
        int indexOfFirstAudioFile = -1;
        int numOfNotEditableFiles = mNumOfSelectedFiles - mNumOfEditableFiles;
        int numOfNotBackupableFiles = 0;
        int numOfNonAudioFiles = 0;
        Log.d(LOG_TAG, "startEditActivity(): numOfNotEditableFiles = " + numOfNotEditableFiles);

        if (mEditableList == null)
        {
            Log.e(LOG_TAG, "startEditActivity(): mEditableList is null");
        }

        for (int i = 0; i < mSelectedList.length; i++)
        {
            DirectoryEntry de = mFileList.get(i);
            boolean isSelectedAndEditable = ((mEditableList == null) || (mEditableList[i]));
            //Log.d(LOG_TAG, "startEditActivity(): check file " + de.getName());
            //Log.d(LOG_TAG, "startEditActivity(): isSelectedAndEditable = " + isSelectedAndEditable);

            if (isAudioFile(de))
            {
                indexOfAudioFiles++;        // this is a kind of sub-list in the list of files

                // mEditableList[] has been filled during AsyncTask,
                // note that these also include the mSelectedList[i] flag
                if (!isSelectedAndEditable)
                {
                    continue;
                }

                if (indexOfFirstAudioFile < 0)
                {
                    indexOfFirstAudioFile = indexOfAudioFiles;
                }

                if (de instanceof DirectoryTreeSaf.DirectoryEntrySaf)
                {
                    // We need both Uri and and parent Uri, thus we concatenate them, separated with LF
                    String s = de.toString();
                    DirectoryEntry parent = de.getParent();
                    if (parent.toString().isEmpty())
                    {
                        Log.w(LOG_TAG, "startEditActivity(): file has no parent: " + de.getName());
                        numOfNotBackupableFiles++;
                    }
                    else
                    {
                        //noinspection UnnecessaryToStringCall
                        s = s + "\n" + parent.toString();
                    }
                    audioEditUriList.add(s);
                }
                else
                {
                    audioEditPathList.add(de.toString());
                }
            }
            else
            if (mSelectedList[i])
            {
                numOfNonAudioFiles++;
                Log.w(LOG_TAG, "startEditActivity(): file is not a supported audio file: " + de.getName());
            }
        }

        if (numOfNonAudioFiles > 0)
        {
            final String message = getString(R.string.str_files_non_audio);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.str_warning))
                    .setIcon(android.R.drawable.ic_dialog_alert)    // drawables: /Android/sdk/platforms/android-26/data/res
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        }

        if (numOfNotEditableFiles - numOfNonAudioFiles > 0)
        {
            final String message = getString(R.string.str_all_files_seek_unsupported);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.str_warning))
                    .setIcon(android.R.drawable.ic_dialog_alert)    // drawables: /Android/sdk/platforms/android-26/data/res
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        }

        if (numOfNotBackupableFiles > 0)
        {
            final String message = getString(R.string.str_files_tree_unsupported);
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        }

        if (audioEditPathList.size() + audioEditUriList.size() != 0)
        {
            Intent intent = new Intent(mThePackageContext, TagsActivity.class);
            intent.setAction(android.content.Intent.ACTION_VIEW);
            // pass table of paths or table of URIs
            if (!audioEditPathList.isEmpty())
            {
                intent.putExtra("pathTable", audioEditPathList);
            }
            if (!audioEditUriList.isEmpty())
            {
                intent.putExtra("uriTable", audioEditUriList);
            }
            intent.putExtra("tableCount", mNumOfAudioFiles);
            intent.putExtra("tablePos", indexOfFirstAudioFile);
            if (mDir != null)
            {
                intent.putExtra("directory", mDir.toString());
            }
            //startActivityForResult(intent, REQUEST_TAGGER);
            mTaggerActivityLauncher.launch(intent);
        }
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    void deferredScannerHandling()
    {
        // deferred scanner handling
        ArrayList<String> deferredAudioPathList = sDeferredAudioPathList;
        sDeferredAudioPathList = null;
        if (deferredAudioPathList != null)
        {
            triggerScanners(deferredAudioPathList);
        }
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     * ActivityResultContracts.StartActivityForResult(): An
     * ActivityResultContract that doesn't do any type conversion,
     * taking raw Intent as an input and ActivityResult as an output.
     *
     *************************************************************************/
    private void registerRequestTaggerCallback()
    {
        mTaggerActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
                {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    // Intent data = result.getData();

                    Log.d(LOG_TAG, "registerRequestTaggerCallback::onActivityResult() : resultCode = " + resultCode);
                    if (resultCode == RESULT_TAGGER_NEXT)
                    {
                        // start editor with next item, if appropriate
                        if (selectNextOrPrevFile(1))
                        {
                            mbAsyncTaskBusy = true;
                            mProgressBar.setVisibility(View.VISIBLE);
                            new FileAccessTask().execute(2);
                            // startEditActivity();
                        }
                    }
                    else
                    if (resultCode == RESULT_TAGGER_PREVIOUS)
                    {
                        // start editor with previous item, if appropriate
                        if (selectNextOrPrevFile(-1))
                        {
                            mbAsyncTaskBusy = true;
                            mProgressBar.setVisibility(View.VISIBLE);
                            new FileAccessTask().execute(2);
                            // startEditActivity();
                        }
                    }
                    else
                    if (resultCode == RESULT_TAGGER_DONE)
                    {
                        // force reload of directory, as a .backup file may have been created
                        forceReload();
                    }

                    deferredScannerHandling();
                }
            });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     * Note that ActivityResultContracts.OpenDocumentTree() is nonsense,
     * because we have to pass some flags in our intent:
     *
     * ActivityResultContracts.OpenDocumentTree(): An ActivityResultContract
     * to prompt the user to select a directory, returning the user selection
     * as a Uri. Apps can fully manage documents within the returned directory.
     *
     * The input is an optional Uri of the initial starting location.
     *
     *************************************************************************/
    private void registerOpenDocumentTreeCallback()
    {
        mOpenDocumentTreeActivityLauncher = registerForActivityResult(
            /* new ActivityResultContracts.OpenDocumentTree(), NONSENSE */
            new ActivityResultContracts.StartActivityForResult(),
            /* new ActivityResultCallback<Uri>() NONSENSE */
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                /* public void onActivityResult(Uri treeUri) NONSENSE */
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        Uri treeUri = data.getData();
                        if (treeUri != null)
                        {
                            Log.d(LOG_TAG, " URI = " + treeUri.getPath());

                            grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                            Context context = getApplicationContext();
                            mDir = new DirectoryTreeSaf(context, treeUri);    // specific to SAF
                            // force reload of directory
                            forceReload();
                        }

                        // deferredScannerHandling();   necessary?
                    }
                }
            });
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     * TODO: Maybe ActivityResultContracts.GetMultipleContents or
     *       OpenMultipleDocuments would be the best solution.
     *
     *************************************************************************/
    private void registerOpenDocumentCallback()
    {
        mOpenDocumentActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    int resultCode = result.getResultCode();
                    Intent data = result.getData();

                    if ((resultCode == RESULT_OK) && (data != null))
                    {
                        ArrayList<UriOrPath> newObjectList = new ArrayList<>();
                        ClipData clipData = data.getClipData();
                        if (clipData != null)
                        {
                            for (int i = 0; i < clipData.getItemCount(); i++)
                            {
                                Uri theUri = clipData.getItemAt(i).getUri();
                                Log.d(LOG_TAG, " URI[" + i + "] = " + theUri);
                                grantUriPermission(getPackageName(), theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                                getContentResolver().takePersistableUriPermission(theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                                UriOrPath newObject = new UriOrPath(theUri);
                                newObjectList.add(newObject);
                            }
                        }

                        Uri theUri = data.getData();
                        if (theUri != null)
                        {
                            UriOrPath newObject = new UriOrPath(theUri);
                            Log.d(LOG_TAG, " URI = " + theUri);
                            grantUriPermission(getPackageName(), theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                            newObjectList.add(newObject);
                        }

                        if (!newObjectList.isEmpty())
                        {
                            enterFileListMode(newObjectList);
                        }
                        // deferredScannerHandling();   necessary?
                    }
                }
            });
    }


    /**************************************************************************
     *
     * callback for requestForPermission30()
     *
     * This is for Android 11 and newer and handles MANAGE_EXTERNAL_STORAGE.
     *
     *************************************************************************/
    private void registerStorageAccessPermissionCallback()
    {
        mStorageAccessPermissionActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                @RequiresApi(api = Build.VERSION_CODES.R)
                public void onActivityResult(ActivityResult result)
                {
                    boolean bReadPermissionGrantedNew;
                    boolean bWritePermissionGrantedNew;

                    // Note that the resultCode is not helpful here, fwr
                    if (Environment.isExternalStorageManager())
                    {
                        Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): r/w permission granted");
                        bReadPermissionGrantedNew = true;
                        bWritePermissionGrantedNew = true;
                    }
                    else
                    {
                        Log.d(LOG_TAG, "registerStorageAccessPermissionCallback(): w permission denied");
                        // Note that file read access may still be possible via READ_EXTERNAL_STORAGE or READ_MEDIA_AUDIO
                        bReadPermissionGrantedNew = mFileReadPermissionGranted;
                        bWritePermissionGrantedNew = false;
                    }

                    if ((bReadPermissionGrantedNew != mFileReadPermissionGranted) ||
                        (bWritePermissionGrantedNew != mFileWritePermissionGranted))
                    {
                        // just granted or denied
                        mFileReadPermissionGranted = bReadPermissionGrantedNew;
                        mFileWritePermissionGranted = bWritePermissionGrantedNew;
                        onFileAccessModesChanged();
                        if (!mFileWritePermissionGranted)
                        {
                            Toast.makeText(getApplicationContext(), R.string.str_permission_denied, Toast.LENGTH_LONG).show();
                        }
                    }
                }
            });
    }


    /**************************************************************************
     *
     * select next or previous file, if any and if only one selected
     * return true in case of success
     *
     *************************************************************************/
    private boolean selectNextOrPrevFile(int idelta)
    {
        Log.d(LOG_TAG, "selectNextFile()");
        if (mNumOfSelectedFiles != 1)
        {
            //Log.d(LOG_TAG, "selectNextFile() : leave because mNumOfSelectedFiles = " + mNumOfSelectedFiles);
            return false;
        }

        int deselectItem = -1;
        int selectItem = -1;

        int istart;
        if (idelta > 0)
        {
            istart = 0;
        }
        else
        {
            istart = mSelectedList.length - 1;
        }

        for (int i = istart; (i >= 0) && (i < mSelectedList.length); i += idelta)
        {
            if (deselectItem < 0)
            {
                if (mSelectedList[i])
                {
                    // this is the first (and only) selected item
                    deselectItem = i;
                }
            }
            else
            {
                if (mDir == null)
                {
                    selectItem = i;
                    break;
                } else
                {
                    DirectoryEntry de = mFileList.get(i);
                    if (isAudioFile(de))
                    {
                        selectItem = i;
                        break;
                    }
                }
            }
        }

        if (selectItem >= 0)
        {
            mSelectedList[deselectItem] = false;   // unselect previous
            mSelectedList[selectItem] = true;   // select current
            // force redraw
            mPathListView.setAdapter(mPathListAdapter);
            return true;
        }
        else
        {
            // last item already selected
            return false;
        }
    }


    /**************************************************************************
     *
     * helper to start SAF file selector for a document tree
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void startSafPickerForDocumentTree()
    {
        final String intent_code = Intent.ACTION_OPEN_DOCUMENT_TREE;
        Intent intent = new Intent(intent_code);

        // specify initial directory tree position
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            if (mDir != null)
            {
                String uriString = mDir.getCurrentUriAsString();
                if (uriString != null)
                {
                    Uri uri = Uri.parse(uriString /*"content://com.android.externalstorage.documents/document/primary%3AMusic"*/);
                    intent.putExtra(DocumentsContract.EXTRA_INITIAL_URI, uri);
                }
            }
        }

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION +
                Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        mOpenDocumentTreeActivityLauncher.launch(intent);
        // deprecated method:
        //startActivityForResult(intent, REQUEST_DIRECTORY_SELECT);
    }


    /**************************************************************************
     *
     * helper to start SAF file selector for a single document
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void startSafPickerForDocuments()
    {
        final String intent_code = Intent.ACTION_OPEN_DOCUMENT;
        Intent intent = new Intent(intent_code);

        // Ask for read and write access to files and sub-directories in the user-selected directory.
        intent.addFlags(
                Intent.FLAG_GRANT_READ_URI_PERMISSION +
                        Intent.FLAG_GRANT_WRITE_URI_PERMISSION +
                        Intent.FLAG_GRANT_PREFIX_URI_PERMISSION +
                        Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        intent.putExtra("android.content.extra.SHOW_ADVANCED", true);

        intent.setType("audio/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        mOpenDocumentActivityLauncher.launch(intent);
        // deprecated method:
        //startActivityForResult(intent, REQUEST_DOCUMENT_SELECT);
    }


    /* *************************************************************************
     *
     * method from Activity
     *
     * This is called when a secondary activity has ended.
     * This activity is either our TagsActivity or the SAF file selector.
     *
     *************************************************************************/
    /* no longer needed, because startActivityForResult() is deprecated
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData)
    {
        Log.d(LOG_TAG, "onActivityResult(req = " + requestCode + ", res = " + resultCode + ")");

        switch (requestCode)
        {
            //
            // our own TagsActivity has ended
            //

            case REQUEST_TAGGER:
                Log.d(LOG_TAG, "onActivityResult() : resultCode = " + resultCode);
                if (resultCode == RESULT_TAGGER_NEXT)
                {
                    // start editor with next item, if appropriate
                    if (selectNextOrPrevFile(1))
                    {
                        mbAsyncTaskBusy = true;
                        mProgressBar.setVisibility(View.VISIBLE);
                        new FileAccessTask().execute(2);
//                        startEditActivity();
                    }
                }
                else
                if (resultCode == RESULT_TAGGER_PREVIOUS)
                {
                    // start editor with previous item, if appropriate
                    if (selectNextOrPrevFile(-1))
                    {
                        mbAsyncTaskBusy = true;
                        mProgressBar.setVisibility(View.VISIBLE);
                        new FileAccessTask().execute(2);
//                        startEditActivity();
                    }
                }
                else
                if (resultCode == RESULT_TAGGER_DONE)
                {
                    // force reload of directory, as a .backup file may have been created
                    forceReload();
                }
                break;

            //
            // SAF document selector has ended
            //

            case REQUEST_DOCUMENT_SELECT:
                if (resultCode == RESULT_OK)
                {
                    ArrayList<UriOrPath> newObjectList = new ArrayList<>();
                    ClipData clipData = resultData.getClipData();
                    if (clipData != null)
                    {
                        for (int i = 0; i < clipData.getItemCount(); i++)
                        {
                            Uri theUri = clipData.getItemAt(i).getUri();
                            Log.d(LOG_TAG, " URI[" + i + "] = " + theUri);
                            grantUriPermission(getPackageName(), theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            getContentResolver().takePersistableUriPermission(theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                            UriOrPath newObject = new UriOrPath(theUri);
                            newObjectList.add(newObject);
                        }
                    }

                    Uri theUri = resultData.getData();
                    if (theUri != null)
                    {
                        UriOrPath newObject = new UriOrPath(theUri);
                        Log.d(LOG_TAG, " URI = " + theUri);
                        grantUriPermission(getPackageName(), theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        getContentResolver().takePersistableUriPermission(theUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        newObjectList.add(newObject);
                    }

                    if (newObjectList.size() > 0)
                    {
                        enterFileListMode(newObjectList);
                    }
                }
                break;

            //
            // SAF directory selector has ended
            //

            case REQUEST_DIRECTORY_SELECT:
                if (resultCode == RESULT_OK)
                {
                    Uri treeUri = resultData.getData();
                    if (treeUri != null)
                    {
                        Log.d(LOG_TAG, " URI = " + treeUri.getPath());

                        grantUriPermission(getPackageName(), treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                        getContentResolver().takePersistableUriPermission(treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                        mDir = new DirectoryTreeSaf(this, treeUri);    // specific to SAF
                        // force reload of directory
                        forceReload();
                    }
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, resultData);
                break;
        }

        // deferred scanner handling
        ArrayList<String> deferredAudioPathList = sDeferredAudioPathList;
        sDeferredAudioPathList = null;
        if (deferredAudioPathList != null)
        {
            triggerScanners(deferredAudioPathList);
        }
    }
    */


    /**************************************************************************
     *
     * Service Connection method
     *
     *************************************************************************/
    @Override
    public void onServiceConnected(ComponentName name, IBinder service)
    {
        Log.d(LOG_TAG, "onServiceConnected()");
        mbScannerServiceConnected = true;
    }


    /**************************************************************************
     *
     * Service Connection method
     *
     *************************************************************************/
    @Override
    public void onServiceDisconnected(ComponentName name)
    {
        Log.d(LOG_TAG, "onServiceDisconnected()");
        mbScannerServiceConnected = false;
    }


    /**************************************************************************
     *
     * warning dialogue for missing SAF access
     *
     *************************************************************************/
    private void hintMissingSafPermission()
    {
        final String strTitle = getString(R.string.str_hints_title);
        final String strDescription = getString(R.string.str_saf_hints);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setMessage(strDescription);
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * Dialogue
     *
     *************************************************************************/
    private void dialogRemoveFiles()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_remove_selected_files);
        builder.setMessage(R.string.str_files_will_be_removed);
        builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                Toast.makeText(getApplicationContext(), R.string.str_files_are_removed, Toast.LENGTH_SHORT).show();
                doRemoveOrRevert(true);
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    /**************************************************************************
     *
     * ask for new name and rename file
     *
     *************************************************************************/
    private void dialogRenameFile()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_rename_selected_file));
        int index = getSelectedIndex();

        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if ((index >= 0) && (inflater != null))
        {
            DirectoryEntry de = mFileList.get(index);
            @SuppressLint("InflateParams") final View view = inflater.inflate(R.layout.dlg_rename_file, null);
            builder.setView(view);
            final EditText textItem = view.findViewById(R.id.edit_filename);
            textItem.setText(de.getName());

            builder.setPositiveButton(getString(R.string.str_ok), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface arg0, int arg1)
                {
                    Log.d(LOG_TAG, "dialogRenameFile() : OK");
                    //mPendingDialog = null;       // test code
                    final String name = textItem.getText().toString();
                    if (!name.isEmpty() && !name.equals(de.getName()))
                    {
                        Log.d(LOG_TAG, "dialogRenameFile() : new name " + name);
                        if (de.renameToEx(name, true))
                        {
                            mAudioPathArrayList.set(index, de.getPath());
                            forceReload();
                        }
                    }
                }
            });

            builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    //mPendingDialog = null;       // test code
                    Log.d(LOG_TAG, "dialogRenameFile() : Cancel");
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            //mPendingDialog = alertDialog;       // test code
        }
    }


    /**************************************************************************
     *
     * trigger Android media scanner for file
     *
     * avoid error message by following this hint:
     * https://stackoverflow.com/questions/8665641/mediascanner-serviceconnectionleaked
     *
     *************************************************************************/
    private void triggerMediaScanner(String path)
    {
        /*
        MediaScannerConnection.scanFile(this,
                new String[]{path}, null,
                new MediaScannerConnection.OnScanCompletedListener()
                {
                    public void onScanCompleted(String path, Uri uri)
                    {
                        Log.i("ExternalStorage", "Scanned " + path + ":");
                        Log.i("ExternalStorage", "-> uri=" + uri);
                    }
                });
                */
        File theFile = new File(path);
        Uri contentUri = Uri.fromFile(theFile);
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }


    /**************************************************************************
     *
     * Start Classical Music Scanner with the provided path list
     *
     *************************************************************************/
    private void startScanner(final ArrayList<String> audioPathList)
    {
        if (!audioPathList.isEmpty())
        {
            final String packageN = "de.kromke.andreas.mediascanner";

            boolean bLaunchActivity = UserSettings.getBool(UserSettings.PREF_DBG_SCANNER_FG, false);
            if (bLaunchActivity)
            {
                // create an Intent with CATEGORY_LAUNCHER and ACTION_MAIN
                Intent theIntent = getPackageManager().getLaunchIntentForPackage(packageN);
                if (theIntent != null)
                {
                    /*
                    // if you want to modify the intent:
                    theIntent.removeCategory(Intent.CATEGORY_LAUNCHER);
                    theIntent.addCategory(Intent.CATEGORY_DEFAULT);
                    theIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
                     */
                    theIntent.putExtra("pathTable", audioPathList);
                    startActivity(theIntent);
                } else
                {
                    Log.d(LOG_TAG, "startScanner(): package not installed");
                }
            } else
            {
                final String ServiceN = ".BackgroundService";

                Intent theIntent = new Intent();
                theIntent.setComponent(new ComponentName(packageN, packageN + ServiceN));
                theIntent.putExtra("pathTable", audioPathList);
                try
                {
                    /*ComponentName c = */

                    if (!mbScannerServiceConnected)
                    {
                        // starting with API 26 (Android 8.0) background services are limited if not bound
                        boolean bRes = bindService(theIntent, this, Context.BIND_AUTO_CREATE);
                        if (bRes)
                        {
                            Log.d(LOG_TAG, "startScanner(): bindService() successful");
                        }
                        else
                        {
                            Log.e(LOG_TAG, "startScanner(): bindService() failed");
                        }
                    }
                    else
                    {
                        Log.d(LOG_TAG, "startScanner(): try to start scanner service");
                        startService(theIntent);
                    }
                } catch (Exception e)
                {
                    Log.d(LOG_TAG, "startScanner(): package not installed");
                }
            }
        }
    }


    /**************************************************************************
     *
     * pass changes to both Android and own media scanner
     *
     *************************************************************************/
    protected void triggerScanners(final ArrayList<String> audioPathList)
    {
        Log.d(LOG_TAG, "triggerScanners()");
        int size = audioPathList.size();
        startScanner(audioPathList);    // our own scanner

        int i;
        for (i = 0; i < size; i++)
        {
            triggerMediaScanner(audioPathList.get(i));  // Google's scanner
        }
    }


    /**************************************************************************
     *
     * MediaPlayer interface
     *
     *************************************************************************/
    @Override
    public void onCompletion(MediaPlayer mp)
    {
        Log.d(LOG_TAG, "onCompletion()");
        mp.reset();

        // handle Activity recreation situations
        if ((mSelectedList != null) && (mFileList != null) && (mMediaPlayer != null) && (mPlayingIndex >= 0))
        {
            mPlayingIndex++;
            playNext();
            updatePlayState();
        }
        else
        {
            mPlayingIndex = -1;
        }

        Log.d(LOG_TAG, "onCompletion() -- playing index now " + mPlayingIndex);
    }


    /**************************************************************************
     *
     * async task for file operations
     *
     *************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class FileAccessTask extends AsyncTaskExecutor<Integer /* do */, Integer /* pre */, Integer /* post */>
    {
        int actionCode = 0;

        @Override
        protected void onPreExecute()
        {
            Log.d(LOG_TAG, "onPreExecute()");
        }

        @Override
        protected Integer doInBackground(Integer... params)
        {
            actionCode = params[0];
            Log.d(LOG_TAG, "doInBackground(" + actionCode + ")");
            if (actionCode == 1)
            {
                mFileList = mDir.getChildren();
            }
            else
            if (actionCode == 2)
            {
                mEditableList = new boolean[mAudioPathArrayList.size()];
                mNumOfEditableFiles = 0;
                for (int i = 0; i < mSelectedList.length; i++)
                {
                    boolean bIsEditable = mSelectedList[i];
                    if (bIsEditable)
                    {
                        DirectoryEntry de = mFileList.get(i);
                        bIsEditable = isAudioFile(de) && isEditable(de);
                    }
                    mEditableList[i] = bIsEditable;
                    if (bIsEditable)
                    {
                        mNumOfEditableFiles++;
                    }
                }
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result)
        {
            if (actionCode == 1)
            {
                MainActivity.this.populateFiles2(result);
            }
            else
            if (actionCode == 2)
            {
                MainActivity.this.startEditActivity();
            }
        }
    }
}
