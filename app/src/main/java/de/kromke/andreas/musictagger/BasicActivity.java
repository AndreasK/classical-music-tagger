/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.musictagger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import de.kromke.andreas.utilities.MyFileUsingFile;

import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;


/**
 * base class of all our activities
 * @noinspection JavadocBlankLines, RedundantSuppression, JavadocLinkAsPlainText, CommentedOutCode
 */

@SuppressWarnings("Convert2Lambda")
@SuppressLint("Registered")
// @noinspection JavadocBlankLines
public class BasicActivity extends AppCompatActivity
{
    private static final String LOG_TAG = "CMT : BasicActivity";
    //protected static final int REQUEST_SETTINGS = 1;
    //protected static final int REQUEST_TAGGER = 2;
    protected static final int RESULT_TAGGER_CANCEL = 0;
    protected static final int RESULT_TAGGER_ERROR = 1;
    protected static final int RESULT_TAGGER_DONE = 2;
    protected static final int RESULT_TAGGER_NEXT = 3;
    protected static final int RESULT_TAGGER_PREVIOUS = 4;
    //protected static final int REQUEST_DIRECTORY_SELECT = 5;
    //protected static final int REQUEST_DOCUMENT_SELECT = 6;
    protected MenuItem mMenuItemManageFiles;
    protected MenuItem mMenuItemSafTreeSelect;
    protected MenuItem mMenuItemSafDocSelect;
    protected MenuItem mMenuItemRevert;         // must be dynamically enabled and disabled
    protected MenuItem mMenuItemRemove;         // must be dynamically enabled and disabled
    protected MenuItem mMenuItemRename;         // must be dynamically enabled and disabled
    protected MenuItem mMenuItemPlay;           // must be dynamically enabled and disabled
    protected boolean mbMenuItemSafSelectEnabled = false;
    protected boolean mbMenuItemRevertEnabled = false;   // handled by derived class
    protected boolean mbMenuItemRemoveEnabled = false;   // handled by derived class
    protected boolean mbMenuItemRenameEnabled = false;   // handled by derived class
    protected boolean mbMenuItemPlayEnabled = false;     // handled by derived class
    protected boolean mbMenuItemStopEnabled = false;     // handled by derived class
    static ArrayList<String> sDeferredAudioPathList = null;     // paths to be sent to scanner
    ActivityResultLauncher<Intent> mPreferencesActivityLauncher;
    private String mStoredBasePath = null;



    /************************************************************************************
     *
     * Activity method: create the activity, holding all tabs
     *
     *  Note that this is also called in case the device had been turned, or
     *  in case the device had been shut off for a while. In this case we only have
     *  to re-initialise the GUI, but not the tables, database etc.
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        UserSettings.setContext(this);

        registerPreferencesCallback();

        // Check if app has been updated. If yes, show list of changes.
        // If preference value does not exist, yet, also show the dialogue

        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);
        int newVersionCode = info.versionCode;
        int oldVersionCode = UserSettings.updateValStoredAsString(UserSettings.PREF_INSTALLED_APP_VERSION, newVersionCode, -1);
        if (oldVersionCode != newVersionCode)
        {
            DialogChanges();
        }

        // TODO: find a better solution for this initialisation:
        MyFileUsingFile.cacheDirectory = getCacheDir();
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * This is called once, while onPrepareOptionsMenu is called everytime
     * the menu opens.
     *
     *************************************************************************/
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        mMenuItemRevert = menu.findItem(R.id.action_revert);
        mMenuItemRemove = menu.findItem(R.id.action_remove);
        mMenuItemRename = menu.findItem(R.id.action_rename);
        mMenuItemPlay = menu.findItem(R.id.action_play);
        mMenuItemManageFiles = menu.findItem(R.id.action_manage_external_files);
        mMenuItemSafTreeSelect = menu.findItem(R.id.action_saf_select_tree);
        mMenuItemSafDocSelect = menu.findItem(R.id.action_saf_select_doc);
        return true;
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * called everytime the menu opens
     * Enable and disable menu items according to state and context
     *
     *************************************************************************/
    @Override
    public boolean onPrepareOptionsMenu(Menu menu)
    {
        mMenuItemRevert.setEnabled(mbMenuItemRevertEnabled);
        mMenuItemRemove.setEnabled(mbMenuItemRemoveEnabled);
        mMenuItemRename.setEnabled(mbMenuItemRenameEnabled);
        mMenuItemPlay.setEnabled(mbMenuItemPlayEnabled);
        mMenuItemSafTreeSelect.setEnabled(mbMenuItemSafSelectEnabled);
        mMenuItemSafDocSelect.setEnabled(mbMenuItemSafSelectEnabled);
        int res = mbMenuItemStopEnabled ? R.string.action_stop : R.string.action_play;
        String text = getString(res);
        mMenuItemPlay.setTitle(text);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
        {
            mMenuItemManageFiles.setCheckable(true);
            mMenuItemManageFiles.setEnabled(true);
            //noinspection RedundantIfStatement
            if (Environment.isExternalStorageManager())
            {
                // full file access already granted
                mMenuItemManageFiles.setChecked(true);
            }
            else
            {
                mMenuItemManageFiles.setChecked(false);
            }
        }
        else
        {
            // The "manage files" permission is available for Android 10 and above
            mMenuItemManageFiles.setEnabled(false);
        }

        return super.onPrepareOptionsMenu(menu);
    }


    /* *************************************************************************
     *
     * method from Activity
     *
     * This is called when a secondary activity has ended.
     *
     *************************************************************************/
    /* no longer needed, because startActivityForResult() is deprecated
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent resultData)
    {
        Log.d(LOG_TAG, "onActivityResult(req = " + requestCode + ", res = " + resultCode + ")");
        super.onActivityResult(requestCode, resultCode, resultData);

        if (resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case REQUEST_SETTINGS:
                    //showUserSettings();
                    break;

                case REQUEST_DOCUMENT_SELECT:
                    ClipData clipData = resultData.getClipData();
                    if (clipData != null)
                    {
                        // also check data.data because if the user select one file the file and uri will be in  data.data and data.getClipData() will be null
                        for (int i = 0; i < clipData.getItemCount(); i++)
                        {
                            Uri theUri = clipData.getItemAt(i).getUri();
                            Log.d(LOG_TAG, " URI[" + i + "] = " + theUri);
                        }
                    }
                // fall through;

                case REQUEST_DIRECTORY_SELECT:
                    Uri treeUri = resultData.getData();
                    if (treeUri != null)
                    {
                        String thePath = treeUri.getPath();
                        Log.d(LOG_TAG, " URI = " + thePath);
                    }
                    break;

            }
        }
    }
    */


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogAbout()
    {
        UserSettings.AppVersionInfo info = UserSettings.getVersionInfo(this);

        final String strTitle = "Classical Music Tagger";
        final String strDescription = getString(R.string.str_app_description);
        final String strAuthor = getString(R.string.str_author);
        @SuppressWarnings("ConstantConditions") boolean bIsPlayStoreVersion = ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")));
        @SuppressWarnings("ConstantConditions") final String tStore = (bIsPlayStoreVersion) ? "   [Play Store]" : "   [free]";
        final String strVersion = "Version " + info.versionName + (((info.isDebug) ? " DEBUG" : "") + tStore);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setIcon(R.drawable.app_icon_noborder);
        alertDialog.setMessage(
                strDescription + "\n\n" +
                        strAuthor + "Andreas Kromke" + "\n\n" +
                        strVersion + "\n" +
                        "(" + info.strCreationTime + ")");
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHtml(final String filename)
    {
        WebView webView = new WebView(this);
        webView.loadUrl("file:///android_asset/html-" + getString(R.string.locale_prefix) + "/" + filename);
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(webView);
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogHelp()
    {
        DialogHtml("help.html");
    }


    /**************************************************************************
     *
     * helper to show a html dialogue
     *
     *************************************************************************/
    private void DialogChanges()
    {
        DialogHtml("changes.html");
    }


    /**************************************************************************
     *
     * "Not Available in Play Store Version" dialogue
     *
     *************************************************************************/
    private void dialogNotAvailableInPlayStoreVersion()
    {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(getString(R.string.str_NeedManageFilesPermission));
        alertDialog.setMessage(getString(R.string.str_NotAvailableInPlayStoreVersion));
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.str_cancel),
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * helper to start SAF file selector, to be overridden
     *
     *************************************************************************/
    protected void startSafPickerForDocumentTree()
    {
    }


    /**************************************************************************
     *
     * helper to start SAF file selector, to be overridden
     *
     *************************************************************************/
    protected void startSafPickerForDocuments()
    {
    }


    /**************************************************************************
     *
     * helper to recreate list, after path change, to be overridden
     *
     *************************************************************************/
    protected void onBasePathChanged()
    {
    }


    /**************************************************************************
     *
     * safe way to check if string differ
     *
     *************************************************************************/
    static boolean stringsDiffer(final String s1, final String s2)
    {
        if (s1 == null)
        {
            return s2 != null;
        }
        // now s1 is not null
        return !s1.equals(s2);
    }


    /**************************************************************************
     *
     * method from Activity
     *
     * This is called when a secondary activity has ended.
     *
     *************************************************************************/
    @SuppressLint("ObsoleteSdkInt")
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.d(LOG_TAG, "onOptionsItemSelected()");
        final int id = item.getItemId();
        if (id == R.id.action_about)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_about");
            DialogAbout();
        }
        else
        if (id == R.id.action_settings)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_settings");
            // User chose the "Settings" item, show the app settings UI...
            Intent intent = new Intent(this, MyPreferenceActivity.class);
            // deprecated: startActivityForResult(intent, REQUEST_SETTINGS);
            // remember base path to be able to detect changes
            mStoredBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
            mPreferencesActivityLauncher.launch(intent);
        }
        else
        if (id == R.id.action_manage_external_files)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R)
            {
                // Android 10 and newer: MANAGE EXTERNAL FILES
                //noinspection ConstantConditions
                if ((BuildConfig.BUILD_TYPE.equals("release_play")) || (BuildConfig.BUILD_TYPE.equals("debug_play")))
                {
                    dialogNotAvailableInPlayStoreVersion();
                }
                else
                {
                    requestForPermission30();
                }
            }
            return true;
        }
        else
        if (id == R.id.action_saf_select_tree)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_select_tree");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafPickerForDocumentTree();
            }
        }
        else
        if (id == R.id.action_saf_select_doc)
            {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_select_doc");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                startSafPickerForDocuments();
            }
        }
        else
        if (id == R.id.action_changes)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_changes");
            DialogChanges();
        }
        else
        if (id == R.id.action_help)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_help");
            DialogHelp();
        }
        else
        if (id == R.id.action_reset_settings)
        {
            Log.d(LOG_TAG, "onOptionsItemSelected() -- action_reset_settings");
            String storedBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
            String defaultBasePath = getDefaultMusicBasePath();
            if (stringsDiffer(storedBasePath, defaultBasePath))
            {
                UserSettings.removeVal(UserSettings.PREF_MUSIC_BASE_PATH);
                UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, defaultBasePath);
                onBasePathChanged();
            }
            else
            {
                Toast.makeText(this, R.string.str_paths_unchanged, Toast.LENGTH_LONG).show();
            }
        }
        else
        if (id == R.id.action_revert)
        {
            actionRevert();
        }
        else
        if (id == R.id.action_remove)
        {
            actionRemove();
        }
        else
        if (id == R.id.action_rename)
        {
            actionRename();
        }
        else
        if (id == R.id.action_play)
        {
            actionPlay();
        }
        else
        if (id == R.id.action_exit)
        {
            System.exit(0);
            return true;
        }
        else
        if (id == R.id.action_open_media_storage)
        {
            showInstalledAppDetails(this, "com.android.providers.media");
            return true;
        }
        else
        if (id == android.R.id.home)
        {
            finish();           // the "<--" icon in Action Bar
            return true;
        }
        else
        {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            return super.onOptionsItemSelected(item);
        }

        return true;
    }


    /**************************************************************************
     *
     * helper for deprecated startActivityForResult()
     *
     *************************************************************************/
    private void registerPreferencesCallback()
    {
        mPreferencesActivityLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>()
            {
                @Override
                public void onActivityResult(ActivityResult result)
                {
                    // redraw if base path was changed
                    String currBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
                    if (stringsDiffer(mStoredBasePath, currBasePath))
                    {
                        mStoredBasePath = currBasePath;
                        onBasePathChanged();
                    }
                }
            });
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected void requestForPermission30()
    {
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected void actionRevert()
    {
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected void actionRemove()
    {
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected void actionRename()
    {
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected void actionPlay()
    {
    }


    /**************************************************************************
     *
     * to be overridden by child class
     *
     *************************************************************************/
    protected boolean handleBackKey()
    {
        return false;
    }


    /**************************************************************************
     *
     * handle BACK key for directory navigation
     *
     *************************************************************************/
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        Log.d(LOG_TAG, "dispatchKeyEvent()");
        int keyCode = event.getKeyCode();
        int actionCode = event.getAction();
        boolean bHandled = false;

        if ((keyCode == KeyEvent.KEYCODE_BACK) && (actionCode == KeyEvent.ACTION_UP))
        {
            bHandled = handleBackKey();
        }
        else
            //noinspection StatementWithEmptyBody
            if (keyCode == KeyEvent.KEYCODE_MENU)
            {
                // reserved for future use
            }

        //noinspection SimplifiableIfStatement
        if (bHandled)
            return true;
        else
            return super.dispatchKeyEvent(event);
    }

    // helper
    protected String getStrForFilesN(int n)
    {
        return n + " " + getString((n > 1) ? R.string.str_AudioFiles : R.string.str_AudioFile);
    }


    /**************************************************************************
     *
     * start media provider info page for fast access to "clear data"
     *
     * -> https://stackoverflow.com/questions/4421527/how-can-i-start-android-application-info-screen-programmatically
     *
     *************************************************************************/
    public static void showInstalledAppDetails(Context context, String packageName)
    {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", packageName, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }


    /**************************************************************************
     *
     * called by async task from TagsActivity
     *
     *************************************************************************/
    protected void triggerScannersAsync(final ArrayList<String> audioPathList)
    {
        Log.d(LOG_TAG, "triggerScannersAsync()");
        // do not handle immediately, but later in Activity context
        if (sDeferredAudioPathList == null)
            sDeferredAudioPathList = audioPathList;
        else
            sDeferredAudioPathList.addAll(audioPathList);
    }


    /************************************************************************************
     *
     * method to get paths of all SD cards. Note that there is no really clean way
     * to do this.
     *
     ***********************************************************************************/
    private File[] getSdCardMusicPaths()
    {
        final String prefix = "/storage/";

        // get private (!) music paths
        File[] files = getExternalFilesDirs(Environment.DIRECTORY_MUSIC);
        // entry #0 is the internal memory, the others are SD cards.
        for (int i = 0; i < files.length; i++)
        {
            File f = files[i];
            files[i] = null;

            if (i == 0)
            {
                continue;       // remove first entry, this is the internal memory
            }

            if (f == null)
            {
                Log.e(LOG_TAG, "getSdCardMusicPaths() : got null path from OS?!?");
                continue;       // bug in this Android device?
            }

            String path = f.getPath();
            Log.d(LOG_TAG, "getSdCardMusicPaths() : found external path " + path);
            if (path.startsWith(prefix))
            {
                // find first '/' after the prefix path
                int index = path.indexOf('/', prefix.length());
                if (index >= 0)
                {
                    path = path.substring(0, index + 1) + "/Music";
                    files[i] = new File(path);
                }
            }
        }

        return files;
    }


    /************************************************************************************
     *
     * get base path(s) from environment
     *
     **********************************************************************************
     * @noinspection ExtractMethodRecommender*/
    private String getDefaultMusicBasePath()
    {
        File basePath;
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
        {
            basePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);
        } else
        {
            basePath = new File("/");
        }

        File[] files = getSdCardMusicPaths();
        files[0] = basePath;

        // concat paths
        String defaultPath = "";
        boolean isFirst = true;
        for (File f:files)
        {
            if (f != null)
            {
                if (isFirst)
                {
                    isFirst = false;
                } else
                {
                    //noinspection StringConcatenationInLoop
                    defaultPath += "\n";
                }
                //noinspection StringConcatenationInLoop
                defaultPath += f.getPath();
            }
        }

        return defaultPath;
    }


    /************************************************************************************
     *
     * get base path(s) from environment or from settings
     *
     ***********************************************************************************/
    protected String getMusicBasePath()
    {
        String storedBasePath = UserSettings.getString(UserSettings.PREF_MUSIC_BASE_PATH);
        if (storedBasePath != null)
        {
            return storedBasePath;
        }

        // not set, yet. Set and store it.
        String defaultPath = getDefaultMusicBasePath();
        return UserSettings.getAndPutString(UserSettings.PREF_MUSIC_BASE_PATH, defaultPath);
    }


    /************************************************************************************
     *
     * check if path poínts to internal or any "external" memory
     *
     ***********************************************************************************/
    protected int getVolumeIndex(final String path)
    {
        // usually this will get one or two paths:
        //
        // f[0] = "/storage/emulated/0/Android/data/de.andreas.musictagger/files"
        // and if there is an SD card:
        // f[1] = "/storage/A1B2-C3D4/Android/data/de.andreas.musictagger/files"

        File[] ftab = getExternalFilesDirs(null);
        int i = 0;
        for (File f : ftab)
        {
            String apath = f.getAbsolutePath();
            Log.d(LOG_TAG, "getVolumeIndex() : check volume path " + apath);
            int index = apath.indexOf("/Android/");
            if (index >= 0)
            {
                apath = apath.substring(0, index);      // get first part of internal memory path
                if (path.startsWith(apath))
                {
                    return i;
                }
            }

            i++;
        }

        return -1;
    }



}
