/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.musictagger;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.kromke.andreas.utilities.AsyncTaskExecutor;
import de.kromke.andreas.utilities.AudioFileInfo;
import de.kromke.andreas.utilities.DirectoryTree;
import de.kromke.andreas.utilities.DirectoryTreeFile;
import de.kromke.andreas.utilities.DirectoryTreeSaf;
import de.kromke.andreas.utilities.Utility;

import static de.kromke.andreas.utilities.AudioFileInfo.*;


/** @noinspection JavadocBlankLines, CommentedOutCode, RedundantSuppression */
@SuppressWarnings("Convert2Lambda")
public class TagsActivity extends BasicActivity implements TextWatcher
{
    private static final String LOG_TAG = "CMT : TagsActivity";
    private static final int HINTS_VERSION = 0;
    FloatingActionButton mFloatingButton1;
    ProgressBar mProgressBar;
    int mTableCount = -1;
    int mTablePos = -1;
    List<DirectoryTree.DirectoryEntry> mFileList;
    ArrayList<AudioFileInfo> mAudioFileInfoList = null;
    int mNumOfWritableFiles = 0;
    int mNumOfIncompatibleFiles = 0;
    int mNumOfUnreadableFiles = 0;
    errCode mLastErrorFromTaggerLib;
    ArrayList<AudioFileInfo.AudioTags> mAudioFileTagsList = null;
    AudioFileInfo.AudioTags mAudioTagsCommon = null;        // read: used to pass results from worker thread to UI thread
    AudioTags mNewTags = null;        // write: used to pass results from UI thread to worker thread
    boolean mbAsyncTaskBusy;
    boolean mbWasLongFloatingButtonClick = false;
    static final String sVarious = "≠";
    int mNumOfChangedTags = 0;
    String[] mGenresLocalised;      // for GUI
    String[] mGenresEnglish;        // for tagger
    boolean mbExpertMode = true;
    boolean mbLocaliseGenres = true;
    boolean mbDryRun = false;
    int mTagMode = 1;
    String[] mComposers;            // currently not localised

    // action codes for FileAccessTask
    private final static int actionNone = 0;
    private final static int actionReadTags = 1;
    private final static int actionCreateBackupFiles = 2;   // preparation for writing
    private final static int actionWriteTags = 3;

    // each tag has its own editText field with background
    private static class TagInputField
    {
        int editTextResourceId;
        int editTextBackgroundResourceId;
        int tagId;
        EditText editText;
        String savedText;
        boolean bDirty;

        private TagInputField(int resId, int residb, int t)
        {
            editTextResourceId = resId;
            editTextBackgroundResourceId = residb;
            tagId = t;
            editText = null;
            savedText = null;
            bDirty = false;
        }
    }

    // the list of all tag input text fields
    private final TagInputField[] mTagInputFieldTable =
    {
                          // resource ID                // background resource ID        tag ID
        new TagInputField(R.id.albumInput,              R.id.bg_albumInput,              idAlbum),
        new TagInputField(R.id.composerInput,           R.id.bg_composerInput,           idComposer),
        new TagInputField(R.id.trackNoInput,            R.id.bg_trackNoInput,            idTrack),
        new TagInputField(R.id.trackTotalInput,         R.id.bg_trackTotalInput,         idTrackTotal),
        new TagInputField(R.id.discNoInput,             R.id.bg_discNoInput,             idDiscNo),
        new TagInputField(R.id.discTotalInput,          R.id.bg_discTotalInput,          idDiscTotal),
        new TagInputField(R.id.yearInput,               R.id.bg_yearInput,               idYear),
        new TagInputField(R.id.workInput,               R.id.bg_workInput,               idGrouping),
        new TagInputField(R.id.nameInput,               R.id.bg_nameInput,               idTitle),
        new TagInputField(R.id.subtitleInput,           R.id.bg_subtitleInput,           idSubtitle),
        new TagInputField(R.id.performerInput,          R.id.bg_performerInput,          idArtist),
        new TagInputField(R.id.albumPerformerInput,     R.id.bg_albumPerformerInput,     idAlbumArtist),
        new TagInputField(R.id.genreInput,              R.id.bg_genreInput,              idGenre),
        new TagInputField(R.id.conductorInput,          R.id.bg_conductorInput,          idConductor),
        new TagInputField(R.id.commentInput,            R.id.bg_commentInput,            idComment),
        new TagInputField(R.id.appleWorkInput,          R.id.bg_appleWorkInput,          idAppleWork),
        new TagInputField(R.id.appleMovementInput,      R.id.bg_appleMovementInput,      idAppleMovement),
        new TagInputField(R.id.appleMovementNoInput,    R.id.bg_appleMovementNoInput,    idAppleMovementNo),
        new TagInputField(R.id.appleMovementTotalInput, R.id.bg_appleMovementTotalInput, idAppleMovementTotal)
    };

    private final int[] mAppleInputTable =
    {
        R.id.appleWorkInput,
        R.id.appleMovementInput,
        R.id.appleMovementNoInput,
        R.id.appleMovementTotalInput,
        R.id.appleWorkDescription,
        R.id.appleMovementDescription,
        R.id.appleMovementNoDescription,
        R.id.appleMovementTotalDescription
    };


    /************************************************************************************
     *
     * Activity method: create the activity, holding all tabs
     *
     *  Note that this is also called in case the device had been turned, or
     *  in case the device had been shut off for a while. In this case we only have
     *  to re-initialise the GUI, but not the tables, database etc.
     *
     ***********************************************************************************/
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d(LOG_TAG, "onCreate()");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tags);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        // keyboard shall not automatically pop up, but wait for user action
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mbMenuItemSafSelectEnabled = false;

        mbExpertMode = UserSettings.getBool(UserSettings.PREF_EXPERT_MODE, true);
        if (!mbExpertMode)
        {
            // hide classic tags
            for (int aAppleInputTable : mAppleInputTable)
            {
                View theView = findViewById(aAppleInputTable);
                theView.setVisibility(View.GONE);
            }
        }
        mTagMode = UserSettings.getIntStoredAsString(UserSettings.PREF_TAG_MODE, 1);
        mbLocaliseGenres = UserSettings.getBool(UserSettings.PREF_LOCALISE_GENRES, true);
        mbDryRun = UserSettings.getBool(UserSettings.PREF_DRY_RUN, false);

        // init genre autocompletion and translation, if configured
        AutoCompleteTextView genreView = findViewById(R.id.genreInput);
        mGenresLocalised = getResources().getStringArray(R.array.genres_array);
        mGenresEnglish = getEnglishStringArray(R.array.genres_array);
        ArrayAdapter<String> genreAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, (mbLocaliseGenres) ? mGenresLocalised : mGenresEnglish);
        genreView.setAdapter(genreAdapter);

        // init composer autocompletion
        AutoCompleteTextView composerView = findViewById(R.id.composerInput);
        mComposers = getEnglishStringArray(R.array.composers_array);
        ArrayAdapter<String> composerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mComposers);
        composerView.setAdapter(composerAdapter);

        mFloatingButton1 = findViewById(R.id.fab);
        mFloatingButton1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Log.d(LOG_TAG, "mFloatingButton1.onClick()");
                if (mNumOfChangedTags > 0)
                {
                    Snackbar.make(view, getString(R.string.str_changed_tags_are_saved), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    mbWasLongFloatingButtonClick = false;
                    saveChanges();
                }
            }
        });

        mFloatingButton1.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                Log.d(LOG_TAG, "mFloatingButton1.onLongClick()");
                if (mNumOfChangedTags > 0)
                {
                    Snackbar.make(view, getString(R.string.str_changed_tags_are_saved), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    // save and advance to next file
                    mbWasLongFloatingButtonClick = true;
                    saveChanges();
                }
                else
                {
                    // do not save, advance to next file
                    setResult(RESULT_TAGGER_NEXT, null);
                    finish();
                }
                return true;
            }
        });

        mProgressBar = findViewById(R.id.progressBar);
        if (!mbAsyncTaskBusy)
        {
            mProgressBar.setVisibility(View.GONE);
        }

        if (savedInstanceState != null)
        {
            for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
            {
                aMTagInputFieldTable.savedText = savedInstanceState.getString("edittext" + aMTagInputFieldTable.editTextResourceId);
            }
        }

        Intent theIntent = getIntent();
        if (theIntent != null)
        {
            Log.d(LOG_TAG, "onCreate(): intent action is " + theIntent.getAction());
            extractFileListFromIntent(theIntent);
            if (mFileList != null)
            {
                Log.d(LOG_TAG, "onCreate(): process path arguments");
                populateAudioFiles();
            } else
            {
                // Show Alert
                Toast.makeText(getApplicationContext(),
                        "ERROR: no path argument", Toast.LENGTH_LONG)
                        .show();
                setResult(RESULT_TAGGER_ERROR, null);
                finish();
            }
            mTableCount = theIntent.getIntExtra("tableCount", -1);
            mTablePos = theIntent.getIntExtra("tablePos", -1);
        }

        int n = mFileList.size();
        if (n == 1)
        {
            TextView footerText = findViewById(R.id.footerText);
            //footerText.setText(Utility.getFilenameFromPath(mFileList.get(0).getPath()));
            footerText.setText(mFileList.get(0).getName());

            footerText.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Log.d(LOG_TAG, "footerText.onClick()");
                    if (mNumOfChangedTags <= 0)
                    {
                        // do not save, advance to next file
                        setResult(RESULT_TAGGER_NEXT, null);
                        finish();
                    }
                }
            });

            footerText.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View view)
                {
                    Log.d(LOG_TAG, "footerText.onLongClick()");
                    if (mNumOfChangedTags <= 0)
                    {
                        // do not save, go back to previous file
                        setResult(RESULT_TAGGER_PREVIOUS, null);
                        finish();
                    }
                    return true;
                }
            });
        }
        else
        {
            LinearLayout footer = findViewById(R.id.footer);
            footer.setVisibility(View.GONE);
        }

        if (actionBar != null)
        {
            // install back arrow in action bar
            actionBar.setDisplayHomeAsUpEnabled(true);
            // set subtitle .. obviously
            String subtitle;
            if ((n > 1) || (mTableCount < 0) || (mTablePos < 0))
            {
                subtitle = getStrForFilesN(n);
            }
            else
            {
                subtitle = getString(R.string.str_AudioFile) + " " + (mTablePos + 1) + "/" + mTableCount;
            }

            actionBar.setSubtitle(subtitle);
        }

        updateFloatingButton();
        Log.d(LOG_TAG, "onCreate() - done");
    }


    /**************************************************************************
     *
     * Show alert informing about write protected files
     *
     *************************************************************************/
    private void dialogWriteProtection(int n)
    {
        int m = mFileList.size();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.str_wp_title);
        String message;
        if ((m == 1) && (n == 1))
        {
            message = getString(R.string.str_wp_one);
        }
        else
        if (n == m)
        {
            message = getString(R.string.str_wp_all);
        }
        else
        {
            message = n + getString(R.string.str_wp_some);
        }
        builder.setMessage(message + "\n" + getString(R.string.str_wp_hint));

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                Log.d(LOG_TAG, "OK pressed");
            }
        });
        /*
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                Toast.makeText(getApplicationContext(), R.string.str_no_tagger, Toast.LENGTH_LONG).show();
            }
        });
         */

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    /**************************************************************************
     *
     * Extract various tables from the intent:
     *
     *    pathTable may exist or is null
     *    uriTable may exist or is null
     *     if exists, also directory Uri exists, same directory for all files
     *
     *************************************************************************/
    void extractFileListFromIntent(Intent theIntent)
    {
        mFileList = new ArrayList<>();
        int numOfReadOnlyFiles = 0;

        ArrayList<String> pathTable = theIntent.getStringArrayListExtra("pathTable");
        if (pathTable != null)
        {
            for (String path : pathTable)
            {
                DirectoryTreeFile.DirectoryEntryFile f = new DirectoryTreeFile.DirectoryEntryFile(path);
                Log.d(LOG_TAG, "extractFileListFromIntent() : got path " + f);
                mFileList.add(f);
                if (!f.canWrite())
                {
                    numOfReadOnlyFiles++;
                }
            }
        }

        ArrayList<String> uriTable = theIntent.getStringArrayListExtra("uriTable");
        if (uriTable != null)
        {
            String directoryPath = theIntent.getStringExtra("directory");

            for (String path : uriTable)
            {
                //Log.d(LOG_TAG, "extractFileListFromIntent() : got Uri from table: \"" + path + "\"");
                String[] paths = path.split("\n");  // check if Uri contains its parent
                String fpath;
                String dpath;
                if (paths.length >= 2)
                {
                    fpath = paths[0];
                    dpath = paths[1];
                }
                else
                {
                    fpath = paths[0];
                    dpath = directoryPath;
                }

                if ((dpath != null) && dpath.isEmpty())
                {
                    Log.w(LOG_TAG, "extractFileListFromIntent() : dpath is null");
                    dpath = null;
                }

                Log.d(LOG_TAG, "extractFileListFromIntent() : fpath = \"" + fpath + "\"");
                Log.d(LOG_TAG, "extractFileListFromIntent() : dpath = \"" + dpath + "\"");

                DirectoryTreeSaf.DirectoryEntrySaf f = new DirectoryTreeSaf.DirectoryEntrySaf(fpath, dpath, this);
                Log.d(LOG_TAG, "extractFileListFromIntent() : got Uri " + f);
                mFileList.add(f);
                if (!f.canWrite())
                {
                    numOfReadOnlyFiles++;
                }
            }
        }

        if (numOfReadOnlyFiles > 0)
        {
            dialogWriteProtection(numOfReadOnlyFiles);
        }
    }


    /**************************************************************************
     *
     * called after onCreate() or after the application has been put back to
     * the foreground.
     * After onStart() the method onResume() is called.
     *
     *************************************************************************/
    @Override
    protected void onStart()
    {
        Log.d(LOG_TAG, "onStart()");
        //dumpTags();
        super.onStart();
        if (UserSettings.getInt(UserSettings.PREF_HINTS_VERSION, -1) < HINTS_VERSION)
        {
            DialogHints();
        }
        Log.d(LOG_TAG, "onStart() - done");
    }


    /************************************************************************************
     *
     * Activity method: save state
     *
     * Note that we save the localised genre here.
     *
     ***********************************************************************************/
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState)
    {
        super.onSaveInstanceState(outState);
        for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
        {
            EditText editText = aMTagInputFieldTable.editText;
            if (editText != null)
            {
                String s = editText.getText().toString();
                outState.putString("edittext" + aMTagInputFieldTable.editTextResourceId, s);
            }
        }
    }


    /************************************************************************************
     *
     * Activity method: pendant to onCreate()
     *
     ***********************************************************************************/
    @Override
    protected void onDestroy()
    {
        Log.d(LOG_TAG, "onDestroy()");
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy() - done");
    }


    /************************************************************************************
     *
     * Activity method: got new intent (should not happen)
     *
     ***********************************************************************************/
    @Override
    protected void onNewIntent(Intent intent)
    {
        Log.d(LOG_TAG, "onNewIntent(): intent action is " + intent.getAction());
        super.onNewIntent(intent);
    }


    /**************************************************************************
     *
     * update action bar
     *
     *************************************************************************/
    void updateFloatingButton()
    {
        Log.d(LOG_TAG, "updateFloatingButton(): mNumOfChangedTags = " + mNumOfChangedTags);
        int id = (mNumOfChangedTags > 0) ? R.color.action_button_save_colour : R.color.action_button_save_disabled_colour;
        mbMenuItemRevertEnabled = (mNumOfChangedTags > 0);
        mbMenuItemRemoveEnabled = false;
        mbMenuItemRenameEnabled = false;
        ColorStateList tint = AppCompatResources.getColorStateList(this, id);
        mFloatingButton1.setBackgroundTintList(tint);
    }


    /**************************************************************************
     *
     * get non-localised string array
     *
     *************************************************************************/
    @NonNull
    protected String[] getEnglishStringArray(int id)
    {
        Configuration configuration = getEnglishConfiguration();

        return createConfigurationContext(configuration).getResources().getStringArray(id);
    }


    /**************************************************************************
     *
     * helper
     *
     *************************************************************************/
    @NonNull
    private Configuration getEnglishConfiguration()
    {
        Configuration configuration = new Configuration(getResources().getConfiguration());
        configuration.setLocale(new Locale("en"));
        return configuration;
    }


    /**************************************************************************
     *
     * localise genre
     *
     *************************************************************************/
    private String localiseGenre(final String genre)
    {
        if (mbLocaliseGenres)
        {
            for (int i = 0; i < mGenresEnglish.length; i++)
            {
                if (genre.equals(mGenresEnglish[i]))
                {
                    return mGenresLocalised[i];
                }
            }
        }

        return genre;
    }


    /**************************************************************************
     *
     * un-localise genre
     *
     *************************************************************************/
    private String unlocaliseGenre(final String genre)
    {
        if (mbLocaliseGenres)
        {
            for (int i = 0; i < mGenresLocalised.length; i++)
            {
                if (genre.equals(mGenresLocalised[i]))
                {
                    return mGenresEnglish[i];
                }
            }
        }

        return genre;
    }


    /**************************************************************************
     *
     * init text edit fields from tags read from audio file(s)
     *
     *************************************************************************/
    private void populateAudioFiles()
    {
        Log.d(LOG_TAG, "populateAudioFiles()");
        mAudioFileInfoList = new ArrayList<>();
        mAudioFileTagsList = new ArrayList<>();

        // run async task to read tags
        mbAsyncTaskBusy = true;
        mProgressBar.setVisibility(View.VISIBLE);

        /*
        Toast.makeText(getApplicationContext(),
                "Start reading tags ...", Toast.LENGTH_SHORT)
                .show();
                */

        new FileAccessTask().execute(actionReadTags);
    }


    /**************************************************************************
     *
     * second part of populateAudioFiles(), called after async task has finished
     *
     *************************************************************************/
    private void populateAudioFiles2()
    {
        int nRead = (mAudioFileInfoList == null) ? 0 : mAudioFileInfoList.size();
        int nCmd = (mFileList == null) ? 0 : mFileList.size();
        if (nRead < nCmd)
        {
            Toast.makeText(getApplicationContext(),
                    "WARN: Only " + nRead + " files could be opened", Toast.LENGTH_LONG)
                    .show();
        }

        /*
        Toast.makeText(getApplicationContext(),
                "... tags read", Toast.LENGTH_SHORT)
                .show();
                */

        // write text to edit input fields
        if (nRead == 0)
        {
            // no readable file, write protect all fields
            for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
            {
                aMTagInputFieldTable.editText = findViewById(aMTagInputFieldTable.editTextResourceId);
                aMTagInputFieldTable.editText.setKeyListener(null);
            }
        }
        else
        if (mAudioTagsCommon != null)
        {
            for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
            {
                aMTagInputFieldTable.editText = findViewById(aMTagInputFieldTable.editTextResourceId);

                int theTagId = aMTagInputFieldTable.tagId;
                if (!mbExpertMode && aMTagInputFieldTable.tagId == idTitle)
                {
                    // if not in expert mode, evaluate proprietary tags with high priority
                    theTagId = idCombinedMovement;
                }

                //Log.v(LOG_TAG, "populateAudioFiles() : set Text \"" + audioTagsCommon.tags[aMTagInputFieldTable.tagId] + "\"");
                if (aMTagInputFieldTable.savedText != null)
                {
                    //
                    // restore text, e.g. after device has been turned
                    //

                    String original = mAudioTagsCommon.tags[theTagId];

                    // localise genre
                    if (aMTagInputFieldTable.editTextResourceId == R.id.genreInput)
                    {
                        original = localiseGenre(original);
                    }

                    if (!aMTagInputFieldTable.savedText.equals(original))
                    {
                        // input file had been changed
                        LinearLayout theEditText = findViewById(aMTagInputFieldTable.editTextBackgroundResourceId);
                        theEditText.setBackgroundColor(Color.parseColor("#50FF0000"));  // 50% red
                        aMTagInputFieldTable.bDirty = true;
                        mNumOfChangedTags++;
                    }

                    aMTagInputFieldTable.editText.setText(aMTagInputFieldTable.savedText);
                    aMTagInputFieldTable.savedText = mAudioTagsCommon.tags[theTagId];

                    // localise genre
                    if (aMTagInputFieldTable.editTextResourceId == R.id.genreInput)
                    {
                        aMTagInputFieldTable.savedText = localiseGenre(aMTagInputFieldTable.savedText);
                    }
                }
                else
                {
                    //
                    // first call
                    //

                    aMTagInputFieldTable.savedText = mAudioTagsCommon.tags[theTagId];

                    // localise genre
                    if (aMTagInputFieldTable.editTextResourceId == R.id.genreInput)
                    {
                        aMTagInputFieldTable.savedText = localiseGenre(aMTagInputFieldTable.savedText);
                    }
                    aMTagInputFieldTable.editText.setText(aMTagInputFieldTable.savedText);
                }

                aMTagInputFieldTable.editText.addTextChangedListener(this);
                if (mNumOfWritableFiles < 1)
                {
                    // there are no writable files, so we write-protect the input fields
                    aMTagInputFieldTable.editText.setKeyListener(null);
                }
            }
        }

        updateFloatingButton();
        mbAsyncTaskBusy = false;
        mProgressBar.setVisibility(View.GONE);

        //
        // check if there are files for which tag writing is not supported, e.g. "mp4-dash"
        //

        if (mNumOfIncompatibleFiles > 0)
        {
            String message;
            if (mNumOfIncompatibleFiles >= nRead)
            {
                if (nRead > 1)
                {
                    message = getString(R.string.str_all_files_write_unsupported);
                }
                else
                {
                    message = getString(R.string.str_the_file_unsupported);
                }
                message += " " + getString(R.string.str_text_input_disabled);
            }
            else
            {
                if (mNumOfIncompatibleFiles > 1)
                {
                    message = mNumOfIncompatibleFiles + " " + getString(R.string.str_files_write_unsupported);
                }
                else
                {
                    message = getString(R.string.str_one_file_write_unsupported);
                }
            }
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.str_warning))
                    .setIcon(android.R.drawable.ic_dialog_alert)    // drawables: /Android/sdk/platforms/android-26/data/res
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        }
    }


    /**************************************************************************
     *
     * write before leaving
     *
     *************************************************************************/
    private void saveChanges()
    {
        //
        // create a new tag table with the edit text strings
        //

        mNewTags = new AudioTags();
        for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
        {
            EditText view = aMTagInputFieldTable.editText;
            if (view != null)
            {
                String s = view.getText().toString();
                if (aMTagInputFieldTable.editTextResourceId == R.id.genreInput)
                {
                    s = unlocaliseGenre(s);
                } else if ((!mbExpertMode) && (aMTagInputFieldTable.tagId == idTitle))
                {
                    // set combined movement ("n/m movement") from title input
                    mNewTags.tags[idCombinedMovement] = s;
                    // set movement tags from combined movement
                    mNewTags.normaliseTitleAndMovement(mTagMode);
                    continue;   // do not set title here
                }
                if (!mbExpertMode && (aMTagInputFieldTable.tagId >= idAppleMovement) && (aMTagInputFieldTable.tagId <= idAppleWork))
                {
                    // skip movement and work tags, because these are set by separateCombinedMovement()
                    continue;
                }

                mNewTags.tags[aMTagInputFieldTable.tagId] = s;
            }
        }


        //
        // apply to files
        //

        /*
        Toast.makeText(getApplicationContext(),
                "Start writing tags ...", Toast.LENGTH_SHORT)
                .show();
                */

        mbAsyncTaskBusy = true;
        mProgressBar.setVisibility(View.VISIBLE);

        new FileAccessTask().execute(actionCreateBackupFiles);
    }


    /**************************************************************************
     *
     * Second part of saveChanges called after async task has finished
     *
     *************************************************************************/
    private void saveChanges2(int result)
    {
        int nFailed = mNumOfWritableFiles - result;
        if (nFailed < 1)
        {
            // backup was successful, proceed with writing
            new FileAccessTask().execute(actionWriteTags);
        }
        else
        {
            mbAsyncTaskBusy = false;
            mProgressBar.setVisibility(View.GONE);

            String message = nFailed + " " + getString(R.string.str_files_backup_failed);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.str_error))
                    .setIcon(android.R.drawable.ic_dialog_alert)    // drawables: /Android/sdk/platforms/android-26/data/res
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.str_cancel), null)
                    .setNegativeButton(getString(R.string.str_proceed), new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            mbAsyncTaskBusy = true;
                            mProgressBar.setVisibility(View.VISIBLE);
                            new FileAccessTask().execute(actionWriteTags);
                        }
                    })
                    .show();
        }
    }


    /**************************************************************************
     *
     * set foreground and background colour of toast
     *
     *************************************************************************/
    @SuppressWarnings("SameParameterValue")
    private void setToastColour(Toast toast, int fg, int bg)
    {
        View view = toast.getView();
        if (view != null)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            {
                // on Android 8 the green text cannot be read because of the light background
                view.setBackgroundColor(bg);
            }
            TextView v = view.findViewById(android.R.id.message);
            if (v != null)
            {
                v.setTextColor(fg);
            }
        }
    }


    /**************************************************************************
     *
     * Third part of saveChanges called after async task has finished
     *
     *************************************************************************/
    private void saveChanges3(int result)
    {
        mbAsyncTaskBusy = false;
        mProgressBar.setVisibility(View.GONE);

        int nFailed = mNumOfWritableFiles - result;
        if (nFailed < 1)
        {
            String message = result + " " + getString(R.string.str_files_changed_successfully);
            Toast toast = Toast.makeText(getApplicationContext(),
                    message, Toast.LENGTH_LONG);
            setToastColour(toast, Color.GREEN, Color.DKGRAY);

            toast.show();
            if (mbWasLongFloatingButtonClick)
            {
                setResult(RESULT_TAGGER_NEXT, null);
            }
            else
            {
                setResult(RESULT_TAGGER_DONE, null);
            }
            finish();       // leave activity
        }
        else
        {
            String message = nFailed + " " + getString(R.string.str_files_changed_failed);
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.str_error))
                    .setIcon(android.R.drawable.ic_dialog_alert)    // drawables: /Android/sdk/platforms/android-26/data/res
                    .setMessage(message)
                    .setPositiveButton("OK", null)
                    .show();
        }
    }


    /**************************************************************************
     *
     * callback for text field input
     *
     *************************************************************************/
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    /**************************************************************************
     *
     * callback for text field input
     *
     *************************************************************************/
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    /**************************************************************************
     *
     * callback for text field input
     *
     *************************************************************************/
    public void afterTextChanged(Editable editable)
    {
        for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
        {
            if (aMTagInputFieldTable.editText.isFocused())
            {
                //Log.v(LOG_TAG, "afterTextChanged(): text field changed " + aMTagInputFieldTable.tagId);
                LinearLayout theEditText = findViewById(aMTagInputFieldTable.editTextBackgroundResourceId);
                String s = aMTagInputFieldTable.editText.getText().toString();
                int theColor;
                boolean bDirty = !aMTagInputFieldTable.savedText.equals(s);
                //Log.v(LOG_TAG, "afterTextChanged(): curr : \"" + s + "\"");
                //Log.v(LOG_TAG, "afterTextChanged(): saved: \"" + aMTagInputFieldTable.savedText + "\"");
                if (bDirty)
                {
                    theColor = Color.parseColor("#50FF0000");  // 50% red
                    if (!aMTagInputFieldTable.bDirty)
                    {
                        aMTagInputFieldTable.bDirty = true;
                        mNumOfChangedTags++;
                    }
                }
                else
                {
                    theColor = Color.parseColor("#00FF0000");  // transparent
                    if (aMTagInputFieldTable.bDirty)
                    {
                        aMTagInputFieldTable.bDirty = false;
                        mNumOfChangedTags--;
                    }
                }
                theEditText.setBackgroundColor(theColor);
                updateFloatingButton();
                break;
            }
        }
    }

    /*
    public void dumpTags()
    {
        for (TagInputField aMTagTable : mTagInputFieldTable)
        {
            EditText view = aMTagTable.editText;
            if (view != null)
            {
                String s = view.getText().toString();
                Log.d(LOG_TAG, "dumpTags(): getText() => \"" + s + "\"");
            }
        }
    }
    */


    /**************************************************************************
     *
     * ask if end app
     *
     *************************************************************************/
    private void dialogEndApp()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.str_loseUnsavedChanges));
        builder.setMessage(R.string.str_tags_are_changed);
        builder.setPositiveButton(R.string.str_leave, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface arg0, int arg1)
            {
                setResult(RESULT_TAGGER_CANCEL, null);
                finish();
            }
        });

        builder.setNegativeButton(getString(R.string.str_cancel), new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    /**************************************************************************
     *
     * handle REVERT menu entry
     *
     * The respective tag input fields are reset
     *
     *************************************************************************/
    protected void actionRevert()
    {
        boolean bDone = false;

        int theColor = Color.parseColor("#00FF0000");  // transparent
        for (TagInputField aMTagInputFieldTable : mTagInputFieldTable)
        {
            LinearLayout theEditText = findViewById(aMTagInputFieldTable.editTextBackgroundResourceId);
            if (aMTagInputFieldTable.bDirty)
            {
                aMTagInputFieldTable.editText.setText(aMTagInputFieldTable.savedText);
                aMTagInputFieldTable.bDirty = false;
                mNumOfChangedTags--;
                theEditText.setBackgroundColor(theColor);
                bDone = true;
            }
        }

        if (bDone)
        {
            if (mNumOfChangedTags != 0)
            {
                Log.e(LOG_TAG, "actionRevert() : revert failed");
            }
            updateFloatingButton();
        }
    }


    /**************************************************************************
     *
     * ignore keys during file operations
     *
     *************************************************************************/
    @Override
    public boolean dispatchKeyEvent(KeyEvent event)
    {
        Log.d(LOG_TAG, "dispatchKeyEvent()");
        if (mbAsyncTaskBusy)
        {
            Log.d(LOG_TAG, "dispatchKeyEvent(): ignore, because reading is busy");
            return true;        // eat the key
        }
        else
            return super.dispatchKeyEvent(event);
    }


    /**************************************************************************
     *
     * handle BACK key for leaving activity
     *
     *************************************************************************/
    @Override
    protected boolean handleBackKey()
    {
        if (mNumOfChangedTags > 0)
        {
            dialogEndApp();
            return true;
        }
        return false;
    }


    /**************************************************************************
     *
     * async task has finished
     *
     * The result is passed from doInBackground(), here it is the number
     * of successfully read files, the other ones are corrupt
     *
     *************************************************************************/
    private void onAsyncReadFinished(Integer result)
    {
        Log.d(LOG_TAG, "onAsyncReadFinished(" + result + ")");
        populateAudioFiles2();
    }


    /**************************************************************************
     *
     * async task has finished with success (0) or error
     *
     * The result is passed from doInBackground(), here it is the number
     * of successfully written files.
     *
     *************************************************************************/
    private void onAsyncBackupFinished(Integer result)
    {
        Log.d(LOG_TAG, "onAsyncBackupFinished(" + result + ")");
        saveChanges2(result);
    }


    /**************************************************************************
     *
     * async task has finished with success (0) or error
     *
     * The result is passed from doInBackground(), here it is the number
     * of successfully written files.
     *
     *************************************************************************/
    private void onAsyncWriteFinished(Integer result)
    {
        Log.d(LOG_TAG, "onAsyncWriteFinished(" + result + ")");
        if (result == null)
        {
            result = 0;     // exception occurred?
        }
        saveChanges3(result);
    }


    /**************************************************************************
     *
     * dialogue
     *
     *************************************************************************/
    private void DialogHints()
    {
        final String strTitle = getString(R.string.str_hints_title);
        final String strDescription = getString(R.string.str_hints);

        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(strTitle);
        alertDialog.setMessage(strDescription);
        alertDialog.setCancelable(true);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int which)
                    {
                        UserSettings.putVal(UserSettings.PREF_HINTS_VERSION, HINTS_VERSION);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }


    /**************************************************************************
     *
     * async task for file operations
     *
     *************************************************************************/
    @SuppressLint("StaticFieldLeak")
    private class FileAccessTask extends AsyncTaskExecutor<Integer /* do */, Integer /* pre */, Integer /* post */>
    {
        int actionCode = actionNone;

        @Override
        protected void onPreExecute()
        {
            Log.d(LOG_TAG, "onPreExecute()");
        }


        /**************************************************************************
         *
         * async: create backup files from music files
         *
         * return number of successfully saved files
         *
         *************************************************************************/
        private int backupAllFiles()
        {
            int nSucceeded = 0;
            for (int i = 0; i < mAudioFileInfoList.size(); i++)
            {
                AudioFileInfo theInfo = mAudioFileInfoList.get(i);
                if ((theInfo.mPath != null) && (theInfo.isWriteCompatible()))
                {
                    int ret = theInfo.doBackup(getContentResolver());
                    if (ret == 0)
                    {
                        // success
                        nSucceeded++;
                    }
                    else
                    {
                        Log.d(LOG_TAG, "backupAllFiles() : failure");
                    }
                }
            }

            return nSucceeded;
        }


        /**************************************************************************
         *
         * async: write tags from music files
         *
         * Skip invalid entries and "dash" files where writing is not supported.
         * Trigger both Android media scanner and Classical Music Scanner
         *
         * return number of successfully written files
         *
         *************************************************************************/
        private int writeAllTags()
        {
            boolean bRemoveId3v1 = UserSettings.getBool(UserSettings.PREF_REMOVE_ID3V1_TAGS, false);
            int nSucceeded = 0;
            ArrayList<String> audioPathList = new ArrayList<>();    // list of paths to pass to Classic Music Scanner
            for (int i = 0; i < mAudioFileInfoList.size(); i++)
            {
                AudioFileInfo theInfo = mAudioFileInfoList.get(i);
                if ((theInfo.mPath != null) && (theInfo.isWriteCompatible()))
                {
                    int ret = theInfo.writeTags(mNewTags, bRemoveId3v1, mbDryRun);
                    if (ret == 0)
                    {
                        // success
                        audioPathList.add(theInfo.mPath);
                        nSucceeded++;
                    }
                    else
                    if (ret == 1)
                    {
                        Log.d(LOG_TAG, "writeAllTags() : write access error");
                    }
                }
            }
            triggerScannersAsync(audioPathList);
            return nSucceeded;
        }


        /**************************************************************************
         *
         * async: read tags from music files to
         *
         *  mAudioTagsCommon
         *  mAudioFileInfoList
         *  mAudioFileTagsList
         *
         * return the number of successfully read files, the
         * other ones are corrupt
         *
         *************************************************************************/
        private int readAllTags()
        {
            mAudioTagsCommon = null;
            int nSucceeded = 0;
            mNumOfWritableFiles = 0;
            mLastErrorFromTaggerLib = errCode.eOk;

            for (int i = 0; i < mFileList.size(); i++)
            {
                DirectoryTree.DirectoryEntry de = mFileList.get(i);
                AudioFileInfo audioInfo = new AudioFileInfo(de, getContentResolver());
                errCode err = audioInfo.open();
                if (err == errCode.eOk)
                {
                    AudioFileInfo.AudioTags audioTags = audioInfo.getTags();
                    assert(audioTags != null);
                    if (de.canWrite())
                    {
                        if (audioInfo.isWriteCompatible())
                        {
                            mNumOfWritableFiles++;
                        }
                        else
                        {
                            mNumOfIncompatibleFiles++;
                        }
                    }

                    nSucceeded++;
                    mAudioFileInfoList.add(audioInfo);
                    mAudioFileTagsList.add(audioTags);
                    if (mAudioTagsCommon == null)
                    {
                        // this is the first tag object, just remember it
                        mAudioTagsCommon = audioTags.copy();
                    }
                    else
                    {
                        // this is not the first tag object, compare each entry to stored one
                        for (int j = 0; j < AudioFileInfo.idNum; j++)
                        {
                            if (!Utility.strEq(audioTags.tags[j], mAudioTagsCommon.tags[j]))
                            {
                                mAudioTagsCommon.tags[j] = sVarious;
                            }
                        }
                    }
                }
                else
                {
                    mLastErrorFromTaggerLib = err;
                    mNumOfUnreadableFiles++;
                }
            }
            return nSucceeded;
        }

        @Override
        protected Integer doInBackground(Integer... params)
        {
            actionCode = params[0];
            Log.d(LOG_TAG, "doInBackground(" + actionCode + ")");
            if (actionCode == actionReadTags)
            {
                return readAllTags();
            }
            else
            if (actionCode == actionCreateBackupFiles)
            {
                return backupAllFiles();
            }
            else
            if (actionCode == actionWriteTags)
            {
                return writeAllTags();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result)
        {
            if (actionCode == actionReadTags)
            {
                TagsActivity.this.onAsyncReadFinished(result);
            }
            else
            if (actionCode == actionCreateBackupFiles)
            {
                TagsActivity.this.onAsyncBackupFinished(result);
            }
            else
            if (actionCode == actionWriteTags)
            {
                TagsActivity.this.onAsyncWriteFinished(result);
            }
        }
    }

}

