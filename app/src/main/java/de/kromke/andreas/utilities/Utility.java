/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.util.Log;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This and that
 */

public class Utility
{
    private static final String LOG_TAG = "CMT : UTI"; //for debugging purposes.

    // safe string compare
    public static boolean strEq(String a, String b)
    {
        if (a == null) return (b == null);
        //noinspection SimplifiableIfStatement
        if (b == null) return false;
        return a.equals(b);
    }

    // safe string compare
    static int strCompare(String a, String b)
    {
        if (a == null) return (b == null) ? 0 : -1;
        //noinspection SimplifiableIfStatement
        if (b == null) return 1;
        return a.compareTo(b);
    }

    static void closeStream(Closeable s)
    {
        try
        {
            s.close();
        }
        catch (Exception e)
        {
            Log.e(LOG_TAG, "I/O exception");
        }
    }

    static boolean copyFileFromTo(InputStream is, OutputStream os)
    {
        boolean result = true;
        try
        {
            byte[] buffer = new byte[4096];
            int length;
            while ((length = is.read(buffer)) > 0)
            {
                os.write(buffer, 0, length);
            }
        } catch (FileNotFoundException e)
        {
            Log.e(LOG_TAG, "file not found");
            result = false;
        } catch (IOException e)
        {
            Log.e(LOG_TAG, "I/O exception  = " + e.getMessage());
            result = false;
        } finally
        {
            if (is != null)
                closeStream(is);
            if (os != null)
                closeStream(os);
        }

        return result;
    }
}
