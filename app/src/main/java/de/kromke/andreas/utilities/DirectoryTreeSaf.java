/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import androidx.annotation.NonNull;
import androidx.documentfile.provider.DocumentFile;
import android.util.Log;
import android.webkit.MimeTypeMap;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static de.kromke.andreas.utilities.Utility.strCompare;

/**
 * helper class to handle directories
 * @noinspection CommentedOutCode, RedundantSuppression
 */

public class DirectoryTreeSaf implements DirectoryTree
{
    private static final String LOG_TAG = "CMT : DES"; //for debugging purposes.

    static public class DirectoryEntrySaf implements DirectoryEntry
    {
        DocumentFile f;
        Context context = null;     // needed if file was created from URI, otherwise not needed

        public DirectoryEntrySaf(DocumentFile f)
        {
            this.f = f;
        }

        public DirectoryEntrySaf(DocumentFile f, Context theContext)
        {
            this.f = f;
            getTree(theContext);   // replace single df with tree df, if possible
        }

        // directory Uri for internal memory is like:
        //      "content://com.android.externalstorage.documents/document/primary%3AMusic"
        // directory Uri for internal SD card is like:
        //      "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic"
        public DirectoryEntrySaf(String fileUriStr, String directoryUriStr, Context theContext)
        {
            context = theContext;

            //
            // first get a SingleDocumentFile, it cannot be renamed and has no parent
            //

            Uri fileUri = Uri.parse(fileUriStr);
            f = DocumentFile.fromSingleUri(context, fileUri);       // get a SingleDocumentFile
            if (f == null)
            {
                Log.w(LOG_TAG, "DirectoryEntrySaf() : DocumentFile.fromSingleUri() failed for uri \"" + fileUri + "\"");
            }

            //
            // If no directory was passed, we try the persistant permissions to find
            // a suitable parent and walk through the tree to finally get a
            // tree document file.
            //

            if (directoryUriStr == null)
            {
                //Log.w(LOG_TAG, "DirectoryEntrySaf() : no directoryUriStr provided");
                DocumentFile ft = SafUtilities.getTreeDocumentFile(f, theContext);
                if (ft != null)
                {
                    Log.d(LOG_TAG, "DirectoryEntrySaf() : successfully got TreeDocumentFile for \"" + fileUri + "\"");
                    f = ft;
                }
                else
                {
                    Log.w(LOG_TAG, "DirectoryEntrySaf() : could not get TreeDocumentFile for \"" + fileUri + "\"");
                }
            }

            //
            // with help of the directory get a TreeDocumentFile (has parent and is renameable)
            //

            if ((directoryUriStr != null) && (f != null))
            {
                String name = f.getName();
                if (name != null)
                {
                    DocumentFile d = SafUtilities.fromTreeUriString(directoryUriStr, context);
                    if (d != null)
                    {
                        DocumentFile temp = d.findFile(name);
                        if (temp != null)
                        {
                            Log.d(LOG_TAG, "DirectoryEntrySaf() : TreeDocumentFile created");
                            f = temp;
                        }
                        else
                        {
                            Log.e(LOG_TAG, "DirectoryEntrySaf() : findFile() failed for name \"" + name + "\" in uri \"" + d.getUri() + "\"");
                        }
                    }
                    else
                    {
                        Log.e(LOG_TAG, "DirectoryEntrySaf() : DocumentFile.fromTreeUri() failed for \"" + directoryUriStr + "\"");
                    }
                }
                else
                {
                    Log.e(LOG_TAG, "DirectoryEntrySaf() : getName() failed for \"" + f.getUri() + "\"");
                }
            }
        }

        private void getTree(Context theContext)
        {
            if (f.getParentFile() == null)
            {
                // seems to be a SingleDocumentFile, trying to get a TreeDocumentFile
                DocumentFile ft = SafUtilities.getTreeDocumentFile(f, theContext);
                if (ft != null)
                {
                    Log.d(LOG_TAG, "getTree() : successfully got TreeDocumentFile for \"" + f.getUri() + "\"");
                    f = ft;
                } else
                {
                    Log.w(LOG_TAG, "getTree() : could not get TreeDocumentFile for \"" + f.getUri() + "\"");
                }
            }
        }

        public int compareTo(DirectoryEntry o)
        {
            DirectoryEntrySaf to = (DirectoryEntrySaf) o;
            return strCompare(f.getName(), to.getName());
        }
        public boolean isDirectory()
        {
            return f.isDirectory();
        }

        public boolean exists() { return f.exists(); }

        public boolean delete() { return f.delete(); }

        public boolean canRead()
        {
            // Due to an Android or Android documentation bug the return code
            // of canRead() is always "false" in case it's not a file, but however
            // one get get an InputStream.
            return (!f.isFile()) || (f.canRead());
        }

        public boolean canWrite()
        {
            return f.canWrite();
        }

        public boolean hasParent()
        {
            return (f.getParentFile() != null);
        }
        public boolean copyTo(DirectoryEntry destination, ContentResolver theContentResolver)
        {
            InputStream is;
            try
            {
                is = theContentResolver.openInputStream(this.f.getUri());
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "source file not found");
                return false;
            }

            if (is == null)
            {
                return false;
            }

            OutputStream os;
            try
            {
                os = theContentResolver.openOutputStream(((((DirectoryEntrySaf) destination).f).getUri()));
            } catch (FileNotFoundException e)
            {
                Log.e(LOG_TAG, "destination file not found");
                Utility.closeStream(is);
                return false;
            }

            return Utility.copyFileFromTo(is, os);
        }

        public DirectoryEntry copyTo(String name, ContentResolver theContentResolver)
        {
            // get name extension
            String extension = name;
            int pos = name.lastIndexOf('.');
            if (pos >= 0)
            {
                extension = name.substring(pos + 1);
            }
            String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
            if (mime == null)
            {
                mime = "";
            }

            DocumentFile parentFile = f.getParentFile();
            if (parentFile != null)
            {
                DocumentFile fn = parentFile.createFile(mime, name);
                DirectoryEntrySaf dfn = new DirectoryEntrySaf(fn);
                if (copyTo(dfn, theContentResolver))
                {
                    return dfn;
                }
            }
            else
            {
                Log.e(LOG_TAG, "copyTo() : parent file is null");
            }

            return null;
        }

        public String getName()
        {
            String s = f.getName();

            if (s == null)
            {
                // It seems that getName() does some database query and will return null on missing permissions
                Uri uri = f.getUri();
                s = uri.getLastPathSegment();
                if (s != null)
                {
                    int index = s.lastIndexOf('/');
                    if (index > 0)
                    {
                        s = s.substring(index + 1);
                    }
                }
                else
                {
                    s = "LOL-ANDROID.wtf";      // "Downloads" SAF directory uses to contain garbage
                }
            }
            return s;
        }

        public String getPath()
        {
            return f.getUri().getPath();
        }

        public String getFilePath()
        {
            final String treePrimary = "/tree/primary/";
            final String pathPrimary = "/storage/emulated/0/";
            final String tree = "/tree/";
            final String storage = "/storage/";

            String path = f.getUri().getPath();
            Log.d(LOG_TAG, "getFilePath() : path is " + path);
            if (path != null)
            {
                path = getPseudoPath(path);
                Log.d(LOG_TAG, "getFilePath() : pseudo path is " + path);
                if (path.startsWith(treePrimary))
                {
                    path = pathPrimary + path.substring(treePrimary.length());
                }
                else
                if (path.startsWith(tree))
                {
                    path = storage + path.substring(tree.length());
                }
            }

            Log.d(LOG_TAG, "getFilePath() : resulting path is " + path);
            return path;
        }

        @NonNull
        public DirectoryEntry getParent()
        {
            return new DirectoryEntrySaf(f.getParentFile());
        }

        public Uri getUri()
        {
            return f.getUri();
        }

        public Object getFile()
        {
            return f;
        }

        @NonNull
        public String toString()
        {
            if (f == null)
            {
                Log.w(LOG_TAG, "toString() : DocumentFile is null");
                return "";
            }
            else
            {
                return f.getUri().toString();
            }
        }

        public opResult existsInSameDirectory(String name)
        {
            Log.d(LOG_TAG, "existsInSameDirectory(\"" + name + "\") : for DocumentFile " + f.getUri());
            DocumentFile parentFile = f.getParentFile();
            if (parentFile != null)
            {
                DocumentFile fn = parentFile.findFile(name);
                if (fn != null)
                {
                    return fn.exists() ? opResult.YES : opResult.NO;
                }
            }
            else
            {
                Log.e(LOG_TAG, "existsInSameDirectory() : no parent for DocumentFile " + f.getUri());
                return opResult.ERROR;
            }
            return opResult.NO;
        }

        // https://stackoverflow.com/questions/37168200/android-5-0-new-sd-card-access-api-documentfile-renameto-unsupportedoperation
        @SuppressLint("ObsoleteSdkInt")
        public boolean renameToEx(String name, boolean changeTo)
        {
            Log.d(LOG_TAG, "renameToEx(\"" + name + "\", " + changeTo + ") : for DocumentFile " + f.getUri());
            DocumentFile parent = f.getParentFile();
            if (parent != null)
            {
                try
                {
                    Log.d(LOG_TAG, "renameTo() : using the renameTo() method");
                    if (f.renameTo(name))
                    {
                        if (changeTo)
                        {
                            DocumentFile fn = parent.findFile(name);
                            if (fn != null)
                            {
                                Log.d(LOG_TAG, "file renamed and DocumentFile changed");
                                f = fn;
                                return true;
                            } else
                            {
                                Log.e(LOG_TAG, "SEVERE ERROR: file renamed, but renamed file not found");
                            }
                        }
                        else
                        {
                            Log.d(LOG_TAG, "file renamed but DocumentFile unchanged");
                            return true;
                        }
                    }
                } catch (Exception e)
                {
                    Log.e(LOG_TAG, "cannot rename file (" + e.getMessage() + ")");
                }
            }
            else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                Log.e(LOG_TAG, "renameToEx() : no parent for DocumentFile " + f.getUri());
                try
                {
                    Log.d(LOG_TAG, "renameTo() : using the DocumentsContract.renameDocument() method");
                    Uri uri = DocumentsContract.renameDocument(context.getContentResolver(), f.getUri(), name);
                    if (uri == null)
                    {
                        Log.e(LOG_TAG, "cannot rename file");
                        return false;
                    }
                    if (changeTo && (uri != f.getUri()))
                    {
                        DocumentFile fn = DocumentFile.fromSingleUri(context, uri);
                        if (fn != null)
                        {
                            Log.d(LOG_TAG, "file renamed and DocumentFile/URI changed");
                            f = fn;
                            return true;
                        } else
                        {
                            Log.e(LOG_TAG, "SEVERE ERROR: file renamed, but renamed file not found");
                        }
                    }
                    else
                    {
                        Log.d(LOG_TAG, "file renamed but DocumentFile unchanged");
                        return true;
                    }
                } catch (Exception e)
                {
                    Log.e(LOG_TAG, "cannot rename file (" + e.getMessage() + ")");
                }
            }
            return false;
        }


        public opResult isSeekable(ContentResolver contentResolver)
        {
            try(ParcelFileDescriptor pfd = contentResolver.openFileDescriptor(f.getUri(), "r"))
            {
                if (pfd == null)
                {
                    return opResult.ERROR;
                }
                FileDescriptor fd = pfd.getFileDescriptor();
                if (fd == null)
                {
                    return opResult.ERROR;
                }
                try(FileInputStream is = new FileInputStream(pfd.getFileDescriptor()))
                {
                    FileChannel ifc = is.getChannel();
                    long pos;
                    pos = ifc.position();
                    if (pos < 0)
                    {
                        return opResult.NO;
                    }
                    ifc.position(pos);
                    return opResult.YES;
                }
            } catch (FileNotFoundException ex)
            {
                Log.e(LOG_TAG, "isSeekable() : file-not-found exception with message = " + ex.getMessage());
                return opResult.ERROR;
            } catch (IOException ex)
            {
                Log.e(LOG_TAG, "isSeekable() : seek unsupported");
                return opResult.NO;
            } catch (SecurityException ex)
            {
                Log.e(LOG_TAG, "isSeekable() : permission denied");
                return opResult.ERROR;
            }
        }
    }


    public static String strWriteProtected = null;
    private DocumentFile mCurrentDir;       // current location
    private final DocumentFile mRootDir;

    // constructor, called after file selector, here the "root" URI is passed
    // Note that we will NOT get a TreeDocumentFile in case the uri does not point to the current "root".
    public DirectoryTreeSaf(Context context, Uri uri)
    {
        DocumentFile d = DocumentFile.fromTreeUri(context, uri);
        if ((d == null) || !d.canRead())
        {
            Log.e(LOG_TAG, "Uri path not readable: " + uri);
            d = DocumentFile.fromFile(new File("/"));
        }

        mCurrentDir = d;
        mRootDir = mCurrentDir;
    }

    // flexible constructor, called with an arbitrary URI string
    public DirectoryTreeSaf(Context context, String uriString)
    {
        mCurrentDir = SafUtilities.fromTreeUriString(uriString, context);
        mRootDir = mCurrentDir;
    }

    // special helper
    static public boolean isValid(Context context, Uri uri)
    {
        try
        {
            DocumentFile d = DocumentFile.fromTreeUri(context, uri);
            if ((d != null) && d.canRead())
            {
                return true;
            }
        }
        catch(Exception e)
        {
            Log.e(LOG_TAG, "Exception : " + e.getMessage());
        }
        Log.w(LOG_TAG, "Uri path not readable: " + uri);
        return false;
    }

    public boolean isValid()
    {
        return (mCurrentDir != null);
    }

    public DirectoryTree setCurrent(Context context, DirectoryEntry base)
    {
        mCurrentDir = ((DirectoryEntrySaf) base).f;
        return this;
    }

    public boolean isRoot()
    {
        //noinspection SimplifiableIfStatement
        if ((mRootDir == null) || (mCurrentDir == null))
        {
            return true;
        }
        return mRootDir.getUri().equals(mCurrentDir.getUri());
    }

    //get the current directory
    public DirectoryEntry getCurrent()
    {
        return new DirectoryEntrySaf(mCurrentDir);
    }

    //get the current path as Uri String
    public String getCurrentUriAsString()
    {
        if (mCurrentDir != null)
            return mCurrentDir.getUri().toString();
        else
            return null;
    }

    //set and get previous directory
    public DirectoryEntry gotoParent()
    {
        DocumentFile parent = mCurrentDir.getParentFile();
        if (parent != null)
        {
            mCurrentDir = parent;
        }
        return getCurrent();
    }

    //set the current directory
    public void setCurrent(Context context, String stringUri)
    {
        Uri uri = Uri.parse(stringUri);
        DocumentFile d = DocumentFile.fromTreeUri(context, uri);
        if ((d == null) || !d.canRead())
        {
            mCurrentDir = d;
        }
    }

    //Returns a sorted list of all dirs and files in a given directory.
    public List<DirectoryEntry> getChildren()
    {
        /* directories and files will be sorted separately, and later files will be placed after directories. */
        List<DirectoryEntry> dirs = new ArrayList<>();
        List<DirectoryEntry> files = new ArrayList<>();

        DocumentFile[] allFiles = mCurrentDir.listFiles();
        for (DocumentFile file : allFiles)
        {
            if (file.isDirectory())
            {
                dirs.add(new DirectoryEntrySaf(file));
            } else
            {
                files.add(new DirectoryEntrySaf(file));
            }
        }

        Collections.sort(dirs);
        Collections.sort(files);

        /* Both lists are sorted, so I can just add the files to the dirs list.
          This will give me a list of dirs on top and files on bottom. */
        dirs.addAll(files);

        return dirs;
    }

    @NonNull
    public String toString()
    {
        return mCurrentDir.getUri().toString();
    }

    @NonNull public String getInfoString()
    {
        String path = getPseudoPath(getCurrent().getPath());

        if (!mCurrentDir.canWrite())
        {
            path += " (" + strWriteProtected + ")";
        }

        return path;
    }

    // Heuristic method to derive kind of path from Uri
    // TODO: handle objects in "Download" folder, having paths like "/document/msf:4711"
    public static String getPseudoPath(String path)
    {
        // the regular expression shall match ":", but not "://", because this pattern is used by "smb://" or similar
        final String[] split = path.split(":(?!//)");
        if (split.length == 3)
        {
            path = split[0] + "/" + split[2];
        }
        else
        if (split.length == 2)
        {
            path = split[0];
        }

        return path;
    }


    /*
    static private DocumentFile getTreeDocumentFile(Context context, final String directoryUriStr)
    {
        Uri directoryUri = Uri.parse(directoryUriStr);
        //TODO: this only works if the system file selector selected this directory, it always goes back to that base directory
        // e.g. TreeDocumentFile from
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3A/document/42B5-1A05%3AMusic%2FKlassik%2FBeethoven%20-%20Complete%20Masterpieces%20(tagged)%2FCD02%20-%20Beethoven%20-%20Complete%20Masterpieces"
        // is
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3A/document/42B5-1A05%3A"
        // or from
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic/document/42B5-1A05%3AMusic%2FKlassik%2FBeethoven%20-%20Complete%20Masterpieces%20(tagged)%2FCD01%20-%20Beethoven%20-%20Complete%20Masterpieces"
        //     as path: "/tree/42B5-1A05:Music/document/42B5-1A05:Music/Klassik/Beethoven - Complete Masterpieces (tagged)/CD01 - Beethoven - Complete Masterpieces"
        // is
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic/document/42B5-1A05%3AMusic"
        //     as path: "/tree/42B5-1A05:Music/document/42B5-1A05:Music"
        // unfortunately.
        DocumentFile d;
        try
        {
            d = DocumentFile.fromTreeUri(context, directoryUri);      // get a tree document file
        } catch (IllegalArgumentException e)
        {
            Log.e(LOG_TAG, "Error on DocumentFile.fromTreeUri()", e);
            return null;
        }

        if (d != null)
        {
            Log.d(LOG_TAG, "getTreeDocumentFile() : TreeDocumentFile from \"" + directoryUri +"\" is \"" + d.getUri() + "\"");
            Log.d(LOG_TAG, "getTreeDocumentFile() : TreeDocumentFile from \"" + directoryUri.getPath() +"\" is \"" + d.getUri().getPath() + "\"");
            String rpath = d.getUri().getPath();        // this is what we have
            String dpath = directoryUri.getPath();      // this is what we want
            if ((rpath != null) && (dpath != null))
            {
                // note that split without parameter -1 will discard empty strings, e.g. when path ends with delimiter
                final String[] rsplit = rpath.split(":", -1);
                final String[] dsplit = dpath.split(":", -1);
                if ((rsplit.length == 3) && (dsplit.length == 3))
                {
                    Log.d(LOG_TAG, "getTreeDocumentFile() : have: \"" + rsplit[2] + "\"");
                    Log.d(LOG_TAG, "getTreeDocumentFile() : need: \"" + dsplit[2] + "\"");
                    final String[] rdsplit = rsplit[2].split("/");
                    final String[] ddsplit = dsplit[2].split("/");
                    Log.d(LOG_TAG, "getTreeDocumentFile() : have " + rdsplit.length + " path elements");
                    Log.d(LOG_TAG, "getTreeDocumentFile() : need " + ddsplit.length + " path elements");

                    // special handling for empty strings
                    int rdsplit_len = rdsplit.length;
                    if (rdsplit_len == 1 && rdsplit[0].isEmpty())
                    {
                        rdsplit_len = 0;
                    }
                    int ddsplit_len = ddsplit.length;
                    if (ddsplit_len == 1 && ddsplit[0].isEmpty())
                    {
                        ddsplit_len = 0;
                    }

                    int i = 0;
                    while ((i < rdsplit_len) && (i < ddsplit_len) && (rdsplit[i].equals(ddsplit[i])))
                    {
                        i++;
                    }
                    Log.d(LOG_TAG, "getTreeDocumentFile() : found " + i + " common prefix directories.");
                    if (i < rdsplit_len)
                    {
                        Log.e(LOG_TAG, "getTreeDocumentFile() : base directory mismatch: \"" + rsplit[2] + "\"");
                    }
                    else
                    {
                        while (i < ddsplit_len)
                        {
                            Log.d(LOG_TAG, "getTreeDocumentFile() : enter directory \"" + ddsplit[i] + "\"");
                            d = d.findFile(ddsplit[i]);
                            if (d == null)
                            {
                                Log.d(LOG_TAG, "getTreeDocumentFile() : could not enter directory \"" + ddsplit[i] + "\"");
                                break;
                            }
                            i++;
                        }
                    }
                }
                else
                {
                    Log.e(LOG_TAG, "getTreeDocumentFile() : unexpected path format, need two ':'");
                }
            }
        }
        else
        {
            Log.e(LOG_TAG, "getTreeDocumentFile() : DocumentFile.fromTreeUri() failed for \"" + directoryUri + "\"");
        }

        return d;
    }
     */
}
