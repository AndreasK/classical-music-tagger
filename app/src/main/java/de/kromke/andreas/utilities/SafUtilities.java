/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.Context;
import android.content.UriPermission;
import android.net.Uri;
import android.util.Log;

import java.util.List;
import androidx.documentfile.provider.DocumentFile;
import de.kromke.andreas.musictagger.BuildConfig;

/**
 * Useful functions regarding Storage Access Framework
 * @noinspection JavadocBlankLines, RedundantSuppression, CommentedOutCode, SpellCheckingInspection
 */

public class SafUtilities
{
    private static final String LOG_TAG = "CMT : SafUtilities"; //for debugging purposes.


    /* ***********************************************************************************
     *
     * debug helper
     *
     ***********************************************************************************/
    private static void printPermissions(List<UriPermission> thePermissionList)
    {
        for (UriPermission perm : thePermissionList)
        {
            Uri uri = perm.getUri();
            Log.d(LOG_TAG, "printPermissions(): uri = " + uri);
            String auth = uri.getAuthority();
            Log.d(LOG_TAG, "printPermissions(): auth = " + auth);
            List<String> segments = uri.getPathSegments();
            Log.d(LOG_TAG, "printPermissions(): segments = " + segments);
        }
    }


    /************************************************************************************
     *
     * helper to derive the complete path, including the file itself, from an URI
     * segment list
     *
     ***********************************************************************************/
    private static String getDocumentPathFromSegments(final List<String> segments)
    {
        //
        // loop over key/value pairs, key is either "tree" or "document"
        //

        for (int i = 1; i < segments.size(); i += 2)
        {
            final String key = segments.get(i - 1);
            if (key.equals("document"))
            {
                return segments.get(i);
            }
        }

        Log.e(LOG_TAG, "getDirectoryPathFromSegments(): unexpected segment list");
        return null;
    }


    /************************************************************************************
     *
     * helper to derive the path, without the file itself, from an URI
     * segment list
     *
     * Example (Android 11):
     *
     *  "tree"
     *  "9C33-6BBD:Music/Klassik"
     *  "document"
     *  "9C33-6BBD:Music/Klassik/CDs/album/bla.m4a"
     *
     * must lead to
     *
     *  9C33-6BBD:Music/Klassik/CDs/album
     *
     ***********************************************************************************/
    private static String getDirectoryPathFromSegments(final List<String> segments)
    {
        //
        // loop over key/value pairs, key is either "tree" or "document"
        //

        String df = null;     // nothing found, yet
        String tf = null;

        for (int i = 1; i < segments.size(); i += 2)
        {
            final String key = segments.get(i - 1);
            if (key.equals("tree"))
            {
                tf = segments.get(i);     // easy solution: explicit tree
            }
            else
            if (key.equals("document"))
            {
                df = segments.get(i);       // complete path, including the file itself
            }
        }

        if (df == null)
        {
            return tf;      // no complete path found, return tree or null
        }

        //
        // derive path from "document"
        //

        int pos = df.lastIndexOf('/');
        if (pos >= 0)
        {
            return df.substring(0, pos);
        }
        else
        {
            return "";
        }
    }


    /************************************************************************************
     *
     * check if the file is inside the given directory
     *
     * URIs derived from getPersistedUriPermissions() seem to have always two segments,
     * either
     *  ["document", "blabla-file"]
     * or
     *  ["tree", "blabla-directory"]
     *
     * URIs derived from DocumentFile.fromSingleUri() seem to have two or four segments,
     * either
     *  ["tree", "blabla-directory", "document", "blabla-file"]
     *  where the tree path (index 1) is a prefix of the document path (index 3)
     * or
     *  ["document", "blabla-file"]
     *
     * For auth = "com.android.externalstorage.documents" a directory looks like
     * "5213-1B10:" (root) or "5213-1B10:Music" (subdirectory of root) or
     * "5213-1B10:Testmusic/Lalo" (deeper subdirectory).
     *
     * return -1 or length of matching path prefix
     *
     ***********************************************************************************/
    private static int isInPath(final String ddoc, String dperm)
    {
        if (!dperm.endsWith(":") && !dperm.endsWith("/"))
        {
            Log.d(LOG_TAG, "isInPath(): add '/' to directory path to avoid wrong match.");
            dperm = dperm + "/";
        }

        if (ddoc.startsWith(dperm))
        {
            return dperm.length();
        }
        else
        {
            return -1;
        }
    }


    /* ***********************************************************************************
     *
     * get filename from Uri segments of a document file
     *
     * Before calling this function make sure there are four segments
     *
     ***********************************************************************************/
    /*
    private static String getFilename(final List<String> segments)
    {
        String doc_filename = segments.get(3);
        int index = doc_filename.lastIndexOf("/");
        if (index < 0)
        {
            index = doc_filename.lastIndexOf(":");
        }
        if (index < 0)
        {
            Log.e(LOG_TAG, "getFilename(): cannot get filename for \"" + doc_filename + "\"");
            return null;
        }
        return doc_filename.substring(index + 1);
    }
    */


    /************************************************************************************
     *
     * try to get a TreeDocumentFile from a SingleDocumentFile,
     * using the persisted Uri permissions
     *
     ***********************************************************************************/
    public static DocumentFile getTreeDocumentFile
    (
        DocumentFile df,
        Context context
    )
    {
        Uri uri = df.getUri();
        final String doc_auth = uri.getAuthority();
        final List<String> doc_segments = uri.getPathSegments();
        String doc_filename = df.getName();
        final String doc_directory;
        if (doc_filename != null)
        {
            doc_directory = getDirectoryPathFromSegments(doc_segments);
            if (doc_directory == null)
            {
                return null;        // must give up here
            }
        }
        else
        {
            // try alternative method to get directory and file name

            Log.w(LOG_TAG, "getTreeDocumentFile(): cannot get filename for \"" + uri + "\"");
            String fullDocumentPath = getDocumentPathFromSegments(doc_segments);
            if (fullDocumentPath == null)
            {
                return null;        // must give up here
            }
            int pos = fullDocumentPath.lastIndexOf('/');
            if (pos >= 0)
            {
                doc_directory = fullDocumentPath.substring(0, pos);
                doc_filename = fullDocumentPath.substring(pos + 1);
            }
            else
            {
                doc_directory = "";
                doc_filename = fullDocumentPath;
            }
        }


        /* experimental, does not work
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            final String docId = DocumentsContract.getTreeDocumentId(uri);
            Log.d(LOG_TAG, "getTreeDocumentFile(): tree document id = " + docId);
            Uri tree_uri = DocumentsContract.buildTreeDocumentUri(auth, docId);
            Log.d(LOG_TAG, "getTreeDocumentFile(): tree uri = " + tree_uri);
            DocumentFile dd = DocumentFile.fromTreeUri(context, tree_uri);
            if (dd != null)
            {
                return dd;
            }
            else
            {
                Log.e(LOG_TAG, "getTreeDocumentFile(): fromTreeUri() failed for " + tree_uri);
            }
        }
        */

        Log.d(LOG_TAG, "getTreeDocumentFile(): uri = " + uri);
        Log.d(LOG_TAG, "getTreeDocumentFile(): segments = " + doc_segments);
        Log.d(LOG_TAG, "getTreeDocumentFile(): auth = " + uri.getAuthority());

        List<UriPermission> thePermissionList = context.getContentResolver().getPersistedUriPermissions();
        //noinspection ConstantConditions
        if (BuildConfig.BUILD_TYPE.startsWith("debug"))
        {
            printPermissions(thePermissionList);
        }

        for (UriPermission perm : thePermissionList)
        {
            Uri perm_uri = perm.getUri();
            String perm_auth = perm_uri.getAuthority();
            if ((doc_auth == null) || !doc_auth.equals(perm_auth))
            {
                // authority mismatch
                continue;
            }

            //Log.d(LOG_TAG, "getTreeDocumentFile(): perm_uri = " + perm_uri);
            List<String> perm_segments = perm_uri.getPathSegments();
            //Log.d(LOG_TAG, "getTreeDocumentFile(): perm_segments = " + perm_segments);

            String dperm = getDirectoryPathFromSegments(perm_segments);
            if (dperm == null)
            {
                // malformed URI
                continue;
            }

            int prefix_len = isInPath(doc_directory, dperm);
            if (prefix_len < 0)
            {
                // path prefix mismatch
                //Log.d(LOG_TAG, "getTreeDocumentFile(): mismatch, skip");
                continue;
            }

            // directory matches, get tree

            //Log.d(LOG_TAG, "getTreeDocumentFile(): matching directory: \"" + perm_uri + "\"");

            DocumentFile d = DocumentFile.fromTreeUri(context, perm_uri);
            if (d == null)
            {
                Log.e(LOG_TAG, "printPermissions(): cannot get tree for \"" + perm_uri + "\"");
                continue;
            }

            // get remaining path and filename from document uri

            final String doc_remaining_path = doc_directory.substring(prefix_len);
            //Log.d(LOG_TAG, "getTreeDocumentFile(): remaining path: \"" + doc_path + "\"");
            //Log.d(LOG_TAG, "getTreeDocumentFile(): remaining filename: \"" + doc_filename + "\"");

            // directory climb loop

            String[] path_segments = doc_remaining_path.split("/");
            DocumentFile cf = null;

            for (String fe: path_segments)
            {
                cf = d.findFile(fe);
                if (cf == null)
                {
                    Log.e(LOG_TAG, "printPermissions(): cannot get child \"" + fe + "\"");
                    break;
                }

                // this is not the final file, so must be a directory
                if (!cf.isDirectory())
                {
                    Log.e(LOG_TAG, "printPermissions(): is no directory \"" + fe + "\"");
                    break;
                }

                d = cf;
            }

            if (cf != null)
            {
                return cf.findFile(doc_filename);
            }
        }

        return null;
    }


    /************************************************************************************
     *
     * workaround for the stupid DocumentFile.fromTreeUri() function that returns
     * not the directory itself, but some of its ancestors
     *
     ***********************************************************************************/
    public static DocumentFile fromTreeUriString(final String directoryUriStr, Context context)
    {
        Uri directoryUri = Uri.parse(directoryUriStr);
        //TODO: this only works if the system file selector selected this directory, it always goes back to that base directory
        // e.g. TreeDocumentFile from
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3A/document/42B5-1A05%3AMusic%2FKlassik%2FBeethoven%20-%20Complete%20Masterpieces%20(tagged)%2FCD02%20-%20Beethoven%20-%20Complete%20Masterpieces"
        // is
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3A/document/42B5-1A05%3A"
        // or from
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic/document/42B5-1A05%3AMusic%2FKlassik%2FBeethoven%20-%20Complete%20Masterpieces%20(tagged)%2FCD01%20-%20Beethoven%20-%20Complete%20Masterpieces"
        //     as path: "/tree/42B5-1A05:Music/document/42B5-1A05:Music/Klassik/Beethoven - Complete Masterpieces (tagged)/CD01 - Beethoven - Complete Masterpieces"
        // is
        //   "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic/document/42B5-1A05%3AMusic"
        //     as path: "/tree/42B5-1A05:Music/document/42B5-1A05:Music"
        // unfortunately.
        DocumentFile d;
        try
        {
            d = DocumentFile.fromTreeUri(context, directoryUri);      // get a tree document file
        } catch (IllegalArgumentException e)
        {
            Log.e(LOG_TAG, "Error on DocumentFile.fromTreeUri()", e);
            return null;
        }
        if (d == null)
        {
            Log.e(LOG_TAG, "getTreeDocumentFile() : DocumentFile.fromTreeUri() failed for \"" + directoryUri + "\"");
            return null;
        }

        Log.d(LOG_TAG, "getTreeDocumentFile() : TreeDocumentFile from \"" + directoryUri +"\" is \"" + d.getUri() + "\"");
        Log.d(LOG_TAG, "getTreeDocumentFile() : TreeDocumentFile from \"" + directoryUri.getPath() +"\" is \"" + d.getUri().getPath() + "\"");

        String rpath = d.getUri().getPath();        // this is what we have
        String dpath = directoryUri.getPath();      // this is what we want

        if ((rpath == null) || (dpath == null))
        {
            Log.e(LOG_TAG, "getTreeDocumentFile() : getPath() failed");
            return null;
        }

        // if dpath is a descendant of rpath, the latter must be the string prefix
        if (!dpath.startsWith(rpath))
        {
            Log.e(LOG_TAG, "getTreeDocumentFile() : \"" + dpath + "\" is no descendant of \"" + rpath + "\"");
            return null;
        }

        // get the rest of the path we still have to go
        String drpath = dpath.substring(rpath.length());
        Log.d(LOG_TAG, "getTreeDocumentFile() : path to go: \"" + drpath + "\"");

        final String[] drsplit = drpath.split("/");

        for (String gotodir : drsplit)
        {
            if (!gotodir.isEmpty() && !gotodir.equals("/"))
            {
                d = d.findFile(gotodir);
                if (d == null)
                {
                    Log.e(LOG_TAG, "getTreeDocumentFile() : could not enter directory \"" + gotodir + "\"");
                    break;
                }
            }
        }

        return d;
    }


    /************************************************************************************
     *
     * get Uri from path
     *
     * The permission list contains Uris like:
     *
     *  "content://com.android.providers.downloads.documents/tree/downloads"
     *  "content://com.android.externalstorage.documents/tree/42B5-1A05%3AMusic"
     *  "content://com.android.externalstorage.documents/tree/primary%3AClassicalMusicDb"
     *  "content://com.android.externalstorage.documents/tree/42B5-1A05%3A"
     *
     ***********************************************************************************/
    public static DocumentFile getUriFromPath(final String path, Context context)
    {
        final String storage = "/storage/";
        final String tree = "/tree/";

        //
        // remove "/storage/" prefix
        //

        if (!path.startsWith(storage))
        {
            return null;
        }
        String wpath = path.substring(storage.length());

        //
        // get volume name
        //

        int index = wpath.indexOf('/');
        if (index < 0)
        {
            return null;
        }
        String volumename = wpath.substring(0, index);
        wpath = wpath.substring(index + 1);
        Log.d(LOG_TAG, "getUriFromPath(): vol = " + volumename);
        Log.d(LOG_TAG, "getUriFromPath(): pth = " + wpath);

        List<UriPermission> thePermissionList = context.getContentResolver().getPersistedUriPermissions();

        for (UriPermission perm : thePermissionList)
        {
            Uri uri = perm.getUri();
            String auth = uri.getAuthority();
            Log.d(LOG_TAG, "getUriFromPath(): auth = " + auth);
            if ((auth != null) && auth.equals("com.android.externalstorage.documents"))
            {
                String upath = uri.getPath();
                Log.d(LOG_TAG, "getUriFromPath(): upath = " + upath);
                if ((upath != null) && upath.startsWith(tree))
                {
                    upath = upath.substring(tree.length());
                    Log.d(LOG_TAG, "getUriFromPath(): upath = " + upath);
                    index = upath.indexOf(':');
                    if (index >= 0)
                    {
                        String uvolume = upath.substring(0, index);
                        Log.d(LOG_TAG, "getUriFromPath(): uvolume = " + uvolume);
                        upath = upath.substring(index + 1);
                        Log.d(LOG_TAG, "getUriFromPath(): upath = " + upath);
                        if (uvolume.equals(volumename) && wpath.startsWith(upath))
                        {
                            String rpath = wpath.substring(upath.length());
                            Log.d(LOG_TAG, "getUriFromPath(): MATCH with remaining path " + rpath);

                            DocumentFile d = DocumentFile.fromTreeUri(context, uri);      // get a tree document file
                            if (d != null)
                            {
                                while (!rpath.isEmpty())
                                {
                                    if (rpath.startsWith("/"))
                                    {
                                        rpath = rpath.substring(1);
                                    }

                                    index = rpath.indexOf('/');
                                    if (index < 0)
                                    {
                                        index = rpath.length();
                                    }

                                    String name = rpath.substring(0, index);
                                    DocumentFile ndir = d.findFile(name);
                                    if (ndir == null)
                                    {
                                        Log.w(LOG_TAG, "getUriFromPath(): cannot find element \"" + name + "\" in Uri " + d.getUri());
                                        return null;
                                    }
                                    d = ndir;
                                    rpath = rpath.substring(name.length());
                                }

                                return d;
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
}
