/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;


// https://stackoverflow.com/questions/28897329/documentfile-randomaccessfile

/** @noinspection CommentedOutCode*/
public class MyRandomAccessFileUsingFile implements MyRandomAccessFile
{
    private RandomAccessFile raf;
    //private boolean bRead;
    //private boolean bWrite;

    //
    // constructors
    //

    MyRandomAccessFileUsingFile(File file, final String mode)
    {
        /*
        switch (mode)
        {
            case "r":
                bRead = true;
                bWrite = false;
                break;
            case "w":
                bRead = false;
                bWrite = true;
                break;
            case "rw":
                bRead = true;
                bWrite = true;
                break;
        }
         */

        try
        {
            raf = new RandomAccessFile(file, mode);
        } catch (FileNotFoundException e)
        {
            raf = null;
        }
    }

    public int read() throws IOException
    {
        return raf.read();
        /*
        try
        {
            return raf.read();
        } catch (IOException e)
        {
            // ignore
            return -1;
        }
         */
    }

    public int read(byte[] b)
    {
        try
        {
            return raf.read(b);
        } catch (IOException e)
        {
            // ignore
            return -1;
        }
    }

    public final byte readByte()
    {
        try
        {
            return raf.readByte();
        } catch (IOException e)
        {
            // ignore
            return -1;
        }
    }

    public final void readFully(byte[] b) throws IOException
    {
        raf.readFully(b);
        /*
        try
        {
            raf.readFully(b);
        } catch (IOException e)
        {
            // ignore
        }
         */
    }

    public final void readFully(byte[] b, int off, int len) throws IOException
    {
        raf.read(b, off, len);
        /*
        try
        {
            raf.read(b, off, len);
        } catch (IOException e)
        {
            // ignore
        }
         */
    }

    public final int read(byte[] b, int off, int len)
    {
        try
        {
            return raf.read(b, off, len);
        } catch (IOException e)
        {
            // ignore
            return -1;
        }
    }

    public void write(byte[] b, int off, int len)
    {
        try
        {
            raf.write(b, off, len);
        } catch (IOException e)
        {
            // ignore
        }
    }

    public void write(byte[] b)
    {
        try
        {
            raf.write(b);
        } catch (IOException e)
        {
            // ignore
        }
    }

    public void write(int b)
    {
        try
        {
            raf.write(b);
        } catch (IOException e)
        {
            // ignore
        }
    }

    public int skipBytes(int n)
    {
        try
        {
            return raf.skipBytes(n);
        } catch (IOException e)
        {
            // ignore
            return -1;
        }
    }

    public void seek(long pos) throws IOException
    {
        raf.seek(pos);
    }

    public void close()
    {
        try
        {
            raf.close();
        } catch (IOException e)
        {
            // ignore
        }
    }

    public final FileChannel getReadChannel()
    {
        return raf.getChannel();
    }

    public final FileChannel getWriteChannel()
    {
        return raf.getChannel();
    }

    public final FileChannel getChannel()
    {
        return raf.getChannel();
    }

    public long length()
    {
        try
        {
            return raf.length();
        } catch (IOException e)
        {
            return -1;
        }
    }

    public long getFilePointer()
    {
        try
        {
            return raf.getFilePointer();
        } catch (IOException e)
        {
            return -1;
        }
    }

    public void setLength(long newLength)
    {
        try
        {
            raf.setLength(newLength);
        } catch (IOException e)
        {
            // ignore
        }
    }

    public boolean isSeekable()
    {
        long pos = getFilePointer();
        if (pos >= 0)
        {
            try
            {
                seek(pos);
                return true;
            } catch (IOException e)
            {
                // ignore
            }
        }
        return false;
    }

    public boolean isValid()
    {
        return raf != null;
    }

    public boolean isMemBased()
    {
        return false;
    }
}
