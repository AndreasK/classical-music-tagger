/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.net.Uri;

import androidx.documentfile.provider.DocumentFile;

import android.util.Log;

import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;


// https://stackoverflow.com/questions/28897329/documentfile-randomaccessfile

/** @noinspection CommentedOutCode*/
public class MyRandomAccessFileUsingMemory implements MyRandomAccessFile
{
    private static final String LOG_TAG = "CMT : RAM"; //for debugging purposes.
    private final DocumentFile f;
    private final ContentResolver contentResolver;
    private ByteBuffer mByteBuffer = null;
    private boolean bRead;
    private boolean bWrite;
    private int dirtyLow = Integer.MAX_VALUE;           // range of file that was written to
    private int dirtyHigh = 0;
    private int originalSize = -1;

    static public class MyFileChannel extends FileChannel
    {
        MyRandomAccessFileUsingMemory mRaf;
        MyFileChannel(MyRandomAccessFileUsingMemory raf)
        {
            mRaf = raf;
        }

        @Override
        public FileLock lock(long position, long size, boolean shared)
        {
            Log.e(LOG_TAG, "MyFileChannel.lock() : not yet implemented");
            return null;        // not implemented
        }

        @Override
        public FileLock tryLock(long position, long size, boolean shared)
        {
            Log.e(LOG_TAG, "MyFileChannel.tryLock() : not yet implemented");
            return null;        // not implemented
        }

        @Override
        public MappedByteBuffer map(MapMode mode, long position, long size)
        {
            Log.e(LOG_TAG, "MyFileChannel.map() : not yet implemented");
            return null;        // not implemented
        }

        // read buffer list subset from current file position and update it
        @Override
        public long read(ByteBuffer[] dsts, int offset, int length)
        {
            Log.e(LOG_TAG, "MyFileChannel.read() : not yet implemented");
            return -1;        // not implemented
        }

        // read buffer from current file position and update it
        @Override
        public int read(ByteBuffer dst)
        {
            int nBytes = Math.min(dst.remaining(), mRaf.mByteBuffer.remaining());
            Log.w(LOG_TAG, "MyFileChannel.read(ByteBuffer) : pos=" + dst.position() + " n=" + nBytes);
            dst.put(mRaf.mByteBuffer.array(), mRaf.mByteBuffer.position(), nBytes);
            mRaf.mByteBuffer.position(mRaf.mByteBuffer.position() + nBytes);
            return nBytes;
            /* WARNING: For whatever reason this works fine, but not when called inside transferFrom().
            int ret = mRaf.read(dst.array(), dst.position(), nBytes);
            Log.w(LOG_TAG, "          -> bytes read=" + ret);
            if (ret > 0)
            {
                dst.position(dst.position() + ret);
            }
            return ret;
            */
        }

        // read from given file position and do not change that position
        @Override
        public int read(ByteBuffer dst, long position)
        {
            Log.e(LOG_TAG, "MyFileChannel.read(ByteBuffer, long) : not yet implemented");
            return -1;        // not implemented
        }

        @Override
        public int write(ByteBuffer src, long position)
        {
            Log.e(LOG_TAG, "MyFileChannel.write(ByteBuffer, long) : not yet implemented");
            return -1;        // not implemented
        }

        @Override
        public long write(ByteBuffer[] srcs, int offset, int length)
        {
            Log.e(LOG_TAG, "MyFileChannel.write() : not yet implemented");
            return -1;        // not implemented
        }

        @Override
        public int write(ByteBuffer src)
        {
            Log.w(LOG_TAG, "MyFileChannel.write(ByteBuffer) : not yet implemented");
            return -1;
        }

        @Override
        public long transferFrom(ReadableByteChannel src, long position, long count)
        {
            Log.e(LOG_TAG, "MyFileChannel.transferFrom() : not yet implemented");
            return -1;        // not implemented
        }

        @Override
        public long transferTo(long position, long count, WritableByteChannel target)
        {
            Log.e(LOG_TAG, "MyFileChannel.transferTo() : not yet implemented");
            return -1;        // not implemented
        }

        @Override
        public void force(boolean metaData)
        {
            Log.e(LOG_TAG, "MyFileChannel.force() : not yet implemented");
        }

        @Override
        public FileChannel truncate(long size)
        {
            Log.e(LOG_TAG, "MyFileChannel.truncate() : not yet implemented");
            return this;
        }

        @Override
        public long size()
        {
            long len = mRaf.length();
            Log.w(LOG_TAG, "MyFileChannel.size() => " + len);
            return len;
        }

        @Override
        public FileChannel position(long newPosition)
        {
            Log.w(LOG_TAG, "MyFileChannel.position(" + newPosition + ")");
            mRaf.seek(newPosition);
            return this;
        }

        @Override
        public long position()
        {
            long pos = mRaf.getFilePointer();
            Log.w(LOG_TAG, "MyFileChannel.position() => " + pos);
            return pos;
        }

        @Override
        protected void implCloseChannel()
        {
            Log.e(LOG_TAG, "MyFileChannel.implCloseChannel() : not yet implemented");
        }
    }

    //
    // constructors
    //

    MyRandomAccessFileUsingMemory(DocumentFile df, final String mode, ContentResolver theContentResolver)
    {
        f = df;
        contentResolver = theContentResolver;
        switch (mode)
        {
            case "r":
                bRead = true;
                bWrite = false;
                break;
            case "w":
                bRead = false;
                bWrite = true;
                break;
            case "rw":
                bRead = true;
                bWrite = true;
                break;
        }

        Uri uri = df.getUri();
        InputStream is;
        try
        {
            is = theContentResolver.openInputStream(uri);
            if (is != null)
            {
                byte[] rawByteArray = readCompleteStream(is);
                is.close();
                mByteBuffer = ByteBuffer.wrap(rawByteArray);
                originalSize = mByteBuffer.limit();
            }
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "MyRandomAccessFileUsingMemory() : exception = " + e.getMessage());
        }
    }

    public int read()
    {
        if ((bRead) && (mByteBuffer != null))
        {
            return mByteBuffer.getInt();
        }
        else
        {
            return -1;
        }
    }

    public int read(byte[] b)
    {
        if ((bRead) && (mByteBuffer != null))
        {
            int len = b.length;
            if (len > maxLen())
            {
                len = maxLen();
            }
            if (len == 0)
            {
                return 0;
            }

            try
            {
                mByteBuffer.get(b, 0, len);
                return len;
            } catch (Exception e)
            {
                // this should not happen, because we checked before
                Log.e(LOG_TAG, "read(byte[]) : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public final byte readByte()
    {
        if ((bRead) && (mByteBuffer != null))
        {
            try
            {
                byte[] b = new byte[1];
                mByteBuffer.get(b);
                return b[0];
            } catch (Exception e)
            {
                // ignore
                Log.e(LOG_TAG, "readByte() : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public final void readFully(byte[] b)
    {
        if ((bRead) && (mByteBuffer != null))
        {
            mByteBuffer.get(b);
        }
    }

    public final void readFully(byte[] b, int off, int len)
    {
        if ((bRead) && (mByteBuffer != null))
        {
            mByteBuffer.get(b, off, len);
        }
    }

    // read up to <len> bytes into <b> at offset <off>
    public final int read(byte[] b, int off, int len)
    {
        if ((bRead) && (mByteBuffer != null))
        {
            if (len > maxLen())
            {
                len = maxLen();
            }
            if (len == 0)
            {
                return 0;
            }

            try
            {
                mByteBuffer.get(b, off, len);
                return len;
            } catch (Exception e)
            {
                // this should not happen, because we checked before
                Log.e(LOG_TAG, "read(byte[], int, int) : exception : " + e);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }

    public void write(byte[] b, int off, int len)
    {
        if ((bWrite) && (mByteBuffer != null))
        {
            // extend?
            if (len > maxLen())
            {
                if (!growFile(len - maxLen()))
                {
                    Log.e(LOG_TAG, "write(byte[] b, int off, int len) : expand failed. ignore.");
                    len = maxLen();
                }
            }

            try
            {
                if (mByteBuffer.position() < dirtyLow)
                {
                    dirtyLow = mByteBuffer.position();
                }
                mByteBuffer.put(b, off, len);
                if (dirtyHigh < mByteBuffer.position())
                {
                    dirtyHigh = mByteBuffer.position();
                }
            } catch (Exception e)
            {
                // ignore
                Log.e(LOG_TAG, "write(byte[], int, int) : exception : " + e);
            }
        }
    }

    public void write(byte[] b)
    {
        if (mByteBuffer.position() < dirtyLow)
        {
            dirtyLow = mByteBuffer.position();
        }
        write(b, 0, b.length);
        if (dirtyHigh < mByteBuffer.position())
        {
            dirtyHigh = mByteBuffer.position();
        }
    }

    public void write(int b)
    {
        if ((bWrite) && (mByteBuffer != null))
        {
            try
            {
                if (mByteBuffer.position() < dirtyLow)
                {
                    dirtyLow = mByteBuffer.position();
                }
                mByteBuffer.putInt(b);
                if (dirtyHigh < mByteBuffer.position())
                {
                    dirtyHigh = mByteBuffer.position();
                }
            } catch (Exception e)
            {
                // ignore
                Log.e(LOG_TAG, "write(int) : exception : " + e);
            }
        }
    }

    public int skipBytes(int n)
    {
        if (mByteBuffer != null)
        {
            if (n > maxLen())
            {
                n = maxLen();
            }
            int newPosition = mByteBuffer.position() + n;
            mByteBuffer.position(newPosition);
            return n;
        }
        else
        {
            return -1;
        }
    }

    public void seek(long pos)
    {
        if (mByteBuffer != null)
        {
            mByteBuffer.position((int) pos);
        }
    }

    public void close()
    {
        if (dirtyHigh <= dirtyLow)
        {
            Log.d(LOG_TAG, "close() : file unchanged");
        }
        else
        {
            Log.d(LOG_TAG, "close() : file dirty from " + dirtyLow + " to " + dirtyHigh);
            if (originalSize != mByteBuffer.limit())
            {
                Log.d(LOG_TAG, "          file size changed from " + originalSize + " to " + mByteBuffer.limit());
            }
        }

        //
        // dumb, primitive, brute-force code, but works
        //

        if (mByteBuffer != null && (dirtyHigh > dirtyLow))
        {
            try
            {
                Uri uri = f.getUri();
                // mode "wt" is unsupported, but "w" behaves like "wt"
                OutputStream os = contentResolver.openOutputStream(uri, "w");
                if (os != null)
                {
                    // write the whole buffer, not only the changed part (brute-force)
                    os.write(mByteBuffer.array());
                    os.close();
                }
            } catch (Exception e)
            {
                Log.e(LOG_TAG, "close() : exception = " + e.getMessage());
            }
        }

        //
        // test code
        //

        /*
        try
        {
            Uri uri = f.getUri();
            // mode "wt" is unsupported
            OutputStream os = contentResolver.openOutputStream(uri, "w");
            if (os != null)
            {
                String testText = "Dies ist ein Test";
                Log.d(LOG_TAG, "close() : write test string to file");
                os.write(testText.getBytes());
                os.close();
            }
        } catch (Exception e)
        {
            Log.e(LOG_TAG, "close() : exception = " + e.getMessage());
        }
        */

        //
        // This code reveals some Android bugs. The write operation to file offset n will leave
        // all bytes below that offset to zero, destroying the file.
        //

        /*
        if (dirtyHigh > dirtyLow)
        {
            //
            // trying efficient method, only writing the changed block
            //

            if (dirtyLow > 0)
            {
                Uri uri = f.getUri();
                try
                {
                    // mode "write"
                    ParcelFileDescriptor pfd = contentResolver.openFileDescriptor(uri, "w");
                    if (pfd != null)
                    {
                        FileDescriptor fd = pfd.getFileDescriptor();
                        if (fd != null)
                        {
                            FileOutputStream os = new FileOutputStream(pfd.getFileDescriptor());
                            FileChannel ofc = os.getChannel();
                            // FileChannel can only write complete ByteBuffer, thus we create a segment of ours
                            ByteBuffer dirtyByteBuffer = ByteBuffer.wrap(mByteBuffer.array(), dirtyLow, dirtyHigh - dirtyLow);
                            // write changed bytes to the given file offset position
                            ofc.write(dirtyByteBuffer, dirtyLow);
                            if (mByteBuffer.limit() < originalSize)
                            {
                                ofc.truncate(mByteBuffer.limit());
                            }
                            ofc.close();
                            os.close();
                            pfd.close();
                            return;
                        }
                    }
                } catch (Exception e)
                {
                    Log.e(LOG_TAG, "close() : exception = " + e.getMessage());
                }
            }

            //
            // trying dumb method without seek, writing the whole file
            //

            Uri uri = f.getUri();
            try
            {
                String mode;
                if (mByteBuffer.limit() < originalSize)
                {
                    mode = "wt";
                }
                else
                {
                    mode = "w";
                }

                // mode "write" or "write and truncate"
                OutputStream os = contentResolver.openOutputStream(uri, mode);
                if (os != null)
                {
                    os.write(mByteBuffer.array());
                    os.close();
                }
            } catch (Exception e)
            {
                Log.e(LOG_TAG, "close() : exception = " + e.getMessage());
            }
        }
        */
    }

    public final FileChannel getReadChannel()
    {
        /*
        Log.e(LOG_TAG, "getReadChannel() : not yet implemented");
        return null;
        */
        return new MyFileChannel(this);
    }

    public final FileChannel getWriteChannel()
    {
        Log.e(LOG_TAG, "getWriteChannel() : not yet implemented");
        return null;
    }

    public final FileChannel getChannel()
    {
        Log.e(LOG_TAG, "getChannel() : not yet implemented");
        return null;
    }

    public long length()
    {
        if (mByteBuffer != null)
        {
            return mByteBuffer.limit();
        }
        return -1;
    }

    public long getFilePointer()
    {
        if (mByteBuffer != null)
        {
            return mByteBuffer.position();
        }
        return -1;
    }

    public void setLength(long newLength)
    {
        if (mByteBuffer != null)
        {
            int currLength = mByteBuffer.limit();
            if (newLength < currLength)
            {
                mByteBuffer.limit((int) newLength);
                Log.d(LOG_TAG, "setLength(" + newLength + ") : file shortened from " + currLength + " to " + newLength);
            }
            else
            if (newLength > currLength)
            {
                // allocate new buffer with higher capacity
                ByteBuffer newByteBuffer = ByteBuffer.allocate((int) newLength);
                // copy data
                byte[] oldArray = mByteBuffer.array();
                byte[] newArray = newByteBuffer.array();
                System.arraycopy(oldArray,
                        0,
                        newArray,
                        0,
                        currLength);
                // copy position, but not limit
                newByteBuffer.position(mByteBuffer.position());
                // replace smaller buffer with large one, limit should be new length
                mByteBuffer = newByteBuffer;
                Log.d(LOG_TAG, "setLength(" + newLength + ") : file enlarged from " + currLength + " to " + newLength);
            }
        }
    }

    public boolean isSeekable()
    {
        return true;
    }


    public boolean isValid()
    {
        return (mByteBuffer != null);
    }


    public boolean isMemBased()
    {
        return true;
    }


    // realloc
    private boolean growFile(int growth)
    {
        if (mByteBuffer != null)
        {
            setLength(mByteBuffer.limit() + growth);
            return true;
        }
        return false;
    }

    // helper
    private int maxLen()
    {
        // space between current read/write pointer and end-of-file
        return mByteBuffer.limit() - mByteBuffer.position();
    }

    // helper
    private byte[] readCompleteStream(InputStream is) throws IOException
    {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        int nRead;
        byte[] data = new byte[1024];
        while ((nRead = is.read(data, 0, data.length)) != -1)
        {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();
        return buffer.toByteArray();
    }
}
