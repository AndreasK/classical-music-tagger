/*
 * Copyright (C) 2020-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.net.Uri;
import android.os.Build;
import android.util.Log;

import org.jaudiotagger.audio.MyFile;
import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;

/** @noinspection CommentedOutCode*/
public class MyFileUsingFile implements MyFile
{
    private File f;
    private static final String LOG_TAG = "CMT : MyFileUsingFile";
    public static File cacheDirectory = null;     // TODO: find a better solution

    //
    // constructors
    //

    /*
    public MyFile(String path)
    {
        Log.d(LOG_TAG, "MyFile(path = \"" + path + "\")");
        f = new File(path);
    }
    */

    MyFileUsingFile(File file)
    {
        Log.d(LOG_TAG, "MyFile(file)");
        f = file;
    }

    /*
    public MyFile(MyFile parent, String child)
    {
        Log.d(LOG_TAG, "MyFile(parent, child)");
        f = new File(parent.f, child);
    }
    */

    /*
    public MyFile(String parent, String child)
    {
        Log.d(LOG_TAG, "MyFile(parent, child)");
        f = new File(parent, child);
    }
    */

    //
    // kind of constructors
    //

    /*
    public MyFile getAbsoluteFile()
    {
        return new MyFile(f.getAbsoluteFile());
    }
    */

    public MyFileUsingFile getParentFile()
    {
        return new MyFileUsingFile(f.getParentFile());
    }

    public MyRandomAccessFile getRandomAccessFile(final String mode)
    {
        // We cannot use the try-with-resources method, because this would
        // automatically close 'raf', but we must return an opened one.
        //noinspection resource
        MyRandomAccessFile raf = new MyRandomAccessFileUsingFile(f, mode);
        return (raf.isValid()) ? raf : null;
    }

    /*
    public static MyFile createTempFile(String prefix, String suffix, MyFile directory)
    {
        try
        {
            return new MyFile(File.createTempFile(prefix, suffix, directory.f));
        } catch (IOException e)
        {
            return null;
        }
    }
     */

    public MyFileUsingFile createTempFileInSameDirectory(String suffix)
    {
        // get original name without extension
        String originalBaseName = getBaseName();

        // check for possible temp filename in same directory
        int count = 0;
        String tmpName = originalBaseName + suffix;
        while (existsInSameDirectory(tmpName))
        {
            count++;
            tmpName = originalBaseName + suffix + count;
        }

        File tmpFile = new File(f.getParentFile(), tmpName);
        return new MyFileUsingFile(tmpFile);
    }

    public MyFile createTempFile(String suffix)
    {
        MyFile mf = null;
        if (cacheDirectory != null)
        {
            try
            {
                File tfile = File.createTempFile(f.getName(), suffix, cacheDirectory);
                /*
                File tfile = new File("/storage/emulated/0/Download/jaudiomusictagger.tmpfile");
                tfile.delete();
                 */
                mf = new MyFileUsingFile(tfile);
                Log.d(LOG_TAG, "createTempFile() -> " + mf.getPath());
            } catch (IOException e)
            {
                Log.e(LOG_TAG, "createTempFile(file) : exception = " + e.getMessage());
            }
        }
        return mf;
    }

    public void objcopyFrom(MyFile from)
    {
        f = ((MyFileUsingFile) from).f;
    }

    //
    // names and paths
    //

    public String getName()
    {
        return f.getName();
    }

    public String getPath()
    {
        return f.getPath();
    }

    public Uri getUri() {return null;}

    public String getAbsolutePath()
    {
        return f.getAbsolutePath();
    }

    public boolean exists()
    {
        return f.exists();
    }

    public boolean delete() { return f.delete(); }

    //public boolean isHidden() { return f.isHidden(); }

    public boolean isDirectory() { return f.isDirectory(); }

    public boolean renameTo(String filename)
    {
        File fp = f.getParentFile();
        if (fp != null)
        {
            File fpn = new File(fp, filename);
            return f.renameTo(fpn);
        }
        return false;
    }

    public boolean renameTo(MyFile newfile)
    {
        return f.renameTo(((MyFileUsingFile) newfile).f);
    }

    public boolean canRead()
    {
        return f.canRead();
    }

    public boolean canWrite()
    {
        return f.canWrite();
    }

    public long length()
    {
        return f.length();
    }

    public long lastModified()
    {
        return f.lastModified();
    }

    @SuppressWarnings("UnusedReturnValue")
    public boolean setLastModified(long time)
    {
        return f.setLastModified(time);
    }

    public long created()
    {
        return 0;   // unsupported
    }

    public boolean setCreated(long time)
    {
        return false;   // unsupported
    }

    public FileInputStream getFileInputStream()
    {
        try
        {
            return new FileInputStream(f);
        } catch (FileNotFoundException e)
        {
            return null;
        }
    }

    public FileOutputStream getFileOutputStream()
    {
        try
        {
            return new FileOutputStream(f);
        } catch (FileNotFoundException e)
        {
            return null;
        }
    }

    public Path toPath()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            return f.toPath();
        }
        else
        {
            return null;
        }
    }

    //
    // utility functions
    //

    public boolean existsInSameDirectory(String name)
    {
        String parentPath = f.getParent();
        File fn = new File(parentPath, name);
        return fn.exists();
    }

    public String getBaseName()
    {
        String name = getName();
        int index = name.lastIndexOf('.');
        if (index > 0)
        {
            return name.substring(0, index);
        }

        return name;
    }

    public boolean replaceWith(MyFile newFile)
    {
        // get original name without extension
        String originalName = getName();
        String originalBaseName = getBaseName();

        // check for possible backup filename in same directory
        int count = 0;
        String backupFileName = originalBaseName + ".old";
        while (existsInSameDirectory(backupFileName))
        {
            count++;
            backupFileName = originalBaseName + ".old" + count;
        }
        File backupFile = new File(f.getParent(), backupFileName);

        // Backup filename of "bla.mp3" is now "bla.old" or "bla.old1", "bla.old2" etc.
        // rename original file "bla.mp3" to "bla.old"
        boolean bRes = renameTo(backupFileName);
        if (!bRes)
        {
            return false;
        }
        // NOTE: "this" now points to a non existing file "bla.mp3",
        //       while the original file "bla.mp3" has been renamed to "bla.old".
        //       So "backupFile" now points to the original file.

        // rename new file "bla.tmp" to original file "bla.mp3"
        bRes = newFile.renameTo(originalName);
        if (!bRes)
        {
            // first re-rename original file to original filename
            renameTo(originalName);  // TODO: result is ignored
            // then give up
            return false;
        }
        // NOTE: "newFile" now points to a non existing file "bla.tmp",
        //       while the new file "bla.tmp" has been renamed to "bla.mp3".
        //       So "this" now points to the new file.

        // delete the original file that had been renamed to "bla.old"
        //noinspection ResultOfMethodCallIgnored
        backupFile.delete();  // TODO: result is ignored

        return true;
    }


    //
    // not allowed
    //

    /*
    File getFile()
    {
        return f;
    }
     */
}
