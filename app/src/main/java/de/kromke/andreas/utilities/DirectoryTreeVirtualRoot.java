/*
 * Copyright (C) 2017-20 Andreas Kromke, andreas.kromke@gmail.com
 *
 * This program is free software; you can redistribute it or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

package de.kromke.andreas.utilities;

import android.content.ContentResolver;
import android.content.Context;
import android.content.UriPermission;
import android.net.Uri;
import androidx.annotation.NonNull;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * helper class to handle directories
 */

public class DirectoryTreeVirtualRoot implements DirectoryTree
{
    static public class DirectoryEntryVirtualRoot implements DirectoryEntry
    {
        String f;
        int i;
        String n;
        @SuppressWarnings({"unused", "RedundantSuppression"})
        private static final String LOG_TAG = "CMT : DEV"; //for debugging purposes.

        @SuppressWarnings("WeakerAccess")
        public DirectoryEntryVirtualRoot(int index, String path, String name)
        {
            i = index;
            f = path;
            n = name;
        }

        public int compareTo(DirectoryEntry o)
        {
            DirectoryEntryVirtualRoot to = (DirectoryEntryVirtualRoot) o;
            return f.compareTo(to.f);
        }

        public boolean isDirectory()
        {
            return true;
        }

        public boolean exists() { return true; }

        public boolean delete() { return false; }

        public boolean canRead()
        {
            return true;
        }

        public boolean canWrite()
        {
            return false;
        }

        public boolean copyTo(DirectoryEntry destination, ContentResolver theContentResolver)
        {
            return false;
        }

        public DirectoryEntry copyTo(String name, ContentResolver theContentResolver)
        {
            return null;
        }

        public String getName()
        {
            return n;
        }

        public String getPath()
        {
            return f;
        }

        public String getFilePath()
        {
            return f;
        }

        @NonNull
        public DirectoryEntry getParent()
        {
            return this;
        }

        public Uri getUri()
        {
            return null;
        }

        public File getFile()
        {
            return new File(f);
        }

        @NonNull
        public String toString()
        {
            return f;
        }

        public opResult existsInSameDirectory(String name)
        {
            return opResult.ERROR;
        }

        public boolean renameToEx(String name, boolean changeTo)
        {
            return false;
        }

        public opResult isSeekable(ContentResolver contentResolver)
        {
            return opResult.NO;
        }
    }


    public static String strStorageSelection = null;
    public static String strSafOnly = null;
    private static final String LOG_TAG = "CMT : DEV"; //for debugging purposes.
    private final String[] mBasePaths;
    private List<UriPermission> mUriPermissions;
    public static boolean mFileReadWriteAccess = false;

    // constructor
    public DirectoryTreeVirtualRoot(String[] basePaths)
    {
        mBasePaths = basePaths;
    }

    public boolean isValid()
    {
        return true;
    }

    public DirectoryTree setCurrent(Context context, DirectoryEntry base)
    {
        if (base instanceof DirectoryEntryVirtualRoot)
        {
            int index = ((DirectoryEntryVirtualRoot) base).i;
            if (index >= mBasePaths.length)
            {
                Uri uri = mUriPermissions.get(index - mBasePaths.length).getUri();
                return new DirectoryTreeSaf(context, uri);
            }
        }
        return new DirectoryTreeFile(base.getPath());
    }

    public boolean isRoot()
    {
        return true;
    }

    // get the current directory
    public DirectoryEntry getCurrent()
    {
        return new DirectoryTreeFile.DirectoryEntryFile("");
    }

    // get the current path as Uri String
    public String getCurrentUriAsString()
    {
        return null;
    }

    // set and get previous directory
    public DirectoryEntry gotoParent()
    {
        return null;
    }

    // set the current directory
    public void setCurrent(Context context, String currentPath)
    {
        Log.e(LOG_TAG, "cannot go to path: " + currentPath);
    }

    // Returns a sorted list of all dirs and files in a given directory.
    // With internal memory and one SD card, this list looks like:
    //  /storage/emulated/0/Music
    //  /storage/12AB-CD78/Music
    public List<DirectoryEntry> getChildren()
    {
        List<DirectoryEntry> dirs = new ArrayList<>();

        int i = 0;
        for (String path : mBasePaths)
        {
            String name = path;
            if (path.startsWith("/storage/emulated/0"))
            {
                name = "/INTERNAL" + path.substring(19);
            }
            else
            if (path.startsWith("/storage/"))
            {
                name = path.substring(8);
            }
            dirs.add(new DirectoryEntryVirtualRoot(i, path, name));
            i++;
        }

        for (UriPermission perm : mUriPermissions)
        {
            String path = perm.getUri().getPath();
            @SuppressWarnings("UnnecessaryLocalVariable") String name = path;
            dirs.add(new DirectoryEntryVirtualRoot(i, path, name));
            i++;
        }

        return dirs;
    }

    @NonNull
    public String toString()
    {
        return "";
    }

    @NonNull
    public String getInfoString()
    {
        String storage = (mFileReadWriteAccess) ?
                            strStorageSelection :
                            strStorageSelection + " (" + strSafOnly + ")";

        return "< " + storage + " >";
    }

    // special function to add SAF capability
    public void setUriPermissions(Context context, List<UriPermission> permissions)
    {
        mUriPermissions = permissions;

        // remove invalid entries. Note that we cannot use the elegant for loop because of self modification
        for (int i = 0; i < mUriPermissions.size(); i++)
        {
            UriPermission perm = mUriPermissions.get(i);
            if (!DirectoryTreeSaf.isValid(context, perm.getUri()))
            {
                mUriPermissions.remove(i);
                i--;    // compensate the (i++) and loop end
            }
        }
    }
}
