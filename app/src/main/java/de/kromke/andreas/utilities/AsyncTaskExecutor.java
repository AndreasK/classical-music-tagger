/*
 Replacement for AsyncTask which was declared as deprecated, for whatever reason.

 improved from https://stackoverflow.com/a/68395429
 */

package de.kromke.andreas.utilities;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Arrays;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;

@SuppressWarnings("unused")
public abstract class AsyncTaskExecutor<Params, Progress, Result>
{
    public static final String TAG = "AsyncTaskRunner";

    private static final Executor THREAD_POOL_EXECUTOR =
            new ThreadPoolExecutor(5, 128, 1,
                    TimeUnit.SECONDS, new LinkedBlockingQueue</*Runnable*/>());

    private final Handler mHandler = new Handler(Looper.getMainLooper());
    private boolean mIsInterrupted = false;
    private Result result = null;
    private Progress[] progressValues;

    @MainThread
    protected void onPreExecute(){}
    @SuppressWarnings("unchecked")
    @MainThread
    protected abstract Result doInBackground(Params... params);
    @MainThread
    protected void onPostExecute(Result result){}
    @MainThread
    protected void onCancelled() {}

    @SafeVarargs
    @MainThread
    public final void execute(Params... params) {
        THREAD_POOL_EXECUTOR.execute(() -> {
            result = null;
            try {
                checkInterrupted();
                mHandler.post(this::onPreExecute);

                checkInterrupted();
                result = doInBackground(params);

                checkInterrupted();
                mHandler.post(this::_onPostExecute);
            } catch (InterruptedException ex) {
                mHandler.post(this::onCancelled);
            } catch (Exception ex) {
                Log.e(TAG, "executeAsync(): " + ex.getMessage());
                mHandler.post(this::_onPostExecute);    // result will be null
            }
        });
    }

    @WorkerThread
    protected final void _publishProgress()
    {
        if (!isInterrupted())
        {
            publishProgress(progressValues);
        }
    }

    @SafeVarargs
    @WorkerThread
    protected final void publishProgress(Progress... values)
    {
        if (!isInterrupted())
        {
            progressValues = Arrays.asList(values).toArray(values);
            mHandler.post(this::_publishProgress);
        }
    }

    @SafeVarargs
    @MainThread
    protected final void onProgressUpdate(Progress... values)
    {
    }

    private void _onPostExecute()
    {
        onPostExecute(result);
    }

    private void checkInterrupted() throws InterruptedException {
        if (isInterrupted()){
            throw new InterruptedException();
        }
    }

    @MainThread
    public void cancel(boolean mayInterruptIfRunning){
        setInterrupted(mayInterruptIfRunning);
    }

    public boolean isInterrupted() {
        return mIsInterrupted;
    }

    public void setInterrupted(boolean interrupted) {
        mIsInterrupted = interrupted;
    }
}
