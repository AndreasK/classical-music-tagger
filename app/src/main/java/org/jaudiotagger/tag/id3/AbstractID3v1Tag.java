/*
 *  @author : Paul Taylor
 *  @author : Eric Farng
 *
 *  Version @version:$Id$
 *
 *  MusicTag Copyright (C)2003,2004
 *
 *  This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser
 *  General Public  License as published by the Free Software Foundation; either version 2.1 of the License,
 *  or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License along with this library; if not,
 *  you can get a copy from http://www.opensource.org/licenses/lgpl-license.php or write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Description:
 * Abstract superclass of all URL Frames
 *
 */
package org.jaudiotagger.tag.id3;

import android.util.Log;

import org.jaudiotagger.audio.MyRandomAccessFile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * This is the abstract base class for all ID3v1 tags.
 *
 * @author : Eric Farng
 * @author : Paul Taylor
 */
abstract public class AbstractID3v1Tag extends AbstractID3Tag
{
    public static final String LOG_TAG = "JAT";

    //Tag ID as held in file
    public static final String TAG = "JAT";

    //Logger
    public static Logger logger = Logger.getLogger("org.jaudiotagger.tag.id3");

    //If field is less than maximum field length this is how it is terminated
    static final byte END_OF_FIELD = (byte) 0;

    //Used to detect end of field in String constructed from Data
    static Pattern endofStringPattern = Pattern.compile("\\x00");

    static final byte[] TAG_ID = {(byte) 'T', (byte) 'A', (byte) 'G'};

    // fields lengths common to v1 and v1.1 tags
    static final int TAG_LENGTH = 128;
    //static final int TAG_DATA_LENGTH = 125;
    static final int FIELD_TAGID_LENGTH = 3;
    static final int FIELD_TITLE_LENGTH = 30;
    static final int FIELD_ARTIST_LENGTH = 30;
    static final int FIELD_ALBUM_LENGTH = 30;
    static final int FIELD_YEAR_LENGTH = 4;
    //static final int FIELD_GENRE_LENGTH = 1;

    // field positions, starting from zero so fits in with Java Terminology
    static final int FIELD_TAGID_POS = 0;
    static final int FIELD_TITLE_POS = 3;
    static final int FIELD_ARTIST_POS = 33;
    static final int FIELD_ALBUM_POS = 63;
    static final int FIELD_YEAR_POS = 93;
    static final int FIELD_GENRE_POS = 127;

    //For writing output
    //static final String TYPE_TITLE = "title";
    //static final String TYPE_ARTIST = "artist";
    //static final String TYPE_ALBUM = "album";
    //static final String TYPE_YEAR = "year";
    //static final String TYPE_GENRE = "genre";

    AbstractID3v1Tag()
    {
    }


    AbstractID3v1Tag(AbstractID3v1Tag copyObject)
    {
        super(copyObject);
    }


    /**
     * Return the size of this tag, the size is fixed for tags of this type
     *
     * @return size of this tag in bytes
     */
    public int getSize()
    {
        return TAG_LENGTH;
    }


    /**
     * Does a v1tag or a v11tag exist
     *
     * @return whether tag exists within the byteBuffer
     */
    private static boolean seekForV1OrV11Tag(ByteBuffer byteBuffer)
    {
        byte[] buffer = new byte[FIELD_TAGID_LENGTH];
        // read the TAG value
        byteBuffer.get(buffer, 0, FIELD_TAGID_LENGTH);
        return (Arrays.equals(buffer, TAG_ID));
    }


    /**
     * Delete tag from file
     * Looks for tag and if found lops it off the file.
     *
     * @param raf to delete the tag from
     * @throws IOException if there was a problem accessing the file
     */
    public void delete(MyRandomAccessFile raf) throws IOException
    {
        ByteBuffer byteBuffer = readTag2ByteBuffer(raf);
        if (byteBuffer == null)
        {
            throw new IOException("File is too small to contain a tag");
        }

        if (AbstractID3v1Tag.seekForV1OrV11Tag(byteBuffer))
        {
            try
            {
                raf.setLength(raf.length() - TAG_LENGTH);
            }
            catch(Exception ex)
            {
                Log.e(LOG_TAG, "Unable to delete existing ID3v1 Tag:" + ex.getMessage());
            }
        }
        else
        {
            Log.e(LOG_TAG, "Unable to find ID3v1 tag to deleteField");
        }
    }


    /**
     * read tag into ByteBuffer
     *
     * @param raf the file
     * @return buffer or null
     */
    ByteBuffer readTag2ByteBuffer(MyRandomAccessFile raf)
    {
        try
        {
            long flen = raf.length();
            if (flen < TAG_LENGTH)
            {
                Log.e(LOG_TAG, "File is too small to contain a tag");
                return null;
            }

            raf.seek(flen - TAG_LENGTH);
            byte[] array = new byte[TAG_LENGTH];        // allocate buffer
            raf.readFully(array);                       // fill buffer
            return ByteBuffer.wrap(array);              // convert to ByteBuffer
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }
    }

}
